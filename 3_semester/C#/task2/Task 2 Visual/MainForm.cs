﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Task_2_Visual
{
    public enum TypeOfClient { u, f };
    public enum TypeOfNedv {flat, house, office, promosquare, square, store};
    
    

    public partial class MainForm : Form
    {
        #region autoscroll parameters
        private const int WM_VSCROLL = 0x115;
        private const int SB_BOTTOM = 7;
        [DllImport("user32.dll", CharSet=CharSet.Auto)]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);
        #endregion


        static public bool dealable = false;
        static public bool autoscroll = true;
        static public int speed = 1;
        static public bool speeding = false;
        static public RieltAgency RA = new RieltAgency();
        private ViewLogForm vlf;
        private Toolbox tb;
   
        Random random = new Random();
            
        
        public MainForm()
        {
            InitializeComponent();
            for (int i = 0; i < 250;i++ )
                richTextBox1.Text += RA.Operation(random.Next(0, 6),dealable);
            
            if (tb != null)
                tb.Dispose();
            tb = new Toolbox();
            tb.Show();
     
        }

      
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (speeding)
                for (int i = 0; i < speed; i++)
                {
                    richTextBox1.Text += RA.Operation(random.Next(0, 6), dealable);
                    if (vlf != null)
                        if (vlf.Visible)
                            vlf.DoUpdate(RA.Log);
                    if (autoscroll)
                        SendMessage(richTextBox1.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, (IntPtr)0);
                }
            else
            {
                richTextBox1.Text += RA.Operation(random.Next(0, 6), dealable);
                if (vlf != null)
                    if (vlf.Visible)
                        vlf.DoUpdate(RA.Log);
                if (autoscroll)
                    SendMessage(richTextBox1.Handle, WM_VSCROLL, (IntPtr)SB_BOTTOM, (IntPtr)0);
            }
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
            {
                timer1.Stop();
                button1.Text = "Start";
                button1.Image = global::Task_2_Visual.Properties.Resources._3;
            }
            else
            {
                timer1.Start();
                button1.Text = "Pause";
                button1.Image = global::Task_2_Visual.Properties.Resources._4;
            }
           
        }      

        private void button2_Click(object sender, EventArgs e)
        {
            bool flag = timer1.Enabled;
            
            if (flag)
                timer1.Stop();
         
            ViewForm vf = new ViewForm();
            vf.DoShow(this);
               
            if (flag)
                timer1.Start();
        }  

        private void button3_Click(object sender, EventArgs e)
        {
           
            if (vlf != null)
                vlf.Dispose();
            vlf = new ViewLogForm();
            vlf.DoShow(RA.Log);
         
            
         
        }
        
 /*       private void button4_Click(object sender, EventArgs e)
        {
            Addclient ac = new Addclient();
            ac.Show(this);
        }*/

        private void button4_Click_1(object sender, EventArgs e)
        {
            if (tb != null)
                tb.Dispose();
            tb = new Toolbox();
            tb.Show();
         
        }

    

       


       

       
    }
}
