﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            
            InitializeComponent(); 
        }

       
        protected virtual void DoSave()
        {
        }

        protected virtual void DoAdd()
        {
        }

        protected virtual void DoDelete()
        {
        }

        protected virtual void DoInit(int index)
        {
        }

        public virtual void DoInit()
        {
        }
        
        public void Show(IWin32Window hwnd, int index)
        {
            DoInit(index);
            ShowDialog(hwnd);
        }



       
        private void button1_Click(object sender, EventArgs e)
        {
            DoSave();
        }       

        private void button2_Click(object sender, EventArgs e)
        {
            DoAdd();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DoDelete();
        }

        private void EditForm_Load(object sender, EventArgs e)
        {

        }

       

        
    }
}
