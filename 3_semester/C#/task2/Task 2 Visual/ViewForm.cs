﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class ViewForm : Form
    {
        
        public ViewForm()
        {
            InitializeComponent();

        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if ((listBox1.SelectedIndex>=0)&&(listBox1.SelectedIndex<MainForm.RA.AllSellers.Count))
            {
                using (EditSeller d = new EditSeller())
                {
                    d.Show(this,listBox1.SelectedIndex);
                }
                ShowAll();
            }
        }

        private void listBox2_DoubleClick(object sender, EventArgs e)
        {
            if ((listBox2.SelectedIndex >= 0) && (listBox2.SelectedIndex < MainForm.RA.AllBuyers.Count))
            {
                using (EditBuyer d = new EditBuyer())
                {
                    d.Show(this,listBox2.SelectedIndex);
                }
                ShowAll();
            }
            
        }

        
//Showing and refresing lists
        private void ShowBuyers()
        {
            listBox2.Items.Clear();
            string[] arr2 = new string[MainForm.RA.AllBuyers.Count];
            for (int i = 0; i < arr2.Length; i++)
                arr2[i] = MainForm.RA.AllBuyers[i].Name + ' ' + MainForm.RA.AllBuyers[i].Surname + ' ' + MainForm.RA.AllBuyers[i].Type.ToString() + ' ' + MainForm.RA.AllBuyers[i].requests.Count.ToString();
            listBox2.Items.AddRange(arr2);
        }
        private void ShowSellers()
        {
            listBox1.Items.Clear();
            string[] arr = new string[MainForm.RA.AllSellers.Count];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = MainForm.RA.AllSellers[i].Name + ' ' + MainForm.RA.AllSellers[i].Surname + ' ' + MainForm.RA.AllSellers[i].Type.ToString() + ' ' + MainForm.RA.AllSellers[i].objects.Count.ToString();
            listBox1.Items.AddRange(arr);
        }
        private void ShowAll()
        {
            ShowSellers();
            ShowBuyers();        
        }
//end
       
       
        public void DoShow(IWin32Window hwnd)
        {
            ShowAll();
            ShowDialog(hwnd);
            this.TopMost = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if ((listBox1.SelectedIndex >= 0) && (listBox1.SelectedIndex < MainForm.RA.AllSellers.Count))
            {
                MainForm.RA.AllSellers.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((listBox2.SelectedIndex>=0)&&(listBox2.SelectedIndex<MainForm.RA.AllBuyers.Count))
            {
                MainForm.RA.AllBuyers.RemoveAt(listBox2.SelectedIndex);
                listBox2.Items.RemoveAt(listBox2.SelectedIndex);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (EditSeller ed = new EditSeller())
            {
                ed.DoInit();
                if (ed.ShowDialog(this) == DialogResult.OK)
                {
                    MainForm.RA.AllSellers.Add(ed.Seller);
                    ShowSellers();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

            using (EditBuyer eb = new EditBuyer())
            {
                eb.DoInit();
                if (eb.ShowDialog(this) == DialogResult.OK)
                {
                    MainForm.RA.AllBuyers.Add(eb.Buyer);
                    ShowBuyers();
                }
            }
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                if (listBox1.SelectedIndex >= 0)
                {
                    MainForm.RA.AllSellers.RemoveAt(listBox1.SelectedIndex);
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                }

        }

        private void listBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                if (listBox2.SelectedIndex >= 0)
                {
                    MainForm.RA.AllBuyers.RemoveAt(listBox2.SelectedIndex);
                    listBox2.Items.RemoveAt(listBox2.SelectedIndex);
                }

        }

        
        
    }
}
