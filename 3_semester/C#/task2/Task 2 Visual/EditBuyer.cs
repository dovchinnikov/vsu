﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class EditBuyer : EditForm
    {
       
        
        private Buyer buyer;
        public Buyer Buyer
        {
            get
            {
                return buyer;
            }
        }//read-only
        private bool flag;
        public EditBuyer()
        {
            InitializeComponent();
     
        }
       
        protected override void DoInit(int index)
        {
            
            buyer = MainForm.RA.AllBuyers[index];

            #region

            this.label4 = new System.Windows.Forms.Label();
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(467, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Finded objects:";
            this.Controls.Add(this.label4);
            this.Controls.SetChildIndex(this.label4, 0);

            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(471, 33);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(191, 134);
            this.listBox2.TabIndex = 26;
            this.Controls.Add(this.listBox2);
            this.Controls.SetChildIndex(this.listBox2, 0);

            this.button4 = new System.Windows.Forms.Button();
            this.button4.Location = new System.Drawing.Point(471, 179);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(191, 23);
            this.button4.TabIndex = 27;
            this.button4.Text = "Buy it!";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.Controls.Add(this.button4);


            #endregion

            TypeOfClient tc = buyer.Type;
            textBox1.Text = buyer.Name;
            textBox2.Text = buyer.Surname;
            if (tc == TypeOfClient.f)
                comboBox2.SelectedIndex = 0;
            else
                comboBox2.SelectedIndex = 1;
            DoShowReqs();
            DoShowSatisfied(-1);
            flag = true;
        }

        public override void DoInit()
        {
            this.ClientSize = new System.Drawing.Size(460, 214);
            buyer = new Buyer();
            comboBox2.SelectedIndex = 0;
            flag = false;
        }

        protected override void DoSave()
        {
            buyer.Name = textBox1.Text;
            buyer.Surname = textBox2.Text;
            if (comboBox2.SelectedIndex == 0)
                buyer.Type = TypeOfClient.f;
            else
                buyer.Type = TypeOfClient.u;
            DialogResult = DialogResult.OK;
            this.Close(); 
            
        }

        protected override void DoAdd()
        {
            //if (buyer.requests<   )
           //{
            EditRequest er = new EditRequest();
            er.DoInit();
            if (er.ShowDialog(this) == DialogResult.OK)
            {
                buyer.requests.Add(er.Req);
                DoShowReqs();
            }
                //}
            //else
            //
        }

        protected override void DoDelete()
        {
            if ((listBox1.SelectedIndex  >=0)&&(listBox1.SelectedIndex<buyer.requests.Count))
            {
                buyer.requests.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if ((listBox1.SelectedIndex>=0)&&(listBox1.SelectedIndex< buyer.requests.Count))
            using (EditRequest er = new EditRequest())
            {
                er.DoInit(buyer.requests[listBox1.SelectedIndex], this);
            }
            DoShowReqs();
        }

        private void DoShowReqs()
        {
            listBox1.Items.Clear();
            string[] arr = new string[buyer.requests.Count];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = buyer.requests[i].Type.ToString() + ' ' + buyer.requests[i].MinPrice + ' ' + buyer.requests[i].MaxPrice + ' ';
            listBox1.Items.AddRange(arr);
        }
        private void DoShowSatisfied(int index)
        {
            listBox2.Items.Clear();
            if (index < 0)
                listBox2.Items.Add("Select request...");

            else
            {
                List<FNedv> fo = new List<FNedv>(MainForm.RA.FindObjects(buyer.requests[index]));
                if (fo.Count == 0)
                    listBox2.Items.Add("No objects found :-(");
                else
                {
                    string[] arr = new string[fo.Count];
                    for (int i = 0; i < arr.Length; i++)
                        arr[i] = fo[i].Obj.Price.ToString() + ' ' + fo[i].Obj.Type.ToString() + ' ' +fo[i].Owner.Name+' '+fo[i].Owner.Surname;
                    listBox2.Items.AddRange(arr);
                }
            }
        }
        private FNedv GiveMeFObject(int rIndex, int oIndex)
        {
            if ((rIndex >= 0) && (oIndex >= 0))
                return MainForm.RA.FindObjects(buyer.requests[rIndex])[oIndex];
            else
                return null;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FNedv fobj = GiveMeFObject(listBox1.SelectedIndex, listBox2.SelectedIndex);
            if (fobj != null)
            {
                if (MainForm.RA.Deal(fobj.Owner, fobj.Obj,buyer , buyer.requests[listBox1.SelectedIndex], true))
                {
                    MessageBox.Show("них#я себе! оно работает!");
                    DoShowReqs();
                    DoShowSatisfied(-1);
                }
            } 
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           if (flag)
            DoShowSatisfied(listBox1.SelectedIndex);
        }
    }
}