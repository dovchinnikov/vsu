﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
   public class Sdelka
    {
        private Nedv obj;
        private Request req;
        private Seller owner;
        private Buyer buyer;
        public bool rFlag = true;

        public Request Request
        {
            get
            {
                if (rFlag)
                    return req;
                else 
                    return null;
            }
            set
            {
                if (rFlag)
                    req = value;
                else
                    req = null;
            }
        }
        public Nedv Object
        {
            get
            {
                return obj;
            }
            set
            {
                obj = value;
            }
        }
        public Seller Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
            }
        }
        public Buyer Buyer
        {
            get
            {
                return buyer;
            }
            set
            {
                buyer = value;
            }
        }

    }
}
