﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public class RieltAgency
    {
        public const int MAXSELLERS = 20;
        public const int MAXBUYERS = 10;
        public const int MAXOBJECTS = 15;
        public const int MAXREQUESTS = 5;
        public const int MINPRICE = 1000;
        public const int MAXPRICE = 1000001;
        
        Random r = new Random();
    
        public List<Sdelka> Log = new List<Sdelka>();
        public List<Seller> AllSellers = new List<Seller>();
        public List<Buyer> AllBuyers = new List<Buyer>();

        private int loIndex;
        public int GetLastOperationIndex
        {
            get
            {
                return loIndex;
            }
        }

        public bool Deal(Seller owner, Nedv obj, Buyer buyer, Request req, bool noConfirm)
        {
            if (noConfirm)
            {
                Sdelka s = new Sdelka();
                s.Buyer = buyer;
                s.Owner = owner;
                s.Object = obj;
                s.Request = req;
                owner.DeleteObject(obj);
                buyer.DeleteRequest(req);
                Log.Add(s);
                return true;
            }
            else
            {
                if ((owner.Confirm()) && (buyer.Confirm()))
                {
                    Sdelka s = new Sdelka();
                    s.Buyer = buyer;
                    s.Owner = owner;
                    s.Object = obj;
                    s.Request = req;
                    owner.DeleteObject(obj);
                    buyer.DeleteRequest(req);
                    Log.Add(s);
                    return true;
                }
                else
                    return false;
            }
        }
       
        public List<FNedv> FindObjects(Request req)
        {
            List<FNedv> results = new List<FNedv>();           
            foreach (Seller seller in AllSellers)
                foreach (Nedv obj in seller.objects)
                    if ((req.MinPrice <= obj.Price) && (obj.Price <= req.MaxPrice) && (req.Type == obj.Type))
                    {
                        FNedv fobj = new FNedv();
                        fobj.Obj = obj;
                        fobj.Owner = seller;
                        results.Add(fobj);
                    }
            return results;
        }

        public FRequest[] FindRequests(Nedv obj)
        {
            List<FRequest> results = new List<FRequest>();
            foreach (Buyer asker in AllBuyers)
            foreach (Request req in asker.requests)
                if ((req.MinPrice <= obj.Price) && (obj.Price <= req.MaxPrice) && (req.Type == obj.Type))
                {
                    FRequest freq = new FRequest();
                    freq.Buyer = asker;
                    freq.Req = req;

                    results.Add(freq);
                }
            return results.ToArray();
        }

        private FNedv FindCheapest(Request req)
        {
            List<FNedv> founded = FindObjects(req);
            FNedv min = founded[0];
            foreach (FNedv f in founded)
                if (f.Obj.Price < min.Obj.Price)
                    min = f;
            return min;
        }

        private void AddBuyer(string _name, string _surname, TypeOfClient _type)
        {
            Buyer buyer = new Buyer();
            buyer.Name = _name;
            buyer.Surname = _surname;
            buyer.Type = _type;
            AllBuyers.Add(buyer);
        }

        private void AddSeller(string _name, string _surname, TypeOfClient _type)
        {
            Seller seller = new Seller();
            seller.Name = _name;
            seller.Surname = _surname;
            seller.Type = _type;
            AllSellers.Add(seller);
        }

        private void DeleteBuyer(int index)
        {
            AllBuyers.Remove(AllBuyers[index]);
        }

        private void DeleteSeller(int index)
        {
            AllSellers.Remove(AllSellers[index]);
        }

        public string Operation(int index,bool dealable)
        {
            loIndex = index;
            switch (index)
            {
                case 0: //добавление покупателя
                    {
                        if (AllBuyers.Count < MAXBUYERS)
                        {
                            AddBuyer(RandomName(), RandomSurname(), RandCT());
                            int c = AllBuyers.Count - 1;
                            return "Покупатель добавлен: " + AllBuyers[c].Name + ' ' + AllBuyers[c].Surname + ' ' + AllBuyers[c].Type.ToString() + '\n';
                        }
                        else
                            return "Покупатель не добавлен: превышение максимума"+'\n';
                    }
                case 1://добавление продавца
                    {
                        if (AllSellers.Count < MAXSELLERS)
                        {
                            AddSeller(RandomName(), RandomSurname(), RandCT());
                            int c = AllSellers.Count -1;
                            return "Продавец добавлен: " + AllSellers[c].Name + ' ' + AllSellers[c].Surname + ' ' + AllSellers[c].Type.ToString() + '\n';
                        }
                        else
                            return "Продавец не добавлен: превышение максимума"+'\n';                       
                    }
                case 2://добавление объекта на продажу
                    {
                        
                        if (AllSellers.Count == 0)                      
                            return "Объект не добавлен: нету продавцов"+'\n';                                                  
                       
                        int current = r.Next(0,AllSellers.Count);//индекс продавца, которому добавим объект

                        if (AllSellers[current].objects.Count >= MAXOBJECTS)                       
                            return "Объект не добавлен:  покупателя превышение максимума запросов"+'\n';
                            //если превышен максимум то конец case-a
                        
                       
                        AllSellers[current].AddObject("", r.Next(MINPRICE, MAXPRICE), RandOT());
                        
                        int last = AllSellers[current].objects.Count - 1;//индекс добавленного объекта в списке объектов продавца
                        
                        string ret = "Объект добавлен: " + AllSellers[current].objects[last].Name + ' ' + AllSellers[current].objects[last].Price.ToString() + ' ' + AllSellers[current].objects[last].Type.ToString()+'\n';
                        if (!dealable)
                            return ret;
                        FRequest[] f = FindRequests(AllSellers[current].objects[last]);//поиск запросов, которым удовлетврояет новый объект



                        if (f.Length == 0)
                            return ret+"Не найдено запросов, которые удовлетворил бы этот объект"+'\n';//если ничего не найдено то конец case-a   
                                                                  
                        for (int i = 0; i < f.Length; i++)
                            if ((AllSellers[current].Confirm()) && (f[i].Buyer.Confirm()))
                            {
                                Deal(AllSellers[current], AllSellers[current].objects[last], f[i].Buyer, f[i].Req,false);
                                ret += "Совершена сделка!" + '\n' + "     Удовлетворен запрос клиента: " + f[i].Buyer.Name + ' ' + f[i].Buyer.Surname + '\n';
                                return ret;                            
                            }
                        return ret+"Сделка не совершена: отказ одной из сторон"+'\n';
                        
                    }
                case 3://добавление запроса на объект 
                    {
                        if (AllBuyers.Count == 0)
                            return "Запрос не добавлен: нету покупателей"+'\n';

                        int current = r.Next(0, AllBuyers.Count);

                        if (AllBuyers[current].requests.Count >= MAXREQUESTS)
                            return "Запрос не добавлен: у продавца превышение максимума запросов"+'\n';

                        int m = r.Next(MINPRICE, MAXPRICE);
                        AllBuyers[current].AddRequest(m, r.Next(m, MAXPRICE), RandOT());
                        
                        int last = AllBuyers[current].requests.Count - 1;

                        Request re = AllBuyers[current].requests[last];
                        string ret = "Запрос добавлен: " + re.MinPrice.ToString() + ' ' + re.MaxPrice.ToString() + ' ' + re.Type.ToString() + '\n';
                        if (!dealable)
                            return ret;
                        List<FNedv> f = FindObjects(AllBuyers[current].requests[last]);

                        int n = f.Count;

                        if (n == 0)
                            return ret + "Не найдено объектов, удовлетворяющих запрос"+'\n';

                        for (int j = 0; j < n; j++)
                        {
                            if (n == 0)
                                return ret + "Сделка не совершена: отказ одной из сторон"+'\n';
                                
                            int min = 0;
                            for (int i = 1; i < f.Count; i++)
                                if (f[i].Obj.Price < f[min].Obj.Price)
                                    min = i;
                            if ((f[min].Owner.Confirm()) && (AllBuyers[current].Confirm()))
                            {
                                Deal(f[min].Owner, f[min].Obj, AllBuyers[current], AllBuyers[current].requests[last],false);
                                return ret + "Сделка совершена!" + '\n' + "     Куплен объект у клиента " + f[min].Owner.Name + ' ' + f[min].Owner.Surname + '\n';
                            }
                            else
                                f.Remove(f[min]);

                        }
                        return ret + "Сделка не совершена: отказ одной из сторон" + '\n';
                        

                    }
                case 4:
                    {                       
                        int current =r.Next(0,AllSellers.Count);
                        if (current < 4)
                            return "Нет продавцов для удаления"+'\n';

                        string tmp = AllSellers[current].Name+' '+AllSellers[current].Surname;
                        AllSellers.RemoveAt(current);
                        return "Продавец успешно удален: " + tmp + '\n';
                    }
                case 5:
                    {
                        int current = r.Next(0, AllBuyers.Count);
                        if (current < 4)
                            return "Нет покупателей для удаления" + '\n';

                        string tmp = AllBuyers[current].Name + ' ' + AllBuyers[current].Surname;
                        AllBuyers.RemoveAt(current);
                        return "Покупатель успешно удален: " + tmp + '\n';
                    }
                default: return "Wrong parameter";
                          
            }
        }

        public string Operation(int index)
        {
            return Operation(index, true);
        }
        
        private string RandomSurname()
        {
            string[] sl1 = { "ков", "рев", "ка", "ку", "ко", "бе", "ля", "гор", "те", "пре", "тон", "нов", "це", "пи", "ре", "тро", "при", "су" };
            string[] sl2 = { "ов", "ев", "нь", "нко", "кин", "ин", "лов" };
            int k = r.Next(1, 4);
            string s = "";
            for (int i = 0; i < k; i++)
                s += sl1[r.Next(0, sl1.Length)];
            
            s += sl2[r.Next(0, sl2.Length)];

            char s1 = s[0];
            string s2 = s.Remove(0, 1);
            s1=Char.ToUpper(s1);
            s = s1 + s2;
            return s;
        }
        
        private string RandomName()
        {
            string[] Name = { "Алексей", "Евлампий", "Иннокентий", "Вин", "Фэйл", "Чип", "Дэйл", "Рокфор", "Тревор", "Клайд", "Кенни", "Георг", "Эрик", "Иван", "Владислав", "Олег", "Петр", "Сергей", "Николай", "Эдуард", "Всеволод", "Роман", "Евгений" };
            return Name[r.Next(0, Name.Length)];
        }

        private TypeOfClient RandCT()
        {
            if (r.Next(0, 2) == 0)
                return TypeOfClient.f;
            else
                return TypeOfClient.u;
        }

        private TypeOfNedv RandOT()
        {
            switch (r.Next(0, 6))
            {
                case 0: return TypeOfNedv.flat;
                case 1: return TypeOfNedv.house;
                case 2: return TypeOfNedv.office;
                case 3: return TypeOfNedv.promosquare;
                case 4: return TypeOfNedv.square;
                default: return TypeOfNedv.store;
            }

        }

    }
}
