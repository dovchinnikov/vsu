﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class EditSeller : EditForm
    {
        private Seller seller;
        public Seller Seller
        {
            get
            {
                return seller;
            }
        } //read-only     
        private bool flag;
        

        public EditSeller()
        {
            InitializeComponent();
        }
        

        protected override void DoInit(int index)
        {
            seller = MainForm.RA.AllSellers[index];
            #region 

            this.label4 = new System.Windows.Forms.Label();
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(467, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 20);
            this.label4.TabIndex = 28;
            this.label4.Text = "Finded requests:";
            this.Controls.Add(this.label4);
            this.Controls.SetChildIndex(this.label4, 0);
            
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(471, 33);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(191, 134);
            this.listBox2.TabIndex = 26;
            this.Controls.Add(this.listBox2);
            this.Controls.SetChildIndex(this.listBox2, 0);

            this.button4 = new System.Windows.Forms.Button();
            this.button4.Location = new System.Drawing.Point(471, 179);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(191, 23);
            this.button4.TabIndex = 27;
            this.button4.Text = "Sell it!";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.Controls.Add(this.button4);

            
            #endregion
            textBox1.Text = seller.Name;
            textBox2.Text = seller.Surname;
            if (seller.Type == TypeOfClient.f)
                comboBox2.SelectedIndex = 0;
            else
                comboBox2.SelectedIndex = 1;            
            DoShowObjects();
            DoShowSatisfied(-1);
            flag = true;
        }
         
        public  override void DoInit()
        {
            this.ClientSize = new System.Drawing.Size(460, 214);
            seller = new Seller();
            comboBox2.SelectedIndex = 0;
            flag = false;
        }

        protected override void DoSave()
        {
            seller.Name = textBox1.Text;
            seller.Surname = textBox2.Text;
            if (comboBox2.SelectedIndex == 0)
                seller.Type = TypeOfClient.f;
            else
                seller.Type = TypeOfClient.u;
            DialogResult = DialogResult.OK;
            this.Close();
        }

        protected override void DoAdd()
        {
            using (EditObject eo = new EditObject() )
            {
                eo.DoInit();
                if (eo.ShowDialog(this)==DialogResult.OK)
                {
                    seller.objects.Add(eo.Obj);
                    DoShowObjects();
                }
            }

        }

        protected override void DoDelete()
        {
            if ((listBox1.SelectedIndex >= 0) && (listBox1.SelectedIndex < seller.objects.Count))
            {
                seller.objects.RemoveAt(listBox1.SelectedIndex);
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }
        
        private void DoShowObjects()
        {
            listBox1.Items.Clear();
            string[] arr = new string[seller.objects.Count];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = seller.objects[i].Type.ToString() + ' ' + seller.objects[i].Price.ToString() + ' '+seller.objects[i].Name;
            listBox1.Items.AddRange(arr);
        }

        private void DoShowSatisfied(int index)
        {
            listBox2.Items.Clear();
            if (index < 0)
                listBox2.Items.Add ("Select object...");
                
            else
            {
                List<FRequest> fr = new List<FRequest>(MainForm.RA.FindRequests(seller.objects[index]));
                if (fr.Count == 0)
                    listBox2.Items.Add("No requests found :-(");
                else
                {
                    string[] arr = new string[fr.Count];
                    for (int i = 0; i < arr.Length; i++)
                        arr[i] = fr[i].Req.MinPrice.ToString() + ' ' + fr[i].Req.MaxPrice.ToString() + ' ' + fr[i].Req.Type.ToString() + ' ' + fr[i].Buyer.Name + ' ' + fr[i].Buyer.Surname;
                    listBox2.Items.AddRange(arr);                    
                }
            }
            
        }
       
        private FRequest GiveMeFrequest(int oIndex,int rIndex)
        {
            if ((oIndex >= 0) && (rIndex>=0))
                return new List<FRequest>(MainForm.RA.FindRequests(seller.objects[listBox1.SelectedIndex]))[listBox2.SelectedIndex];
            else
                return null;
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if ((listBox1.SelectedIndex >= 0) && (listBox1.SelectedIndex < seller.objects.Count))
            {
                using (EditObject eo = new EditObject())
                {
                    eo.DoInit(seller.objects[listBox1.SelectedIndex], this);
                }
                DoShowObjects();
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {   
            if (flag)
            DoShowSatisfied(listBox1.SelectedIndex);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FRequest freq = GiveMeFrequest(listBox1.SelectedIndex,listBox2.SelectedIndex);
            if (freq != null)
            {
                if (MainForm.RA.Deal(seller, seller.objects[listBox1.SelectedIndex], freq.Buyer, freq.Req, true))
                {
                    MessageBox.Show("них#я себе! оно работает!");
                    DoShowObjects();
                    DoShowSatisfied(-1);
                }
            }    
        }
    }
}
