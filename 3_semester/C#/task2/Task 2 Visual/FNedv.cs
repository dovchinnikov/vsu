﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public class FNedv
    {
        private Seller owner;
        private Nedv obj;

        public Seller Owner
        {
            get
            {
                return owner;
            }
            set
            {
                owner = value;
            }
        }
        public Nedv Obj
        {
            get
            {
                return obj;
            }
            set
            {
                obj = value;
            }
        }

    }
}
