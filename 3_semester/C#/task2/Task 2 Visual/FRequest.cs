﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public class FRequest
    {
        private Request req;
        private Buyer buyer;

        public Request Req
        {
            get
            {
                return req;
            }
            set
            {
                req = value;
            }
        }
        public Buyer Buyer
        {
            get
            {
                return buyer;
            }
            set
            {
                buyer = value;
            }
        }
    }
}
