﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class ViewLogForm : Form
    {
  
        public ViewLogForm()
        {
            InitializeComponent();
           
        }
        public void DoShow(List<Sdelka> l)
        {
            string[] arr = new string[l.Count];
            for (int i = 0; i < arr.Length; i++)
                if (l[i].rFlag)
                    arr[i] = "Покупатель " + l[i].Buyer.Name + ' ' + l[i].Buyer.Surname + " искал " + l[i].Request.Type.ToString() +" от "+ l[i].Request.MinPrice.ToString()+" до " + l[i].Request.MaxPrice.ToString()+" и купил у продавца " + l[i].Owner.Name +' '+ l[i].Owner.Surname+' ' + l[i].Object.Type.ToString()+" по цене " + l[i].Object.Price.ToString();
            listBox1.Items.Clear();
            listBox1.Items.AddRange(arr);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            Show();
        }
        public void DoUpdate(List<Sdelka> l)
        {
            if (l.Count != listBox1.Items.Count)
            {
                int i = l.Count - 1;
                listBox1.Items.Add("Покупатель " + l[i].Buyer.Name + ' ' + l[i].Buyer.Surname + " искал " + l[i].Request.Type.ToString() + " от " + l[i].Request.MinPrice.ToString() + " до " + l[i].Request.MaxPrice.ToString() + " и купил у продавца " + l[i].Owner.Name + ' ' + l[i].Owner.Surname + ' ' + l[i].Object.Type.ToString() + " по цене " + l[i].Object.Price.ToString());
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            }
        }
        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            DoShow(MainForm.RA.Log);
            
            
           
        }

        private void listBox1_MouseHover(object sender, EventArgs e)
        {
         
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Sdelka s = MainForm.RA.Log[listBox1.SelectedIndex];
            toolTip1.Show("Покупатель: " + s.Buyer.Name +' '+ s.Buyer.Surname + '\n'
                + "Запрос: " + s.Request.Type + '\n'
                +"Цена: от " + s.Request.MinPrice.ToString() +" до "+ s.Request.MaxPrice.ToString() + '\n'+'\n'
                +"Продавец: "+s.Owner.Name+' '+s.Owner.Surname+'\n'
                +"Объект: "+s.Object.Type+'\n'
                +"Название: "+s.Object.Name+'\n'
                +"Цена: "+s.Object.Price.ToString()
                , this.listBox1);
        }

        
    }
}
