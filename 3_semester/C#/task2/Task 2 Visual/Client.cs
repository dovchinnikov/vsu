﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public class Client
    {
        private string name;
        private string surname;
        private TypeOfClient type;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public string Surname
        {
            get
            {
                return surname;
            }
            set
            {
                surname = value;
            }
        }
        public TypeOfClient Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }

        public bool Confirm()
        {
            Random r = new Random();
            if (r.Next(0, 2) == 1)
                return true;
            else
                return false;
        }
    }
}
