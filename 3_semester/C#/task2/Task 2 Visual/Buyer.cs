﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public  class Buyer : Client
    {
        public List<Request> requests = new List<Request>();
     
        public void AddRequest(int _min,int _max, TypeOfNedv _type)
        {
            Request req = new Request();
            req.MinPrice = _min;
            req.MaxPrice = _max;
            req.Type = _type;
            requests.Add(req);                 
        }

        public void DeleteRequest(Request req)
        {
            foreach (Request r in requests)
                if (r.Equals(req))
                {
                    requests.Remove(r);
                    break;
                }
        }
    }
}
