﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public class Seller : Client
    {
        public List<Nedv> objects = new List<Nedv>();

        public void AddObject(string _name, int _price, TypeOfNedv _type)
        {
            Nedv obj = new Nedv();
            obj.Name = _name;
            obj.Price = _price ;
            obj.Type = _type;
            //obj.Owner = this;
            objects.Add(obj);
            obj = null;

            
        }

        public bool DeleteObject(Nedv dobj)
        {
            bool flag = false;
            foreach (Nedv obj in objects)
                if (obj.Equals(dobj))
                {
                    objects.Remove(obj);
                    flag = true;
                    break;
                    
                }
            
            return flag;
                             
        }  
    }
}
