﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class Toolbox : Form
    {
        public Toolbox()
        {
            InitializeComponent();
            checkBox1.Checked = MainForm.dealable;
            checkBox2.Checked = MainForm.autoscroll;
            checkBox3.Checked = MainForm.speeding;
            trackBar1.Value = MainForm.speed;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                MainForm.dealable = true;
            else
                MainForm.dealable = false;
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
                MainForm.autoscroll = true;
            else
                MainForm.autoscroll = false;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            MainForm.speed = trackBar1.Value;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                MainForm.speeding = true;
                trackBar1.Enabled = true;
            }
            else
            {
                MainForm.speeding = false;
                trackBar1.Enabled = false;       
               }
        }
    }
}
