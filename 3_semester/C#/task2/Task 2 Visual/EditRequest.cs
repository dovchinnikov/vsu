﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class EditRequest : Form
    {
        private Request req;
        public Request Req
        {
            get
            {
                return req;
            }
        }//read-only
       
        public EditRequest()
        {
            InitializeComponent();
        }

        public void DoInit(Request r, IWin32Window hWnd)
        {
            
            
            comboBox1.Items.Add(TypeOfNedv.flat);
            comboBox1.Items.Add(TypeOfNedv.house);
            comboBox1.Items.Add(TypeOfNedv.office);
            comboBox1.Items.Add(TypeOfNedv.promosquare);
            comboBox1.Items.Add(TypeOfNedv.square);
            comboBox1.Items.Add(TypeOfNedv.store);
            
            req = r;
            
            comboBox1.SelectedIndex = comboBox1.Items.IndexOf(req.Type);
            //numericUpDown1.Minimum = 
            numericUpDown1.Value = req.MinPrice;
            //numericUpDown1.Maximum = 
            numericUpDown2.Minimum = numericUpDown1.Value;
            numericUpDown2.Value = req.MaxPrice;
            //numericUpDown2.Maximum = 
            ShowDialog(hWnd);
        }

        public void DoInit()
        {
            comboBox1.Items.Add(TypeOfNedv.flat);
            comboBox1.Items.Add(TypeOfNedv.house);
            comboBox1.Items.Add(TypeOfNedv.office);
            comboBox1.Items.Add(TypeOfNedv.promosquare);
            comboBox1.Items.Add(TypeOfNedv.square);
            comboBox1.Items.Add(TypeOfNedv.store);
            comboBox1.SelectedIndex = 0;
            //numericUpDown1.Minimum = 
            //numericUpDown1.Value = 
            //numericUpDown1.Maximum = 
            //numericUpDown2.Minimum = 
            //numericUpDown2.Value = 
            //numericUpDown2.Maximum = 
            req = new Request();
            
        }
       
        private void DoSave()
        {
            req.MinPrice = (int)numericUpDown1.Value;
            req.MaxPrice = (int)numericUpDown2.Value;
            req.Type = (TypeOfNedv) comboBox1.SelectedItem;
        }

      
        private void button2_Click(object sender, EventArgs e)
        {
            DoSave();
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            numericUpDown2.Minimum = numericUpDown1.Value;
        }

    }
}
