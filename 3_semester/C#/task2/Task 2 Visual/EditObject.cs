﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task_2_Visual
{
    public partial class EditObject : Form
    {
        private Nedv obj;
        public Nedv Obj
        {
            get
            {
                return obj;
            }
        }//read-only

        public EditObject()
        {
            InitializeComponent();
            comboBox1.Items.Add(TypeOfNedv.flat);
            comboBox1.Items.Add(TypeOfNedv.house);
            comboBox1.Items.Add(TypeOfNedv.office);
            comboBox1.Items.Add(TypeOfNedv.promosquare);
            comboBox1.Items.Add(TypeOfNedv.square);
            comboBox1.Items.Add(TypeOfNedv.store);
        }
        
        public void DoInit(Nedv o, IWin32Window hwnd)
        {       
            obj = o;

            comboBox1.SelectedIndex = comboBox1.Items.IndexOf(obj.Type);
            
            numericUpDown1.Value = obj.Price;
            textBox1.Text = obj.Name;
            
            ShowDialog(hwnd);
        }

        public void DoInit()
        {
           
            comboBox1.SelectedIndex = 0; 
            obj = new Nedv();
        }
       
        private void DoSave()
        {
            obj.Name = textBox1.Text;
            obj.Price = (int)numericUpDown1.Value;
            obj.Type = (TypeOfNedv)comboBox1.SelectedItem;
        }


       
        private void button2_Click(object sender, EventArgs e)
        {
            DoSave();
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
