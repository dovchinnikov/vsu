﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_2_Visual
{
    public class Request 
    {
        private int minprice;
        private int maxprice;
        private TypeOfNedv type;
       

        public int MinPrice
        {
            get
            {
                return minprice;
            }
            set
            {
                minprice = value;
            }
        }
        public int MaxPrice
        {
            get
            {
                return maxprice;
            }
            set
            {
                maxprice = value;
            }
        }
        public TypeOfNedv Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
            }
        }
       }
           
}

