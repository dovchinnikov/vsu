﻿using System;
using System.Collections.Generic;
using Oop.Tasks.Paint.Interface;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Circle
{
    [PluginForLoad(true)]
    public class CirclePlugin: IToolPaintPlugin
    {
        private IPaintPluginContext pluginContext = null;
        private IPaintApplicationContext appcontext = null;
        private CurclePluginControl optionsControl = null;
        private Image icon = null;
      
        private Font font;
        private Rectangle rect;
        private Pen fore, back;

        private Graphics graph;
        private Image virginImage;
        private bool mdown;
       
        private TextForm textForm;
        private Options opts;
        private Point startpoint;

        public string ToolName
        {
            get
            {
                return "TextType tool";
            }
        }
        public Image Icon
        {
            get
            {
                return icon;
            }
        }
        public string Name 
        {
            get 
            {
                return "plugin name"; 
            } 
        }
        public string CommandName 
        { 
            get
            {
                return "Напечатать текст"; 
            } 
        }
       

        public void Select(bool selection)
        {
            if (selection)
            {
                pluginContext.ApplicationContext.OptionsControl = optionsControl;
                pluginContext.ApplicationContext.Cursor = Cursors.IBeam;
            }
            else
            {
                optionsControl.ToolForm.Dispose();
            }
        }

        public void AfterCreate(IPaintPluginContext pluginContext)
        {            
            this.opts = new Options(false, 4);
            this.pluginContext = pluginContext;            
            this.appcontext = pluginContext.ApplicationContext;
            this.font = new Font("Mistral", 22);
            this.graph = Graphics.FromImage(appcontext.Image);
            this.optionsControl = new CurclePluginControl(this.opts,font);
            this.mdown = false;
            this.fore = new Pen(appcontext.ForegroundColor);
            this.back = new Pen(appcontext.BackgroundColor);
            try
            {
                string imgdir = pluginContext.PluginDir;
                imgdir += "/icon.bmp";
                this.icon = Image.FromFile(imgdir);
            }
            catch { }          
            TextForm_Disposed(new Object(), new EventArgs());
            optionsControl.FontChanged += new EventHandler(optionsControl_FontChanged);
            optionsControl.ControlOptionsChanged += new EventHandler(optionsControl_ControlOptionsChanged);
        }

        private void TextForm_Disposed(object sender, EventArgs e)
        {
           textForm = new TextForm("Текст",font);
           textForm.EditingCompeled+=new EventHandler(TextForm_EditingCompeled);
           textForm.Disposed += new EventHandler(TextForm_Disposed);
           textForm.EditingEscaped += new EventHandler(textForm_EditingEscaped);
        }

        void textForm_EditingEscaped(object sender, EventArgs e)
        {
            graph.DrawImage(virginImage, new Point(0, 0));
            appcontext.Invalidate();
            textForm.Close();
        }     
        private void optionsControl_ControlOptionsChanged(object sender, EventArgs e)
        {
            this.opts = (Options)sender;
        }
        void optionsControl_FontChanged(object sender, EventArgs e)
        {
            this.font = (Font)sender;
        }
        void TextForm_EditingCompeled(object sender, EventArgs e)
        {
            graph.DrawImage(virginImage, new Point(0, 0));
            MethodInvoker invoker = new MethodInvoker(textForm.Close);
            invoker.Invoke();       
            invoker = null;
            if (opts.Shadow) 
                graph.DrawString((string)sender, font, back.Brush, new Point(rect.X +opts.ShadowSize,rect.Y + opts.ShadowSize));
            graph.DrawString((string)sender, font, fore.Brush, new Point(rect.X,rect.Y) );
            appcontext.Invalidate();
            rect = new Rectangle(-10, -10, 0, 0);            
        }
       

        public void MouseDown(MouseEventArgs me, Keys modifierKeys)
        {
            if (me.Button == MouseButtons.Left)
            {
                textForm.Dispose();
                textForm.Location = Cursor.Position;
                startpoint = me.Location;
                mdown = true;
                virginImage = (Image)appcontext.Image.Clone();
            }
        }

        public void MouseMove(MouseEventArgs me, Keys modifierKeys)
        {           
            if (mdown)
            {
                #region Вычисление rect-a
                int x1, x2, y1, y2;

                if (startpoint.X < me.X)
                {
                    x1 = startpoint.X;
                    x2 = me.X;
                }
                else
                {
                    x1 = me.X;
                    x2 = startpoint.X;
                }
                if (startpoint.Y < me.Y)
                {
                    y1 = startpoint.Y;
                    y2 = me.Y;
                }
                else
                {
                    y1 = me.Y;
                    y2 = startpoint.Y;
                }
                rect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
                #endregion   
                graph.DrawImage(virginImage, new Point(0, 0));           
                graph.DrawRectangle(new Pen(Color.Black), rect);
                appcontext.Invalidate();
            }
            
        }

        public void MouseUp(MouseEventArgs me, Keys modifierKeys)
        {
            #region Вычисление rect-a
            int x1, x2, y1, y2;

            if (startpoint.X < me.X)
            {
                x1 = startpoint.X;
                x2 = me.X;
            }
            else
            {
                x1 = me.X;
                x2 = startpoint.X;
            }
            if (startpoint.Y < me.Y)
            {
                y1 = startpoint.Y;
                y2 = me.Y;
            }
            else
            {
                y1 = me.Y;
                y2 = startpoint.Y;
            }
            rect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
            #endregion
            if (me.Button == MouseButtons.Left)
            {
                mdown = false;
                if (textForm.Location.X < Cursor.Position.X)
                {
                    textForm.Width = Cursor.Position.X - textForm.Location.X;
                }
                else
                {
                    textForm.Width = textForm.Location.X - Cursor.Position.X;
                    textForm.Location = new Point(Cursor.Position.X, textForm.Location.Y);
                }
                if (textForm.Location.Y > Cursor.Position.Y)
                    textForm.Location = new Point(textForm.Location.X, Cursor.Position.Y);
                if (textForm.Width < 100) textForm.Width = 100;
                textForm.Show();
                textForm.BringToFront();
            }
        }
       
        public void Escape()
        {
        }//do nothing

        public void Enter()
        {
            
         }//do nothing

        public void ColorChange()
        {
            try
            {
                this.fore.Color = appcontext.ForegroundColor;
                this.back.Color = appcontext.BackgroundColor;
            }
            catch { }
            
        }//do nothing

        public void ImageChange()//do nothing
        {
        }   

        public void BeforeDestroy()
        {
            
           if (MessageBox.Show("Вы точно хотите удалить плагин?","Вы не шутите???",MessageBoxButtons.YesNo,MessageBoxIcon.Question)==DialogResult.No )
               return;
           optionsControl.Dispose();
           pluginContext = null;
           appcontext = null;
        }
    }
}
