﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Circle
{
    public partial class ToolForm : Form
    {
        private Options options; 
 
        public event EventHandler ToolOptionsChanged;

        public ToolForm(Options opts)
        {
            InitializeComponent();
            options = opts;
            checkBox1.Checked = opts.Shadow;
            trackBar1.Value = opts.ShadowSize;
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            trackBar1.Enabled = checkBox1.Checked;
            options.Shadow = checkBox1.Checked;
            ToolOptionsChanged(options, new EventArgs());
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            options.ShadowSize = trackBar1.Value;
            ToolOptionsChanged(options, new EventArgs());
        }
    
        
    }
}
