﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Circle
{
    public class Options
    {
        public Options(bool s, int ss)
        {
            shadow = s;
            shadowsize = ss;
        }
        
        private bool shadow;
        private int shadowsize;


        public bool Shadow

        {
            get
            {
                return shadow;
            }
            set
            {
                shadow = value;
            }
        }

        public int ShadowSize
        {
            get
            {
                return shadowsize;
            }
            set
            {
                shadowsize = value;
            }
        }

    }
}
