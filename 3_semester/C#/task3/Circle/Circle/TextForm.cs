﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Circle
{
    public partial class TextForm : Form
    {
        public event EventHandler EditingCompeled;
        public event EventHandler EditingEscaped;
        private string resulttext;

        public TextForm(string text,Font font)
        {
            InitializeComponent();
            textBox1.Text = text;
            textBox1.Font = font;
            this.Height = textBox1.Height;

        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                resulttext = textBox1.Text;
                EditingCompeled(resulttext,new KeyEventArgs(Keys.Enter));
               // this.Close();
            }
            if (e.KeyData == Keys.Escape)
            {
                EditingEscaped(sender, e);
               // this.Close();
                
            }
        }

        
    }
}
