﻿using System;
using System.Windows.Forms;


namespace Circle
{
    public partial class CurclePluginControl : System.Windows.Forms.UserControl
    {
        #region from CirclePluginControl.Designer.cs
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 52);
            this.button1.TabIndex = 0;
            this.button1.Text = "Выбрать шрифт";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // fontDialog1
            // 
            this.fontDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fontDialog1.FontMustExist = true;
            // 
            // label1
            // 
            this.label1.AutoEllipsis = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.label1.Location = new System.Drawing.Point(0, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 221);
            this.label1.TabIndex = 0;
            this.label1.Text = "Съешь еще этих мягких французких булочек да выпей чаю";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.button2.Location = new System.Drawing.Point(85, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 52);
            this.button2.TabIndex = 1;
            this.button2.Text = "Опции";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // CurclePluginControl
            // 
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Name = "CurclePluginControl";
            this.Size = new System.Drawing.Size(170, 276);
            this.ResumeLayout(false);

        }

        #endregion
        #endregion

        private FontDialog fontDialog1;
        private Button button1;
        private Label label1;
        private Button button2;
       
        private Options opts;
        private System.Drawing.Font font;
        public ToolForm ToolForm;
        
        public event EventHandler FontChanged;
        public event EventHandler ControlOptionsChanged;
        
        public CurclePluginControl(Options opts, System.Drawing.Font font)
        { 
            InitializeComponent();
            this.opts = opts;
            this.font = font;
            label1.Font = font;
            ToolForm_Disposed(new Object(), new EventArgs());
            
        }

        void ToolForm_Disposed(object sender, EventArgs e)
        {
            ToolForm = new ToolForm(opts);
            ToolForm .ToolOptionsChanged +=new EventHandler(ToolForm_ToolOptionsChanged);
            ToolForm.Disposed+=new EventHandler(ToolForm_Disposed);
        }

        void ToolForm_ToolOptionsChanged(object sender, EventArgs e)
        {
            this.opts = (Options)sender;
            //MessageBox.Show("ToolForm_OptionsChanged "+opts.Shadow.ToString()+' '+opts.ShadowSize.ToString());
            ControlOptionsChanged(sender, e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {    
                    font = fontDialog1.Font;              
                    label1.Font = font;
                    FontChanged(font,new EventArgs());
                }
                catch { }
            }   
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ToolForm.Location = Cursor.Position;
            if (!ToolForm.Visible) ToolForm.Show(this);           
            ToolForm.BringToFront();
        }

       
    }
}
