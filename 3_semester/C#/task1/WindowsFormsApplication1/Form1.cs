﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace wfa1
{
    
    public partial class Form1 : Form
    {
        MDate date= new MDate();
       
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //date.Day = 12;
            //date.Month  = 3;
            //date.Year = 4;            
            //date.DateToDays();
            date.Days = 734416;
            date.DaysToDate();
            show();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = date.format(0);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = date.format(1);
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            textBox1.Text = date.format(2);
        }
   
        private void show()
        {          
            textBox2.Text = date.Days.ToString();
            textBox1.Text = date.format(GetFormat());
            label1.Text = date.DayName();
        }
        private void SetDays()
        {        
            if (textBox2.Text == "")
                date.Days = 0;
            else
            {
                int  temp = Int32.Parse(textBox2.Text);
                if (temp <= 1)
                    date.Days = 1;
                else
                    date.Days = temp;
            }
        }        
        private int GetFormat()
        {
            if (radioButton1.Checked)
                return 0;
            else
                if (radioButton2.Checked)
                    return 1;
                else
                    if (radioButton3.Checked)
                        return 2;
                    else
                        return 0;
        }
        private void showLeapYear(bool flag)
        {
            if (flag)
                MessageBox.Show("Високосный год", "Проверка завершена");
            else
                MessageBox.Show("Год не високосный", "Проверка завершена");              
        }

        private void button1_Click(object sender, EventArgs e)
        {
          //  date.SetDate(textBox1.Text);
          //  radioButton1.Checked = true;
            date.DateToDays();
            show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SetDays();
            date.DaysToDate();
            show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                date.movedate(Int32.Parse(f.textBox1.Text));
                show();
            }
            f.Dispose();             
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
           
            string s="";  
            if (f.ShowDialog(this) == DialogResult.OK)
            {         
                s = date.DaysOfIMonth(Int32.Parse(f.textBox1.Text)).ToString();
                if (s[s.Length - 1] == '1')
                    s = s + " день.";
                else
                    s = s + " дней.";
                MessageBox.Show("В " + f.textBox1.Text + " месяце " + s);
            }
          // else
          //      MessageBox.Show("Wrong parameter!");
            f.Dispose();
            
        }
             
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void checkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showLeapYear(date.CheckLeapYear(date.Year));
        }

        private void typeYearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f=new Form2();
            
            if (f.ShowDialog(this) == DialogResult.OK)
                showLeapYear(date.CheckLeapYear(Int32.Parse(f.textBox1.Text)));
           // else
           //     MessageBox.Show("Wrong parameter!");                      
        }

     
       
       

      
    }
}
