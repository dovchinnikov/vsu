﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wfa1
{
    public class MDate
    {
        private int c_day;
        private int c_month;
        private int c_year;
        private int c_days;

        private const int DD_MM_YYYY=0;
        private const int MM_DD_YYYY = 1;
        private const int YYYY_MM_DD = 2;

        private static int[] dm={31,28,31,30,31,30,31,31,30,31,30,31,29};

        public int Year
        {
            get
            {
                return c_year;
            }
            set
            {
                c_year = value;
            }
        }
        public int Month
        {
            get
            {
                return c_month;
            }
            set
            {
                c_month = value;
            }
        }
        public int Day 
        {
            get
            {
                return c_day;            
            }
            set
            {
                c_day = value;
            }

        }
        public int Days
        {
            get
            {
                return c_days;
            }
            set
            {
                c_days = value;
            }
        }

        public bool CheckLeapYear(int year)
        {
            if (year % 4 == 0)
            {
                if (year % 100 == 0)
                {
                    if (year % 400 != 0)
                        return false;
                    else
                        return true;
                }
                else
                    return true;
            }
            else
                return false;            
        }

        public void DateToDays()
        {
            int month = c_month;
            int year = c_year;            
            c_days = 0;             
            for (int i = 0; i < year; i++)
            {
                c_days += 365;
                if (CheckLeapYear(i))
                    c_days++;
            }
            for (int i = 0; i < month - 1; i++)
                c_days += dm[i];
            if ((CheckLeapYear(year)) && (month > 1))
                c_days++; 
            c_days +=c_day;               
        }
        
        public void DaysToDate()
        {
            int days = c_days;       
            int month = 0;
            int year = 0;
            bool flag = true;   //флаг остановки цикла
        
            if ((days <= 365)||((year%4==0)&&(days==366)))     //установка флага
                flag = false;
         
            while (flag)
            {
                if (CheckLeapYear(year))      
                    days = days - 366;
                else
                    days = days - 365;
                year++;
                if ((days <= 365) || ((year % 4 == 0) && (days == 366)))     //установка флага
                    flag = false;
            }
            while (days > dm[month])
            {
                if (month == 1)
                {
                    if (CheckLeapYear(year))
                    {
                        if (days == dm[12])
                        {
                            break;
                        }
                        else
                            days = days - dm[12];
                    }
                    else
                        days = days - dm[1];
                }
                else
                    days = days - dm[month];
                month++;
            }
            month++;
            c_day = days;
            c_month = month;
            c_year = year;
        }

        public void movedate(int d_moving)
        {
            c_days += d_moving;
            DaysToDate();
        }

        public string format(int key)
        {
            string day;
            string month;
            string year;
            day = c_day.ToString();
            month = c_month.ToString();
            year = c_year.ToString();
            if (day.Length == 1)
                day = '0' + day;
            if (month.Length == 1)
                month = '0' + month;
            if (year.Length == 1)
                year = "000" + year;
            if (year.Length == 2)
                year = "00" + year;
            if (year.Length == 3)
                year = "0" + year;
            switch (key)
            {
                case DD_MM_YYYY: return day + '.' + month + '.' + year;

                case MM_DD_YYYY: return month + '.' + day + '.' + year;

                case YYYY_MM_DD: return year + '.' + month + '.' + day;
                default: return "wrong date key";

            }
        }              

        public string DayName()
        {
            switch ((c_days-2) % 7)
            {
                case 1: return "Понедельник";
                case 2: return "Вторник";
                case 3: return "Среда";
                case 4: return "Четверг";
                case 5: return "Пятница";
                case 6: return "Суббота";
                default: return "Воскресенье";
            }
        }

        public int DaysOfIMonth(int t)
        {
            int month = c_month+t;
            int year = c_year;
            year = year + (month-1) / 12;
            month = (month-1) % 12 + 1;
            if (month == 2)
                if (CheckLeapYear(year))
                    return dm[12];
                else
                    return dm[month-1];
            else
                return dm[month-1];
        }

        public void SetDate(string s)
        {
            string ss = "";
            int k=0;
            bool flag = false;
            for (int i = 0; i < s.Length; i++)
            {
                if ((s[i] == '.') || (i == s.Length - 1))
                {
                    k++;
                    switch (k)
                    {
                        case 1: c_day = Int32.Parse(ss); break;
                        case 2: c_month = Int32.Parse(ss); break;
                        case 3: c_year = Int32.Parse(ss); break;
                        default: break;
                    }
                    ss = "";
                    flag = false;
                }
                else
                {
                    if (s[i] != '0')
                    {
                        flag = true;
                        s = s + s[i];
                    }
                    else
                    {
                        if (flag)
                            ss = ss + s[i];
                    }
                }

            }
            
        }

    }
}