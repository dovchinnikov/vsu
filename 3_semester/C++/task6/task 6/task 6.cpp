// task 6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

typedef int vType;

typedef struct element
{
	int number;
	vType value;
	struct element* prev;
	struct element* next;
}elem,*pelem;

typedef struct 
{
	int count;
	pelem head;
   
} gLst, *pLst;

pLst newList(int count);
void addElem(pLst somelist, int addafter,vType value); 
void delElem(pLst somelist, int numberof);
elem getElem(pLst fromlist, int numberof);
vType setElem(pLst somelist, int numberof, vType newvalue); 
void printList(pLst somelist);
void printList(pLst somelist, int from, int to);
void copyLists(pLst fromlist, pLst tolist);


int _tmain(int argc, _TCHAR* argv[])
{
	pLst mylist = newList(10);
	if (mylist == NULL)
	{
		return 0;
	}
	printList(mylist,0,9);
    
	addElem(mylist,2,123);
	printList(mylist);

	delElem(mylist,3);
	printList(mylist);

	pLst secondlist = newList(6);
	copyLists(secondlist,mylist);
	printList(secondlist);
	printList(mylist);
	
	cout<<getElem(mylist,1).value<<'\n'<<'\n';

	cout<<setElem(mylist,0,100)<<'\n'<<'\n';
	printList(mylist);
	
	return 0;
}
/*
elem newElem()
{
	elem element;
	element.number =0;
	element.value = element.number*2;
	element.prev = &element;
	element.next = &element;
	return element;
}
*/

pLst newList(int count)
{
	if (count<=1)
	{
		cout<<"wrong count"<<'\n';
		return NULL;
	}

	pelem current = new elem();
    pelem next;

	pLst cList = new gLst();
	cList->count = count;
	cList->head = current;
    
	current->number =0;
	current->value = current->number *2;
	current->prev =current;
    
	for (int i=1;i<count;i++)
	{		
		next = new elem(); 
		next->number=i;
        next->value = next->number *2;
        next->prev = current;
		current->next = next;
		current= current->next;
	}
    
	current->next=cList->head;
    cList->head->prev=current;
    return cList;
}

void addElem(pLst somelist,int addafter,vType value)
{
    if (somelist->count <=addafter)
	{
		cout<<"wrong addafter"<<'\n';
		return;
	}
	somelist->count++;

    pelem current = somelist->head;
    
	for (int i=0;i<addafter;i++)
	    current=current->next;
    
	pelem temp = new elem();
    
    temp->value = value;
    temp->number = current->number;
    temp->next = current->next;
    temp->prev =current;
	
	current->next->prev = temp;
	current->next = temp;
    current=current->next;
	
	while (current != somelist->head)
	{
		current->number ++;
		current=current->next;
	}
}

void delElem(pLst somelist, int numberof)
{
	somelist->count--;
	pelem current = somelist->head;
	for (int i=0;i<numberof;i++)
		current=current->next;	
	if (numberof==0)	
		somelist->head = somelist->head ->next;
	
	current->prev->next = current->next;
	current->next->prev = current->prev;

	pelem temp=current;
	current= current->next;

	do
	{
		current->number--;
		current=current->next;
	}
	while (current!=somelist->head);
    delete(temp);
}


void printList(pLst somelist)
{
	if (somelist->count ==0)
	{
		cout<<"empty list"<<'\n';
		return;
	}
	cout<<"count = "<<somelist->count<<'\n';
	pelem temp=somelist->head;
	for (int i=0;i<somelist->count;i++)
	{
		cout<<temp->prev<<' '<<temp<<' '<<temp->next <<' '<<temp->number <<' '<<temp->value<<'\n';
		temp= temp->next;
	}
	cout<<'\n'<<'\n';
}

void printList(pLst somelist, int from, int to)
{
	pelem temp=somelist->head;
    for (int i=0; i<from; i++)
	{
        temp=temp->next;
	}
    cout<<"count = "<<somelist->count<<'\n';
	for (int i=from;i<to+1;i++)
	{
		cout<<temp->prev<<' '<<temp<<' '<<temp->next <<' '<<temp->number <<' '<<temp->value<<'\n';
		temp= temp->next;
	}
	cout<<'\n'<<'\n';
}

elem getElem(pLst fromlist, int numberof)
{
	pelem result = fromlist->head;
	for (int i=0;i<numberof;i++)
		result=result->next;
	return *result;
}

vType setElem(pLst somelist, int numberof, vType newvalue)
{
	pelem current = somelist->head;
	for (int i=0;i<numberof;i++)
		current=current->next;
	vType res = current->value;
	current->value = newvalue;
	return res;
}

void copyLists(pLst fromlist, pLst tolist)
{
	int i=0;
	pelem from,to;
	from = fromlist->head;
	to = tolist->head;
	while (i<fromlist->count&&i<tolist->count)
	{
		to->value = from->value;
		to=to->next;
		from=from->next;
		i++;
	}

}