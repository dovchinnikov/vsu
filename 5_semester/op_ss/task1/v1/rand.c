#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	if (argc<4)
	{
		printf("Too less parameters!\n");
		exit(0);
	}
	int i;
	int r;
	int num = atoi(argv[2]);
	int max = atoi(argv[3]);
	FILE *f;
	f = fopen(argv[1],"w");
	if (f==NULL)
	{
		printf("No such file!\n");
		exit(0);
	}
	for (i=0; i<num;i++)
	{
		r=rand()%max+1 ;	
		//	printf(" %d\n",r);
		fwrite(&r,1,sizeof(int),f);
	}
	fclose(f);
	printf("File writed and closed\n");
	return 0;
}
