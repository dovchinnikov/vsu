#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define _MAX 2000000
#define _BLOCK 1000
#define _THREADS 5
#define null NULL								//задолбала ошибка компиляции

void insertionSort(int arr[], int length);

void *blockThread(void *arg);

extern void uni(char *outfile,char *m_block, int k);

extern void insertionSort(int arr[], int length); 

typedef struct									//структура для передачи параметров в новый поток
{
	int number;								//номер блока
	int *a;									//адрес начала массива
	int len;								//длина массива
} param_t;

sem_t *semaphore;

int main(int argc, char *argv[])
{
	if (argc<3)
	{
		printf("Too less parameters\n");
		exit(0);
	}
	FILE* fd;								//исходный файл
	fd = fopen(argv[1],"r");						//открытие
	if (fd == NULL)
	{		
		printf("Error opening file\n");
		exit(1);
	}	
	else
	{
		printf("File opened\n");
	}
//----------------------------------------------------

	int *arr;								//память под блок элементов
	param_t *param;								//структура параметров
	int read;								//количество прочитанных элементов
	int k=0;								//счетчик блоков
	int s_value;								//номер нового потока в списке текущих потоков
	int i;									//просто счетчик для циклов	
	pthread_t *thread;
	semaphore=(sem_t *)malloc(sizeof(sem_t)); 
	sem_init(semaphore,0,_THREADS);
	sem_getvalue(semaphore,&s_value);
	printf("semaphore value is %d\n",s_value);
	while (1)
	{
		arr  = (int *)malloc(_BLOCK*sizeof(int));			//память под новый блок
		param = (param_t *)malloc(sizeof(param_t));			//память под новую структуру параметров
		read = fread(arr, sizeof(int),_BLOCK,fd);			//читаем блок в память	
		if (read!=0)							//срабатывает когда конец файла
		{
			param->number = k;					//заполняем структуру
			param->a = arr;		
			param->len = read;
//			printf("%d items read\n",read);
 			sem_wait(semaphore);
			sem_getvalue(semaphore,&s_value);
			printf("semaphore value is %d\n",s_value);
			pthread_create(thread,NULL,blockThread,param);//ну тут все ясно
			k++;							
		} 
		else
			break;
	} 
	if (fclose(fd)!=0)							//закрываем исходный файл
		printf("Error closing file\n");
	else
		printf("File closed\n");
	while(s_value!=_THREADS)
		sem_getvalue(semaphore,&s_value);
	sem_destroy(semaphore);
	uni(argv[2],"block",k);

//	return 0;
}

void *blockThread(void *arg)
{
	param_t *p = (param_t *)arg; 			//превращаем в структуру параметров
	int p_num = p->number;				//вытаскиваем номер блока
	int *d =p->a;					//вытаскиваем адрес начала массива элементов в памяти
	int len =p->len; 				//вытаскиваем количество элементов
	printf("start sorting block %d\n",p_num);
	insertionSort(d,len);				//сортируем блок элементов
	printf("  block %d sorted\n",p_num);
//	int i;						//это счетчик
//	for (i=0;i<len;i++)				
//		printf("%d -> %d\n",p_num,d[i]);	
	char fname[8];					//имя файла, в который помещается блок
	sprintf(fname, "block%d",p_num);
	FILE *f = fopen(fname,"w+");
	fwrite(d,len,sizeof(int),f);			//скидываем блок элементов в файл с названием fname
	fclose(f);
	printf("    block %d writed\n",p_num);
	free(d);					//очищаем память из-под блока элементов
	free(p);					//очищаем память из-под структуры параметров
	int s_value;
//	sem_getvalue(semaphore,&s_value);
//	printf("semaphore value is %d\n",s_value);
	sem_post(semaphore);
//	sem_getvalue(semaphore,&s_value);
//	printf("semaphore value is %d\n",s_value);
	pthread_exit(null);
}


