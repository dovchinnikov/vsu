#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
	if (argc<2)
	{
		printf("Too less parameters!\n");
		exit(0);
	}
	
	FILE *f;
	f = fopen(argv[1],"r");
	if (f==NULL)
	{
		printf("No such file!\n");
		exit(0);
	}
	int e;
	int i=0;
	printf("   %s:\n",argv[1]);
	if (argv[2])
	{
		int t=atoi(argv[2]);
		for (i=0;i<t;i++)
		{
			printf("%d -> %d\n",i,e);
			fread(&e,sizeof(int),1,f);
		}
	}
	else
		while (fread(&e,sizeof(int),1,f)!=0)
		{	i++;		
			printf("%d -> %d\n",i,e);
		}
	fclose(f);
}
