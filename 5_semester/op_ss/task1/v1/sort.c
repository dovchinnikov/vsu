#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define _MAX 2000000
#define _BLOCK 1000
#define _THREADS 6
#define null NULL								//задолбала ошибка компиляции

void insertionSort(int arr[], int length);

void *blockThread(void *arg);

extern void uni(char *outfile,char *m_block, int count);

extern void insertionSort(int arr[], int length); 

typedef struct									//структура для передачи параметров в новый поток
{
	int number;								//номер блока
	int *a;									//адрес начала массива
	int len;								//длина массива
} param_t;

int main(int argc, char *argv[])
{
	if (argc<3)
	{
		printf("Too less parameters\n");
		exit(0);
	}
	FILE* fd;								//исходный файл
	fd = fopen(argv[1],"r");						//открытие
	if (fd == NULL)
	{		
		printf("Error opening file\n");
		exit(1);
	}	
	printf("File opened\n");
//	lockf((int)*fd,F_LOCK,NULL);	
//----------------------------------------------------

	pthread_t *threads = (pthread_t *)malloc(_THREADS*sizeof(pthread_t));	//массив текущих потоков
	int *arr;								//память под блок элементов
	param_t *param;								//структура параметров
	int read;								//количество прочитанных элементов
	int k=0;								//счетчик блоков
	int c_thread;								//номер нового потока в списке текущих потоков
	int i;									//просто счетчик для циклов	

	while (!feof(fd))
	{
		arr  = (int *)malloc(_BLOCK*sizeof(int));			//память под новый блок
		param = (param_t *)malloc(sizeof(param_t));			//память под новую структуру параметров
		read = fread(arr, sizeof(int),_BLOCK,fd);			//читаем блок в память	
		if (read!=0)							//срабатывает когда конец файла
		{
			param->number = k;					//заполняем структуру
			param->a = arr;		
			param->len = read;
			c_thread = k%_THREADS;					//номер потока в массиве текущих потоков
//			printf("%d items read\n",read);
			if (k>=_THREADS) pthread_join(threads[c_thread],null);  //если занят ждем пока отработает 
			pthread_create(&threads[c_thread],NULL,blockThread,param);//ну тут все ясно
			k++;							
		} 
	} 
	printf("          k = %d\n          c_thread=%d\n\n",k,c_thread);
	if (fclose(fd)!=0)							//закрываем исходный файл
		printf("Error closing file\n");
	else
		printf("File closed\n");
	if (k<=_THREADS)
		for (i=0;i<k;i++) pthread_join(threads[i],null);
	else 
		for (i=0;i<_THREADS;i++) pthread_join(threads[i],null);
	free(threads);
		
	uni(argv[2],"block",k);
	return 0;
}

void *blockThread(void *arg)
{
	param_t *p = (param_t *)arg; 			//превращаем в структуру параметров
	int p_num = p->number;				//вытаскиваем номер блока
	int *d =p->a;					//вытаскиваем адрес начала массива элементов в памяти
	int len =p->len; 				//вытаскиваем количество элементов
	printf("start sorting block %d\n",p_num);
	insertionSort(d,len);				//сортируем блок элементов
	printf("  block %d sorted\n",p_num);
//	int i;						//это счетчик
//	for (i=0;i<len;i++)				
//		printf("%d -> %d\n",p_num,d[i]);	
	char fname[8];					//имя файла, в который помещается блок
	sprintf(fname, "block%d",p_num);
	FILE *f = fopen(fname,"w+");
	fwrite(d,len,sizeof(int),f);			//скидываем блок элементов в файл с названием fname
	fclose(f);
	printf("    block %d writed\n",p_num);
	free(d);					//очищаем память из-под блока элементов
	free(p);					//очищаем память из-под структуры параметров
	pthread_exit(null);
}


