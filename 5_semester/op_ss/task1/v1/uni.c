#include <stdio.h>
#include <stdlib.h>

typedef struct
{
	FILE* f;
	int closed;
} block;

void uni(char *outfile,char *m_block, int k)
{
	printf("Starting union blocks\n");
	int i;
	printf("parameters %s %s %d\n",outfile,m_block,k);
	FILE *r_file = fopen(outfile,"w");
	if (r_file == NULL)
		exit(1);
	block *blocks= (block *)malloc(sizeof(block)*k);
	char fname[10];
	for (i=0;i<k;i++)
	{
		sprintf(fname,"%s%d",m_block,i);
//		printf("%s\n",fname);
		blocks[i].f = fopen(fname,"r");
		if (blocks[i].f==NULL)
		{
			printf("kosjak bleat`\n");
			free(blocks);
			exit(1);
		}
		blocks[i].closed=0;
	}
	int nums[k];
	int min=0;
	for (i=0;i<k;i++)
	{
		fread(&nums[i],sizeof(int),1,blocks[i].f);
	}
	while(1)
	{
		for (i=0;i<k;i++)
			if (blocks[i].closed==0 && nums[i]<nums[min]) min=i;
//		printf("%d -> %d\n",min,nums[min]);
		fwrite(&nums[min],sizeof(int),1,r_file);
		if(fread(&nums[min],sizeof(int),1,blocks[min].f)==0)
		{
			fclose(blocks[min].f);
			blocks[min].closed=1;
			min = findfirst(blocks,k);
			if (min==-1)
				break;	
		}
	}
	for (i=0;i<k;i++)
	{
		sprintf(fname,"%s%d",m_block,i);
		unlink(fname);
	}
	printf("Union completed\n");
}

int findfirst(block blocks[], int l)
{
	int i;
	for (i=0;i<l;i++)
		if (blocks[i].closed==0) return i;
	return -1;
}
