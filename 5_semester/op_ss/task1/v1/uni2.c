#include <stdio.h>
#include <stdlib.h>
#define _MAX_FILES 60

typedef struct
{
	FILE* f;
	int closed;
} block;

typedef struct
{
	block *blocks;
	int length;
} uniN_params;

void uniN(block *blocks,char *outfile,int num_outfile,int k);

void uni(char *outfile,char *m_block, int count)
{
	printf("Starting union %d blocks\n",count);
	printf("parameters: %s %s %d\n",outfile,m_block,count);
	char fname[10];							//старое имя файла
	char fname_n[10];						//новое имя файла
	block *bs;			//
	int i;								//счетчик
	int j;								//стетчик блоков за текущий заход
	int k;								//счетчик блоков сквозной за все заходы
	int b;								//счетчик заходов
	int c_count = count;						//текущее количество обрабатываемых блоков
	while (c_count>1)
	{
		b=0;
		k=0;
		printf ("b=%d   k=%d   c_count=%d \n",b,k,c_count);
		getc(stdin);
		while(k<c_count)
		{
			j=0;
			bs= (block *)malloc(sizeof(block)*_MAX_FILES);
			while (j<_MAX_FILES && k<c_count)
			{
				sprintf(fname,"%s%d",m_block,k);
				printf("%s\n",fname);
				bs[j].f = fopen(fname,"r");
				bs[j].closed=0;
				j++;
				k++;
			}
			uniN(bs,m_block,b,j);
			free(bs);
			for (i=b*_MAX_FILES;i<b*_MAX_FILES+j;i++)
			{
				sprintf(fname,"%s%d",m_block,i);
				unlink(fname);
			}
			b++;
		}
		for (i=0;i<b;i++)
		{
			sprintf(fname,"_%s%d",m_block,i);
			sprintf(fname_n,"%s%d",m_block,i);
			rename(fname,fname_n);
		}
		c_count=b;
	}

	
	printf("Union completed\n");
}

int findfirst(block blocks[], int l)
{
	int i;
	for (i=0;i<l;i++)
		if (blocks[i].closed==0) return i;
	return -1;
}

void uniN(block *blocks,char *outfile,int num_outfile,int k)
{
	printf("get %d blocks to uni\n",k);
	printf("get parameters: blocks=%p outfile=%s num_outfile=%d k=%d\n",blocks,outfile,num_outfile,k);
//	getc(stdin);
	char r_file_name[10];
	sprintf(r_file_name,"_%s%d",outfile,num_outfile);
	FILE *r_file = fopen(r_file_name,"w");
	if (r_file == NULL)
		exit(1);
	int nums[k];
	int min=0;

	int i;
	for (i=0;i<k;i++)
	{
		fread(&nums[i],sizeof(int),1,blocks[i].f);
	}
	while(1)
	{
		for (i=0;i<k;i++)
			if (blocks[i].closed==0 && nums[i]<nums[min]) min=i;
//		printf("%d -> %d\n",min,nums[min]);
		fwrite(&nums[min],sizeof(int),1,r_file);
		if(fread(&nums[min],sizeof(int),1,blocks[min].f)==0)
		{
			fclose(blocks[min].f);
			blocks[min].closed=1;
			min = findfirst(blocks,k);
			if (min==-1)
				break;	
		}
	}
}
