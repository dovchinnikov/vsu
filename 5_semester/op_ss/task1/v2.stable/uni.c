#include <stdio.h>
#include <stdlib.h>
#include "appconfig.h"

typedef struct
{
	int pos;
	int size;
	int eob;
} block;

void uni(char *outfile, int k, int l)
{
	printf("Starting union blocks\n");
	int i;
	FILE *in_file = fopen(_TEMP_FILE,"r");
	FILE *out_file = fopen(outfile,"w");
	block *b = (block *)malloc(sizeof(block)*k);
	int nums[k];
	for (i=0;i<k;i++)
	{
		b[i].pos = 0;
		b[i].size=(i*_BLOCK+_BLOCK>l)?l-i*_BLOCK:_BLOCK;
		b[i].eob= 0;
		fseek(in_file,i*_BLOCK*sizeof(int),SEEK_SET);
		fread(&nums[i],sizeof(int),1,in_file);
		b[i].pos++;
	}
	int min=0;
	while(1)
	{
//		for (i=0;i<k;i++)								//to enable trace uncomment these two lines
//			printf("  %d -> %d, pos %d, eob %d\n", i, nums[i],b[i].pos,b[i].eob);
		for (i=0;i<k;i++)
			if (b[i].eob==0 && nums[i]<nums[min]) min=i;
		fwrite(&nums[min],sizeof(int),1,out_file);
		if(b[min].pos<b[min].size)
		{
			fseek(in_file,(min*_BLOCK+b[min].pos)*sizeof(int),SEEK_SET);
			fread(&nums[min],sizeof(int),1,in_file);
			b[min].pos++;
		}
		else
		{		
			b[min].eob=1;
			min = findfirst(b,k);
			if (min==-1) break;
		}		
	}
	free(b);
	unlink(_TEMP_FILE);
	printf("Union completed\n");
}

int findfirst(block blocks[], int l)
{
	int i;
	for (i=0;i<l;i++)
		if (blocks[i].eob==0) return i;
	return -1;
}
