#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#include "appconfig.h"
								
void *blockThread(void *arg);

extern void uni(char *outfile, int count, int length);

extern void insertionSort(int arr[], int length); 

FILE *out_file;
FILE *in_file;

volatile int k;
volatile int length;

pthread_mutex_t mutex_in;
pthread_mutex_t mutex_out;
typedef struct								
{
	int number;							
} param_t;

int main(int argc, char *argv[])
{
	k=0;
	length=0;
	pthread_mutex_init(&mutex_in,NULL);
	pthread_mutex_init(&mutex_out,NULL);
	if (argc<3)
	{
		printf("Too less parameters\n");
		exit(0);
	}								//
	in_file = fopen(argv[1],"r");
	if (in_file == NULL)
	{		
		printf("Error opening file\n");
		exit(1);
	}	
	printf("File opened\n");
	
	out_file = fopen(_TEMP_FILE,"w");
	
//----------------------------------------------------

	pthread_t *threads = (pthread_t *)malloc(_THREADS*sizeof(pthread_t));	//массив текущих потоков
	param_t *param;
	int i;
	for (i=0;i<_THREADS;i++)
	{
		param = (param_t *)malloc(sizeof(param_t));
		param->number = i;
		pthread_create(&threads[i],NULL,blockThread,param);
	} 
	printf("All %d threads created!\n",_THREADS);
	for (i=0;i<_THREADS;i++) pthread_join(threads[i],null);
	free(threads);
	
	if (fclose(in_file)!=0)	printf("Error closing file\n");
	else 			printf("File closed\n");
	fclose(out_file);
	pthread_mutex_destroy(&mutex_in);
	pthread_mutex_destroy(&mutex_out);	
//	getc(stdin);
	printf("k = %d\n",k);
	printf("length = %d\n",length);
	uni(argv[2],k,length);
	return 0;
}

void *blockThread(void *arg)
{
	param_t *p = (param_t *)arg; 			
	int p_num = p->number;				
	printf("starting thread %d\n",p_num);
	int *block;
	int l;
//	int i;
	while (!feof(in_file))
	{
		block = (int *)malloc(sizeof(int)*_BLOCK);
		pthread_mutex_lock(&mutex_in);
		l = fread(block,sizeof(int),_BLOCK,in_file);
		pthread_mutex_unlock(&mutex_in);
		if (l==0) break;
		insertionSort(block,l);
		pthread_mutex_lock(&mutex_out);
		fwrite(block,sizeof(int),l,out_file);
//		for (i=0;i<l;i++)				
//			printf("%d -> %d\n",p_num,block[i]);
		k++;
		length+=l;
		pthread_mutex_unlock(&mutex_out);			
		free(block);
	}
	free(p);
	pthread_exit(null);
}


