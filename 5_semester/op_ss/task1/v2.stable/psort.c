#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "appconfig.h"

void blockProcess(int num);
extern void insertionSort(int arr[], int length);
extern void uni(char *outfile, int k, int l);

int *pp;
FILE * in_file;

int main(int argc, char *argv[])
{
	if (argc<3)
		exit(0);
//----------------- creating descriptors
	pp = malloc(sizeof(int)*2);
	pipe(pp);
	in_file = fopen(argv[1],"r");
	
//----------------- getting filesize
	struct stat *f_stat =(struct stat *)malloc(sizeof(struct stat));
	fstat(fileno(in_file),f_stat);
	int filesize = (int)f_stat->st_size;
	printf("filesize %d\n",filesize);
	free(f_stat);

//----------------- writing numbers of block to pipe
	int i=0;
	while (i*_BLOCK*4<filesize)
	{
		write(pp[1],&i,sizeof(int));
		i++;
	}
	int blocks = i;
	i = _PIPE_END;
	write(pp[1],&i,sizeof(int));

//---------------- starting to fork 
	printf("i`m parent\n");
	printf("my pid=%d, my ppid=%d\n",getpid(),getppid());
	for (i=0;i<_PROCS;i++)
	{
		if (fork()==0) blockProcess(i);
	}
	close(pp[1]);
	close(pp[0]);
	fclose(in_file);
	wait(null);
//---------------- union blocks
	int elements = filesize/sizeof(int);
	printf("elements count is %d\n",elements);
	printf("blocks count is %d\n",blocks);
	printf("press any key to continue...\n");
	getc(stdin);
	uni(argv[2],blocks,elements);
	printf("ololo\n");
}


void blockProcess(int num)
{
	int b;
	int l;
	int i;
	FILE *temp_file;
	int * block = (int *)malloc(sizeof(int)*_BLOCK);

	read(pp[0],&b,sizeof(int));
	printf("%d process read %d from pipe\n",num,b);
	while (b!=_PIPE_END)
	{		
		fseek(in_file,_BLOCK*b*sizeof(int),SEEK_SET);
		l =  fread(block,sizeof(int),_BLOCK,in_file);
//		printf("fread returns %d\n",l);
		if (l==0) exit(0);
		insertionSort(block,l);
//		printf("%d block length is %d:\n",b,l);
//		for (i=0;i<l;i++) printf("%d element is %d\n",i,block[i]);

//		printf("%d process will write %d elements of %d block\n",num,l,b);
		temp_file = fopen(_TEMP_FILE,"a");	
		fwrite(block,sizeof(int),l,temp_file);
		fclose(temp_file);
//		printf("%d process finished writing %d elements of %d block\n",num,l,b);
		
		read(pp[0],&b,sizeof(int));
		printf("%d process read %d from pipe\n",num,b);
	}
	write(pp[1],&b,sizeof(int));
	close(pp[1]);
	close(pp[0]);
	exit(0);
}
