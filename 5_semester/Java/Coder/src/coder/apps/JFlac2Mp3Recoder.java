package coder.apps;

import coder.utils.Config;
import coder.utils.ConfigLoader;
import coder.utils.FlacToRawDecoder;
import coder.utils.RawToMp3Encoder;

public class JFlac2Mp3Recoder {

	public static void main(String[] args){
		if (args.length<1) System.exit(1);
		try {
			System.out.println("recoding started...");
			Config ccc = ConfigLoader.getConfig(args[0]);
			RawToMp3Encoder encoder = new RawToMp3Encoder(ccc.getMp3_filename());
			encoder.setBitrate(ccc.getBitrate());
			encoder.setSamplerate(ccc.getSamplerate());
			encoder.setMpgmode(ccc.getMode());
			encoder.Encode(new FlacToRawDecoder(ccc.getFlac_filename()).getRawAudioInputStream());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		System.out.println("done.");
	}
}
