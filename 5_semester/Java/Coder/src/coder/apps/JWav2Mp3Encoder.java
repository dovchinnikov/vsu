package coder.apps;

import coder.utils.RawToMp3Encoder;

public class JWav2Mp3Encoder {

	public static void main(String[] argv){
		if (argv.length<2){
			System.out.println("enter two filenames");
			System.exit(1);
		}
		RawToMp3Encoder encoder = new RawToMp3Encoder(argv[1]);
		System.out.println("encoding started...");
		try {
			encoder.Encode(argv[0]);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		} 
		System.out.println("done.");
	}
}
