package coder.utils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import net.sf.lamejb.LamejbConfig.MpegMode;

@XmlRootElement
public class Config {
	private String flac_filename;
	private String mp3_filename;
	private int samplerate;
	private int bitrate;
	private MpegMode mode;
	
	public String getFlac_filename() {
		return flac_filename;
	}
	@XmlElement
	public void setFlac_filename(String flac_filename) {
		this.flac_filename = flac_filename;
	}
	
	public String getMp3_filename() {
		return mp3_filename;
	}
	@XmlElement
	public void setMp3_filename(String mp3_filename) {
		this.mp3_filename = mp3_filename;
	}
	
	public int getSamplerate() {
		return samplerate;
	}
	@XmlElement
	public void setSamplerate(int samplerate) {
		this.samplerate = samplerate;
	}
	
	public int getBitrate() {
		return bitrate;
	}
	@XmlElement
	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}
	
	public MpegMode getMode() {
		return mode;
	}
	@XmlElement
	public void setMode(MpegMode mode) {
		this.mode = mode;
	}
	
}
