package coder.utils;

import java.io.File;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;

import org.kc7bfi.jflac.sound.spi.Flac2PcmAudioInputStream;
import org.kc7bfi.jflac.sound.spi.FlacAudioFileReader;
import org.kc7bfi.jflac.sound.spi.FlacFormatConversionProvider;

public class FlacToRawDecoder {

	private File file;
	
	public FlacToRawDecoder(String filename) {
		this.file = new File(filename);
	}
	public FlacToRawDecoder(File file) {
		this.file = file;
	}
	public AudioInputStream getRawAudioInputStream() throws Exception {
		AudioInputStream flacInputStream = new FlacAudioFileReader().getAudioInputStream(file);
		return new FlacFormatConversionProvider().getAudioInputStream(AudioFormat.Encoding.PCM_SIGNED, flacInputStream);
	}
	
	public AudioInputStream getRawAudioInputStream2() throws Exception {
		AudioInputStream flacData = new FlacAudioFileReader().getAudioInputStream(file);
		AudioFormat rawAudioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 44100.0F, 16, 2, 4, 44100.0F, false);
		return new Flac2PcmAudioInputStream(flacData, rawAudioFormat, 100);
	}
}
