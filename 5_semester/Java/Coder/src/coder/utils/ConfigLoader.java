package coder.utils;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ConfigLoader {
	
	public static Config getConfig(String filename) throws JAXBException {
		File c_file = new File(filename);
		JAXBContext jaxbContext = JAXBContext.newInstance(Config.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return (Config) jaxbUnmarshaller.unmarshal(c_file);
	}
}
