package coder.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import net.sf.lamejb.LameCodec;
import net.sf.lamejb.LamejbConfig;
import net.sf.lamejb.LamejbConfig.MpegMode;

public class RawToMp3Encoder {
	
	public static final int DEFAULT_SAMPLERATE = 44100;
	public static final int DEFAULT_BITRATE = 256;
	public static final MpegMode DEFAULT_MODE = MpegMode.STEREO;
	
	private File outfile;
	private int samplerate = DEFAULT_SAMPLERATE;
	private int bitrate = DEFAULT_BITRATE;
	private MpegMode mpgmode = DEFAULT_MODE;
	
	public RawToMp3Encoder(String filename){
		this.outfile = new File(filename);
	}
	
	public RawToMp3Encoder(File file){
		this.outfile = file;
	}
	
	public void Encode(AudioInputStream rawData) throws FileNotFoundException {
		FileOutputStream outStream = new FileOutputStream(outfile);
		LamejbConfig config = new LamejbConfig(samplerate,bitrate, mpgmode, false);
		new LameCodec().encodeStream(rawData, outStream, config);
	}
	public void Encode(String rawFileName) throws UnsupportedAudioFileException, IOException{
		Encode(AudioSystem.getAudioInputStream(new File(rawFileName)));
	}
	
	public MpegMode getMpgmode() {
		return mpgmode;
	}

	public void setMpgmode(MpegMode mpgmode) {
		this.mpgmode = mpgmode;
	}

	public int getBitrate() {
		return bitrate;
	}

	public void setBitrate(int bitrate) {
		this.bitrate = bitrate;
	}

	public int getSamplerate() {
		return samplerate;
	}

	public void setSamplerate(int samplerate) {
		this.samplerate = samplerate;
	}
}
