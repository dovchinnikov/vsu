package calc.processor;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import calc.buttons.CalcButton;
import calc.buttons.NumButton;
import calc.buttons.OperationButton;
import calc.utils.Operation;

public class Calculator implements MouseListener{
	
	private final String STARTSTRING = "0"; 
	private JTextField ta;
	private String fsNumber;				//first string number
	private String ssNumber;				//second string number
	private Operation operation;			//operation to compute
	private boolean firstdigit;				//indicates current digit is first or not
	
	public Calculator(JTextField ta) {
		this.ta = ta;
		RefreshCalc();
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		Component c = e.getComponent();
		String newdigit = ((CalcButton)c).getValueToPrint();
		if (c instanceof NumButton) {
			int digit = ((NumButton)e.getComponent()).getDigit();
			if (firstdigit) {
				if((digit == NumButton.DOUBLEZERO)||(digit == 0)) {
				} else {
					fsNumber = newdigit;
					firstdigit = false;
				}
			} else {
				if (this.operation == null) 
					fsNumber +=newdigit;
				else 
					ssNumber +=newdigit;
				firstdigit =false;
			}
		} else {
			Operation o = ((OperationButton)c).getOperation();	
			if (o == Operation.CANCEL) {
				RefreshCalc();
			} else if (firstdigit) { 
				if (o == Operation.MINUS) {
					firstdigit = false;
					fsNumber +=o;
				} else {
					if (fsNumber!= "") {
						this.operation = o;
						this.firstdigit = false;
					}
				}
			} else if (o == Operation.RESULT) {
				if (this.operation !=null) {
					computeCurrent();
					//firstdigit = true;
				}
			} else if (this.operation !=null && ssNumber!="") {
				computeCurrent();
				this.operation = o;
			} else {
				this.operation = o;
			} 
		}
		RefreshTextField();
	}

	private void computeCurrent() {
		if (fsNumber == "") 
			return;
		double n1 = Double.parseDouble(fsNumber);
		double n2;
		double res = 0;
		if (ssNumber =="") {
			if (this.operation == Operation.MULTIPLE)
				res = n1*n1;
			else
				return;
		} else {
			n2 = Double.parseDouble(ssNumber);
			switch (this.operation) {
				case DIVIDE: if (n2 == 0) {
					JOptionPane.showMessageDialog(null, "�� ���� ������ ������");
					RefreshCalc();
					return;
				} 
				res = n1/n2; 
				break;
				case PLUS: res = n1+n2; break;
				case MINUS: res = n1-n2; break;
				case MULTIPLE: res = n1*n2;	break; 	
			}
		}
		this.ta.setText(""+res);
		this.fsNumber = ta.getText();
		this.operation = null;
		this.ssNumber = "";
	}
	
	private void RefreshTextField() {
		if (fsNumber == "") {
			ta.setText(STARTSTRING);
			return;
		}
		String resultstring = "";
		resultstring+=fsNumber+(this.operation==null?"":this.operation+ssNumber);
		ta.setText(resultstring);
	}
	
	private void RefreshCalc() {
		this.fsNumber = "";
		this.ssNumber = "";
		this.firstdigit = true;
		this.operation = null;
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {}

}
