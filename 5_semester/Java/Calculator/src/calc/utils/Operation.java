package calc.utils;

public enum Operation {

	PLUS {
		public String toString() {
			return "+";
		}
	},
	MINUS {
		public String toString() {
			return "-";
		}
	},
	MULTIPLE {
		public String toString() {
			return "*";
		}
	},
	DIVIDE {
		public String toString() {
			return "/";
		}
	},
	RESULT {
		public String toString() {
			return "=";
		}
	},
	CANCEL {
		public String toString() {
			return "C";
		}
	}
}
