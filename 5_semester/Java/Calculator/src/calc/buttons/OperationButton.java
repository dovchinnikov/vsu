package calc.buttons;

import calc.processor.Calculator;
import calc.utils.Operation;

public class OperationButton extends CalcButton{
	
	private static final long serialVersionUID = -8269229318587007860L;
	
	private Operation operation;

	public OperationButton(Operation o, Calculator c) {
		super();
		this.operation = o;
		this.setText(o.toString());
		this.addMouseListener(c);
	}
		
	public Operation getOperation() {
		return this.operation;
	}
	
	public void setOperation(Operation o) {
		this.operation = o;
	}
	
	public String getValueToPrint() {
		return operation.toString();
	}
}
