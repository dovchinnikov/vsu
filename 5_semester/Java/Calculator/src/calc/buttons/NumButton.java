package calc.buttons;

import calc.processor.Calculator;

public class NumButton extends CalcButton{
	
	public final static int DOUBLEZERO = -1;
	public final static int POINT = -1234;
	
	private static final long serialVersionUID = -4396356755871846725L;

	private int digit;
	
	public NumButton(int i, Calculator c) {
		super();
		this.digit = i;
		this.setText(this.getValueToPrint());
		this.addMouseListener(c);
	}
	
	public int getDigit() {
		return this.digit;
	}

	public void setDigit(int digit) {
		this.digit = digit;
	}

	public String getValueToPrint() {
		switch (digit){
			case DOUBLEZERO: return "00";
			case POINT: return ".";
			default: return ""+digit;
		}
	}
}
