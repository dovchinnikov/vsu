package calc.buttons;

import javax.swing.JButton;

public abstract class CalcButton extends JButton{

	private static final long serialVersionUID = 7980847265099865057L;

	public abstract String getValueToPrint();
	
}
