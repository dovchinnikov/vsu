package calc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import calc.buttons.NumButton;
import calc.buttons.OperationButton;
import calc.processor.Calculator;
import calc.utils.Operation;

public class MainWindow extends JFrame {
	
	private static final long serialVersionUID = -8333760771436009073L;

	public MainWindow() {
		
		this.setTitle("Calculator");
		this.setSize(640, 480);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		JTextField textBox = new JTextField("0");
		textBox.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		textBox.setEditable(false);
		textBox.setHorizontalAlignment(JTextField.RIGHT);
		textBox.setFont(Font.decode("Arial-BOLD-18"));
		Calculator c = new Calculator(textBox);
		
		JPanel btPane = new JPanel(true);
		btPane.setLayout(new GridLayout(4,5,10,10));
//first row		
		btPane.add(new NumButton(7,c));
		btPane.add(new NumButton(8,c));
		btPane.add(new NumButton(9,c));
		btPane.add(new OperationButton(Operation.PLUS, c));
		btPane.add(new OperationButton(Operation.MINUS, c));
//second row		
		btPane.add(new NumButton(4,c));
		btPane.add(new NumButton(5,c));
		btPane.add(new NumButton(6,c));
		btPane.add(new OperationButton(Operation.MULTIPLE, c));
		btPane.add(new OperationButton(Operation.DIVIDE, c));
//third row		
		btPane.add(new NumButton(1,c));
		btPane.add(new NumButton(2,c));
		btPane.add(new NumButton(3,c));
		btPane.add(new OperationButton(Operation.RESULT, c));
		btPane.add(new OperationButton(Operation.CANCEL, c));
//forth row		
		btPane.add(new NumButton(0,c));
		btPane.add(new NumButton(NumButton.DOUBLEZERO,c));
		btPane.add(new NumButton(NumButton.POINT,c));

		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout(10, 10));
		contentPane.add(textBox,BorderLayout.NORTH);
		contentPane.add(btPane,BorderLayout.CENTER);

		this.pack();
	}
	
	public static void main(String[] args) {
		new MainWindow().setVisible(true);
	}

}
