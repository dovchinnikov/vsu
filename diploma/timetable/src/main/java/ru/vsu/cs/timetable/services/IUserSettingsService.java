package ru.vsu.cs.timetable.services;

import ru.vsu.cs.timetable.entity.User;
import ru.vsu.cs.timetable.entity.UserSettings;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Daniil Ovchinnikov
 * @since 5/31/13 6:27 PM
 */
public interface IUserSettingsService {

    void saveSettings(UserSettings settings);

    UserSettings getUserSettings(User user);

}
