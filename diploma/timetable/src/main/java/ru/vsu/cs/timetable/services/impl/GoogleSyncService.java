package ru.vsu.cs.timetable.services.impl;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver.Builder;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.services.calendar.CalendarScopes;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.entity.User;
import ru.vsu.cs.timetable.services.IClassPairService;
import ru.vsu.cs.timetable.services.ISyncService;
import ru.vsu.cs.timetable.services.IUserService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 * Year: 2013
 */
@Service
public class GoogleSyncService implements ISyncService {

    @Resource
    private IUserService userService;

    @Resource
    private IClassPairService classPairService;

    @Resource
    private JsonFactory jsonFactory;

    @Resource
    private HttpTransport httpTransport;

    @Resource
    private CredentialStore credentialStore;

    private AuthorizationCodeInstalledApp authorizationManager;

    @Value("${application.name}")
    private String applicationName;

    @PostConstruct
    public void init() throws IOException {
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory,
                new InputStreamReader(this.getClass().getResourceAsStream("/client_secrets.json")));
        if (clientSecrets.getDetails().getClientId().startsWith("Enter")
                || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
            throw new BeanCreationException("Cannot load Google Client API secrets");
        }

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow
                .Builder(httpTransport, jsonFactory, clientSecrets, Collections.singleton(CalendarScopes.CALENDAR))
                .setCredentialStore(credentialStore)
                .build();
        authorizationManager = new AuthorizationCodeInstalledApp(flow, new Builder().setPort(8080).build());
    }


    @Override
    public void sync(List<ClassPair> newClasses, List<ClassPair> deletedClasses) {

    }

    @Override
    public void fullSync(User user) {

    }

    private Credential getClient(String username) throws IOException {
        return authorizationManager.authorize(username);
    }

}
