package ru.vsu.cs.timetable.services.impl;

import org.springframework.stereotype.Service;
import ru.vsu.cs.timetable.entity.User;
import ru.vsu.cs.timetable.services.IUserService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
@Service
public class UserService implements IUserService {

    @PersistenceContext
    private EntityManager em;

    @Override
    public User getUser(Long id) {
        return null;
    }

    @Override
    public User createUser(User user) {
        return null;
    }

    @Override
    public List<User> getAllUsersWithRoles() {
        return null;
    }

    @Override
    public List<User> getUsersForNotifications(int course, int group, int subgroup) {
        return null;
    }

    @Override
    public List<User> getUsersForSync(int course, int group, int subgroup) {
        return null;
    }
}
