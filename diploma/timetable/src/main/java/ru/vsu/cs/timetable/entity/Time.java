package ru.vsu.cs.timetable.entity;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
public enum Time {
    UNDEFINED,
    FIRST,
    SECOND,
    THIRD,
    FOURTH,
    FIFTH,
    SIXTH,
    SEVENTH
}
