package ru.vsu.cs.timetable.job;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.vsu.cs.timetable.services.IUpdateService;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
@Component
public class CheckUpdatesJob {

    @Resource
    private IUpdateService updateService;

    @Value("${application.fileURL}")
    private String url;

    @Scheduled(cron = "0 0 12 ? * MON,TUE,WED,THU,FRI *")
    public void checkUpdates() {

    }

}
