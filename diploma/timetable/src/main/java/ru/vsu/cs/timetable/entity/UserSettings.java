package ru.vsu.cs.timetable.entity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
@Entity
@Table(name = "USER_SETTINGS")
public class UserSettings {

    @Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "COURSE_NUMBER")
    private Long course;

    @Column(name = "GROUP_NUMBER")
    private Long group;

    @Column(name = "SUBGROUP_NUMBER")
    private Long subgroup;

    @Column
    private String email;

    @Column(name = "EMAIL_CONFIRMED")
    private Boolean emailConfirmed;

    @Column(name = "EMAIL_NOTIFICATION_ENABLED")
    private Boolean emailNotificationEnabled;

    @Column(name = "SYNC_ENABLED")
    private Boolean syncEnabled;

    @Column(name = "SYNC_ACCESS_TOKEN")
    private String syncAccessToken;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getCourse() {
        return course;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public Long getGroup() {
        return group;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    public Long getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(Long subgroup) {
        this.subgroup = subgroup;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getEmailConfirmed() {
        return emailConfirmed;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        this.emailConfirmed = emailConfirmed;
    }

    public Boolean getEmailNotificationEnabled() {
        return emailNotificationEnabled;
    }

    public void setEmailNotificationEnabled(Boolean emailNotificationEnabled) {
        this.emailNotificationEnabled = emailNotificationEnabled;
    }

    public Boolean getSyncEnabled() {
        return syncEnabled;
    }

    public void setSyncEnabled(Boolean syncEnabled) {
        this.syncEnabled = syncEnabled;
    }

    public String getSyncAccessToken() {
        return syncAccessToken;
    }

    public void setSyncAccessToken(String syncAccessToken) {
        this.syncAccessToken = syncAccessToken;
    }
}
