package ru.vsu.cs.timetable.services.impl;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.services.INotificationService;
import ru.vsu.cs.timetable.services.IUserService;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 * Year: 2013
 */
@Service
public class NotificationService implements INotificationService {

    @Resource
    private JavaMailSender mailer;

    @Resource
    private IUserService userService;

    @Override
    public void notifyUsers(List<ClassPair> newClasses, List<ClassPair> deletedClasses) {

    }
}
