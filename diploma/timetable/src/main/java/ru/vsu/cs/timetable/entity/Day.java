package ru.vsu.cs.timetable.entity;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
public enum Day {

    UNDEFINED,
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY

}
