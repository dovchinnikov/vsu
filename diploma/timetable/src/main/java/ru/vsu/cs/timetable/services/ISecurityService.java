package ru.vsu.cs.timetable.services;

import org.springframework.security.core.GrantedAuthority;
import ru.vsu.cs.timetable.entity.Role;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
public interface ISecurityService {

    List<GrantedAuthority> getGrantedAuthorities(String username);

    List<Role> getRoles();

    void changeRole(Long userId, Long roleId);
}
