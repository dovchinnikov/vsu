package ru.vsu.cs.timetable.entity;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
@Entity
@Table(name = "classes")
public class ClassPair {

    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private String classroom;

    @Enumerated(EnumType.ORDINAL)
    @Column
    private Day day;

    @Enumerated(EnumType.ORDINAL)
    private Time time;

    @Column
    private Boolean numerator;

    @Column(name = "course_number")
    private Long course;

    @Column(name = "group_number")
    private Long group;

    @Column(name = "subgroup_number")
    private Long subgroup;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Boolean getNumerator() {
        return numerator;
    }

    public void setNumerator(Boolean numerator) {
        this.numerator = numerator;
    }

    public Long getCourse() {
        return course;
    }

    public void setCourse(Long course) {
        this.course = course;
    }

    public Long getGroup() {
        return group;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    public Long getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(Long subgroup) {
        this.subgroup = subgroup;
    }
}
