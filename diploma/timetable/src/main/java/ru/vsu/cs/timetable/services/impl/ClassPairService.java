package ru.vsu.cs.timetable.services.impl;

import org.springframework.stereotype.Service;
import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.entity.User;
import ru.vsu.cs.timetable.services.IClassPairService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Daniil Ovchinnikov
 */
@Service
public class ClassPairService implements IClassPairService {

    @PersistenceContext
    private EntityManager em;

    @Override
    public ClassPair getClassPair(Long id) {
        return null;
    }

    @Override
    public List<User> getUsers(ClassPair classPair) {
        return null;
    }

    @Override
    public void updateTimetable(List<ClassPair> newClasses) {

    }

    @Override
    public List<ClassPair> filter(Integer course, Integer group, Integer subgroup) {
        return null;
    }

    @Override
    public List<ClassPair> search(String searchString) {
        return null;
    }
}
