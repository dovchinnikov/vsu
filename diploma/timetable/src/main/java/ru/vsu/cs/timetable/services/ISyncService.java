package ru.vsu.cs.timetable.services;

import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.entity.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 * Year: 2013
 */
public interface ISyncService {

    void sync(List<ClassPair> newClasses, List<ClassPair> deletedClasses);

    void fullSync(User user);

}
