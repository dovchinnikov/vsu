package ru.vsu.cs.timetable.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import ru.vsu.cs.timetable.entity.Role;
import ru.vsu.cs.timetable.entity.User;
import ru.vsu.cs.timetable.services.ISecurityService;
import ru.vsu.cs.timetable.services.IUpdateService;
import ru.vsu.cs.timetable.services.IUserService;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 * Year: 2013
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private IUserService userService;

    @Resource
    private ISecurityService securityService;

    @Resource
    private IUpdateService updateService;

    @RequestMapping
    public ModelAndView index() {
        return new ModelAndView("admin");
    }

    @RequestMapping("/users")
    public
    @ResponseBody
    List<User> getUsers() {
        return userService.getAllUsersWithRoles();
    }

    @RequestMapping
    public
    @ResponseBody
    List<Role> getRoles() {
        return null;
    }

    @RequestMapping
    public void changeRole(@RequestParam Long userId, @RequestParam Long roleId) {

    }

    @RequestMapping(value = "/timetable", params = "action=update", method = RequestMethod.POST)
    public void startUpdate(MultipartHttpServletRequest request) {

    }

}
