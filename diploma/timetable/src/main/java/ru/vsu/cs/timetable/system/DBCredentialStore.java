package ru.vsu.cs.timetable.system;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.CredentialStore;
import org.springframework.stereotype.Component;
import ru.vsu.cs.timetable.services.IUserService;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Daniil Ovchinnikov
 * @since 5/31/13 5:58 PM
 */
@Component
public class DBCredentialStore implements CredentialStore {

    @Resource
    private IUserService userService;

    @Override
    public boolean load(String userId, Credential credential) throws IOException {
        return false;
    }

    @Override
    public void store(String userId, Credential credential) throws IOException {

    }

    @Override
    public void delete(String userId, Credential credential) throws IOException {

    }

}
