package ru.vsu.cs.timetable.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.services.IClassPairService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Daniil Ovchinnikov
 * @since 5/31/13 6:15 PM
 */
@Controller
@RequestMapping("/home")
public class MainController {

    @Resource
    private IClassPairService classPairService;

    @RequestMapping
    public ModelAndView handle() {
        return new ModelAndView("home");
    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST)
    public
    @ResponseBody
    List<ClassPair> filter(@RequestBody HashMap<String, Object> params) {
        return null;
    }

    @RequestMapping("/search")
    public
    @ResponseBody
    List<ClassPair> search(@RequestParam String searchString) {
        return classPairService.search(searchString);
    }
}
