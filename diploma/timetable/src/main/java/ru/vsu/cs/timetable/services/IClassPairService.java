package ru.vsu.cs.timetable.services;

import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.entity.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
public interface IClassPairService {

    ClassPair getClassPair(Long id);

    List<User> getUsers(ClassPair classPair);

    void updateTimetable(List<ClassPair> newClasses);

    List<ClassPair> filter(Integer course, Integer group, Integer subgroup);

    List<ClassPair> search(String searchString);

}
