package ru.vsu.cs.timetable.services.impl;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.timetable.entity.ClassPair;
import ru.vsu.cs.timetable.entity.Day;
import ru.vsu.cs.timetable.entity.Time;
import ru.vsu.cs.timetable.services.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 * Year: 2013
 */
@Service
public class UpdateService implements IUpdateService {

    @Resource
    private IUserService userService;

    @Resource
    private INotificationService notificationService;

    @Resource
    private ISyncService syncService;

    @Resource
    private IClassPairService classPairService;

    @Transactional
    @Override
    public void parseFile(File file) throws Exception {
        String FILENAME = "D:\\work\\diploma\\timetable\\test.xls";
        InputStream fileStream = new FileInputStream(FILENAME);
        Workbook wb = new HSSFWorkbook(fileStream);
        Sheet sheet = wb.getSheetAt(0);


        for (int i = 10; i < 95; i++) {
            Row row = sheet.getRow(i);
            Cell cell = row.getCell(2, Row.CREATE_NULL_AS_BLANK);
//            if (cell != null) {
//            System.out.println(i + ": " + cell.getStringCellValue());

//            }
        }

//        Pattern dayPattern = Pattern.compile("");
        Pattern coursePattern = Pattern.compile("^\\s*(\\d)\\s+к\\s*у\\s*р\\s*с\\s*$");
        Pattern groupPattern = Pattern.compile("^\\s*(\\d)\\s+группа\\s*$");
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress r = sheet.getMergedRegion(i);
            Cell firstCell = sheet.getRow(r.getFirstRow()).getCell(r.getFirstColumn());
            String value = firstCell.getStringCellValue();

            if (value.matches("^(Понедельник|Вторник|Среда|Четверг|Пятница|Суббота)$")) {
                System.out.println(r.formatAsString() + "  " + value);
            } else {
                Matcher m = coursePattern.matcher(value);
                if (m.find()) {
                    System.out.print(m.group(1));
                    System.out.println("  " + r.formatAsString() + "  " + value);
                } else {
                    m = groupPattern.matcher(value);
                    if (m.find()) {
                        System.out.print(m.group(1));
                        System.out.println("  " + r.formatAsString() + "  " + value);
                    }
                }
            }

            //            if(r.isInRange(cell.getRowIndex(), cell.getColumnIndex())){
//                System.out.println(r);
//                break;
//            }

        }

    }

    private boolean checkIfClassType(Cell cell) {
        return true;
    }

    private boolean checkIfClassType(CellRange range) {
        return false;
    }

    private Day getDay(int rowNumber, Map<Day, CellRange> daysMap) {
        return Day.MONDAY;
    }

    private Time getTime(int rowNumber, Map<Day, CellRange> daysMap) {
        return Time.FIRST;
    }

    private boolean getNumerator(int rowNumber, int firstRowNumber) {
        return true;
    }

    private int getCourse(int columnNumber, Map<Integer, Map<Integer, CellRange>> coursesGroupsMap) {
        return 1;
    }

    private int getGroup(int columnNumber, Map<Integer, Map<Integer, CellRange>> coursesGroupsMap) {
        return 1;
    }

    private int getSubgroup(int columnNumber, Map<Integer, Map<Integer, CellRange>> coursesGroupsMap) {
        return 1;
    }

    private void parseValue(ClassPair classPair, String value) {

    }

}
