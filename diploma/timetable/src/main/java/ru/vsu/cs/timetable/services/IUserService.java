package ru.vsu.cs.timetable.services;

import ru.vsu.cs.timetable.entity.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
public interface IUserService {

    User getUser(Long id);

    User createUser(User user);

    List<User> getAllUsersWithRoles();

    List<User> getUsersForNotifications(int course, int group, int subgroup);

    List<User> getUsersForSync(int course, int group, int subgroup);

}
