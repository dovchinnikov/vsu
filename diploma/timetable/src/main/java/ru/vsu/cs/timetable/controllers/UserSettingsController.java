package ru.vsu.cs.timetable.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.vsu.cs.timetable.services.IUserService;
import ru.vsu.cs.timetable.services.IUserSettingsService;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.security.Principal;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Daniil Ovchinnikov
 * @since 5/31/13 6:15 PM
 */
@Controller
@RequestMapping("/settings")
public class UserSettingsController {

    @Resource
    private IUserService userService;

    @Resource
    private IUserSettingsService userSettingsService;

    @RequestMapping
    public ModelAndView index(HttpSession session) {
        userSettingsService.getUserSettings(null);
        return null;
    }

    @RequestMapping(params = "save=course")
    public void saveCourse(@RequestParam Long course, Principal principal) {

    }

    @RequestMapping(params = "save=group")
    public void saveGroup(@RequestParam Long group, Principal principal) {

    }

    @RequestMapping(params = "save=subgroup")
    public void saveSubgroup(@RequestParam Long subgroup, Principal principal) {

    }

    @RequestMapping(params = "save=email")
    public void saveEmail(@RequestParam String email, Principal principal) {

    }

    @RequestMapping(params = "save=notificationEnabled")
    public void saveEmailNotificationEnabled(@RequestParam Boolean emailNotificationEnabled, Principal principal) {

    }

    @RequestMapping(params = "save=syncEnabled")
    public void saveSyncEnabled(@RequestParam Boolean syncEnabled, Principal principal) {

    }

}
