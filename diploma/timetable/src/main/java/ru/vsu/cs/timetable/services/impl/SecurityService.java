package ru.vsu.cs.timetable.services.impl;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vsu.cs.timetable.entity.Role;
import ru.vsu.cs.timetable.services.ISecurityService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
@Service
public class SecurityService implements ISecurityService {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public List<GrantedAuthority> getGrantedAuthorities(String username) {
        return null;
    }

    @Override
    public List<Role> getRoles() {
        return null;
    }

    @Override
    public void changeRole(Long userId, Long roleId) {

    }
}
