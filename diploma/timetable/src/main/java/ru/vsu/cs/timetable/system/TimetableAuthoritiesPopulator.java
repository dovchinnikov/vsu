package ru.vsu.cs.timetable.system;

import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;
import ru.vsu.cs.timetable.services.ISecurityService;

import javax.annotation.Resource;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 */
@Component
public class TimetableAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    @Resource
    private ISecurityService securityService;

    @Override
    public List<GrantedAuthority> getGrantedAuthorities(DirContextOperations userData, String username) {
        return securityService.getGrantedAuthorities(username);
    }

}