package ru.vsu.cs.timetable.services;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * Author: Daniil Ovchinnikov
 * Year: 2013
 */
public interface IUpdateService {

    void parseFile(File file) throws Exception;

}
