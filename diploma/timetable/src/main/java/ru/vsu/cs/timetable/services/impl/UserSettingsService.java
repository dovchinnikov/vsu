package ru.vsu.cs.timetable.services.impl;

import org.springframework.stereotype.Service;
import ru.vsu.cs.timetable.entity.User;
import ru.vsu.cs.timetable.entity.UserSettings;
import ru.vsu.cs.timetable.services.IUserSettingsService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Daniil Ovchinnikov
 * @since 5/31/13 6:32 PM
 */
@Service
public class UserSettingsService implements IUserSettingsService {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void saveSettings(UserSettings settings) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public UserSettings getUserSettings(User user) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
