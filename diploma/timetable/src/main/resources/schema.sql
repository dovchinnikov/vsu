CREATE TABLE user_table (
  id         NUMBER PRIMARY KEY,
  username   VARCHAR2(100) NOT NULL UNIQUE,
  first_name VARCHAR2(100) NOT NULL,
  last_name  VARCHAR2(100) NOT NULL
);

CREATE TABLE user_settings (
  id                         NUMBER PRIMARY KEY,
  user_id                    NUMBER REFERENCES user_table (id) NOT NULL,
  course_number              NUMBER(2, 0) DEFAULT 0            NOT NULL,
  group_number               NUMBER(2, 0) DEFAULT 0            NOT NULL,
  subgroup_number            NUMBER(2, 0) DEFAULT 0            NOT NULL,
  email                      VARCHAR2(100),
  email_confirmed            NUMBER(1, 0) DEFAULT 0            NOT NULL,
  email_notification_enabled NUMBER(1, 0) DEFAULT 0            NOT NULL,
  sync_enabled               NUMBER(1, 0) DEFAULT 0            NOT NULL,
  sync_access_token          VARCHAR2(100)
);

CREATE TABLE sec_privilege (
  id          NUMBER PRIMARY KEY,
  privilege   VARCHAR2(100) NOT NULL UNIQUE,
  description VARCHAR2(100)
);

CREATE TABLE sec_role (
  id          NUMBER PRIMARY KEY,
  name        VARCHAR2(100) NOT NULL UNIQUE,
  description VARCHAR2(100)
);

CREATE TABLE sec_roles_privileges (
  id           NUMBER,
  role_id      NUMBER REFERENCES sec_role (id)      NOT NULL,
  privilege_id NUMBER REFERENCES sec_privilege (id) NOT NULL,
  CONSTRAINT uq_role_privilege UNIQUE (role_id, privilege_id)
);

CREATE TABLE sec_users_roles (
  id      NUMBER PRIMARY KEY,
  user_id NUMBER REFERENCES user_table (id),
  role_id NUMBER REFERENCES sec_role (id),
  CONSTRAINT uq_user_role UNIQUE (user_id, role_id)
);

CREATE TABLE teacher (
  id              NUMBER PRIMARY KEY,
  user_id         NUMBER REFERENCES user_table (id),
  first_name      VARCHAR2(100) NOT NULL,
  middle_name     VARCHAR2(100) NOT NULL,
  last_name       VARCHAR2(100) NOT NULL,
  photo_file_name VARCHAR2(100)
);

CREATE TABLE classes (
  id              NUMBER PRIMARY KEY,
  name            VARCHAR2(100) NOT NULL,
  teacher_id      NUMBER REFERENCES teacher (id),
  classroom       VARCHAR2(20),
  day             NUMBER(2, 0)  NOT NULL,
  time            NUMBER(2, 0)  NOT NULL,
  numerator       NUMBER(1, 0)  NOT NULL,
  course_number   NUMBER(2, 0)  NOT NULL,
  group_number    NUMBER(2, 0)  NOT NULL,
  subgroup_number NUMBER(2, 0)  NOT NULL,
  CONSTRAINT uq_where_when UNIQUE (classroom, day, time, numerator)
);

