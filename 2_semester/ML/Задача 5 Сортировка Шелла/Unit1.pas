unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtDlgs;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    New: TButton;
    Button5: TButton;
    SaveDialog1: TSaveDialog;
    SaveResults: TButton;
    OpenTextFileDialog1: TOpenTextFileDialog;
    procedure SaveResultsClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure NewClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  Tarray = array of integer;
var
  Form1: TForm1;
  a,b: Tarray;
  n,k1,k2: integer;
implementation

{$R *.dfm}
procedure copyarray(a: Tarray);
var i: integer;
begin
  b:=nil;
  setlength(b,n);
  for i := 0 to n - 1 do
    b[i]:=a[i];
end;

function sr1(var o1,o2:integer):boolean;
begin
  k1:=k1+1;
  if o1<o2 then result:=true else result:=false;
end;

function sr2(var o1,o2:integer):boolean;
begin
  k2:=k2+1;
  if o1<o2 then result:=true else result:=false;
end;

procedure TForm1.Button1Click(Sender: TObject);
var i,k:integer;
    f: textfile;
    s,sm: string;
begin
  k:=0;
  n:=0;
 // if OpenTextFileDialog1.Execute then
 // assignfile(f,OpenTextFileDialog1.FileName)
 // else exit;
  assignfile(f,'C:\Users\AxE\Desktop\input.txt');
  reset(f);
  read(f,s);
  closefile(f);
  if s[length(s)]=' ' then delete(s,length(s),1);
  for i := 1 to length(s) do
    if s[i]=' ' then n:=n+1;
  n:=n+1;
  setlength(a,n);

  for i := 1 to length(s) do
    if ((s[i]=' ') or (i=length(s))) then
    begin
      if i=length(s) then sm:=sm+s[i];
      a[k]:=strtoint(sm);
      k:=k+1;
      sm:='';
    end
    else sm:=sm+s[i];

  stringgrid1.ColCount:=n;
  for i := 0 to n-1 do
    stringgrid1.Cells[i,0]:=inttostr(a[i]);
  button3.Enabled:=true;
  button2.Enabled:=true;
end;

procedure SortShell1(var b: Tarray);
var
  i,j,k,step: integer;
  tmp       : integer;
begin
  i:=0;
  j:=0;
  k:=0;
  step:=1;
  while step<n do begin
    k:=step;
    for i:=k to N-1 do begin
      tmp:=b[i];
      j:=i-k;
      while (j>=0) and sr1(tmp,b[j]) do
      begin
        b[j+k]:=b[j];
        j:=j-k;
      end;
      b[j+k]:=tmp;
    end;
    step:=step*3+1;
  end;
end;

procedure SortShell2(var b: tarray);
var
  i,j,k,step: integer;
  tmp       : integer;
begin
  k2:=0;
  step:=31;
  while step>=1 do begin
    k:=step;
    for i:=k to N do begin
      tmp:=b[i];
      j:=i-k;
      while (j>=0) and sr2(tmp,b[j]) do
      begin
        b[j+k]:=b[j];
        j:=j-k;
      end;
      b[j+k]:=tmp;
    end;
    step:=(step-1) div 2;
  end;
end;


procedure TForm1.Button2Click(Sender: TObject);
var i: integer;
begin
  copyarray(a);
  k1:=0;
  sortshell1(b);
  stringgrid2.ColCount:=n;  //����� �������
  for i := 0 to n-1 do
    stringgrid2.Cells[i,0]:=inttostr(b[i]);
  showmessage(inttostr(k1));
 // button2.Enabled:=false;
  if not(button3.Enabled) then saveresults.Enabled :=true;
end;

procedure TForm1.Button3Click(Sender: TObject);
var i: integer;
begin
  copyarray(a);
  sortshell2(b);
  stringgrid2.ColCount:=n;  //����� �������
  for i := 0 to n do
    stringgrid2.Cells[i,0]:=inttostr(b[i]);
  showmessage(inttostr(k2));
 // button3.Enabled:=false;
  if not(button2.Enabled) then saveresults.Enabled :=true;
end;

procedure TForm1.NewClick(Sender: TObject);
var i: integer;
begin
  stringgrid1.ColCount:=20;
  stringgrid2.ColCount:=20;
  for i  := 0 to 19 do
    begin
      stringgrid1.Cells[i,0]:='';
      stringgrid2.Cells[i,0]:='';
    end;
//  button2.Enabled:=false;
  //button3.Enabled:=false;
  //saveresults.Enabled:=false;
  n:=0;
  a:=nil;
  b:=nil;
  k1:=0;
  k2:=0;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  i,m: integer;
  flag: boolean;
begin
  n:=100;
  randomize;
  i:=0;
 // button3.Enabled:=true;
 // button2.Enabled:=true;
  setlength(a,n);
  repeat
    a[i]:=random(n)+1;
    flag:=true;
    for m := 0 to i-1 do
      if a[m]=a[i] then flag:=false;
    if flag then
    begin
      i:=i+1;
    end;
  until i=n;
  stringgrid1.ColCount:=n;
  for i := 0 to n-1 do
    stringgrid1.Cells[i,0]:=inttostr(a[i]);
end;

procedure TForm1.SaveResultsClick(Sender: TObject);
var
  f: textfile;
  i: integer;
begin
  if savedialog1.execute then
  assignfile(f,savedialog1.FileName)
  else exit;
  rewrite(f);
  writeln(f,'��� ������� �� ',n,' ��������� ���������� ��������� ����������� ����� � ������ ...121,40,13,4,1 ����� ',k1,'.');
  writeln(f,'��� ������� �� ',n,' ��������� ���������� ��������� ����������� ����� � ������ ...31,15,7,3,1 ����� ',k2,'.');
  writeln(f);
  for i := 0 to n-1 do
    write(f,a[i],' ');
  closefile(f);
end;


end.
