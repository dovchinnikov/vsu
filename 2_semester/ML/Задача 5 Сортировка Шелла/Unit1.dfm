object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 330
  ClientWidth = 648
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 8
    Top = 8
    Width = 625
    Height = 49
    ColCount = 20
    DefaultColWidth = 30
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    ScrollBars = ssHorizontal
    TabOrder = 0
  end
  object StringGrid2: TStringGrid
    Left = 8
    Top = 80
    Width = 625
    Height = 49
    ColCount = 20
    DefaultColWidth = 30
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    ScrollBars = ssHorizontal
    TabOrder = 1
  end
  object Button1: TButton
    Left = 184
    Top = 160
    Width = 105
    Height = 25
    Caption = 'Read'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 384
    Top = 160
    Width = 105
    Height = 25
    Caption = 'Go Shell 1'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 384
    Top = 191
    Width = 105
    Height = 25
    Caption = 'Go Shell 2'
    TabOrder = 4
    OnClick = Button3Click
  end
  object New: TButton
    Left = 184
    Top = 222
    Width = 105
    Height = 25
    Caption = 'New'
    TabOrder = 5
    OnClick = NewClick
  end
  object Button5: TButton
    Left = 184
    Top = 191
    Width = 105
    Height = 25
    Caption = 'Random'
    TabOrder = 6
    OnClick = Button5Click
  end
  object SaveResults: TButton
    Left = 384
    Top = 222
    Width = 105
    Height = 25
    Caption = 'Save Results'
    Enabled = False
    TabOrder = 7
    OnClick = SaveResultsClick
  end
  object SaveDialog1: TSaveDialog
    Filter = 'text file|*.txt'
    Left = 592
    Top = 296
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Left = 552
    Top = 264
  end
end
