object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 204
  ClientWidth = 648
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 8
    Top = 8
    Width = 625
    Height = 49
    ColCount = 20
    DefaultColWidth = 30
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    ScrollBars = ssHorizontal
    TabOrder = 0
  end
  object StringGrid2: TStringGrid
    Left = 8
    Top = 72
    Width = 625
    Height = 49
    ColCount = 20
    DefaultColWidth = 30
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    ScrollBars = ssHorizontal
    TabOrder = 1
  end
  object Button1: TButton
    Left = 239
    Top = 135
    Width = 105
    Height = 25
    Caption = 'Read'
    TabOrder = 2
    OnClick = Button1Click
  end
  object New: TButton
    Left = 48
    Top = 135
    Width = 105
    Height = 56
    Caption = 'Clear'
    TabOrder = 3
    OnClick = NewClick
  end
  object Button5: TButton
    Left = 239
    Top = 166
    Width = 105
    Height = 25
    Caption = 'Random'
    TabOrder = 4
    OnClick = Button5Click
  end
  object SaveResults: TButton
    Left = 430
    Top = 166
    Width = 105
    Height = 25
    Caption = 'Save Results'
    Enabled = False
    TabOrder = 5
    OnClick = SaveResultsClick
  end
  object Button2: TButton
    Left = 430
    Top = 135
    Width = 105
    Height = 25
    Caption = 'Go sort'
    Enabled = False
    TabOrder = 6
    OnClick = Button2Click
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Filter = 'Text file (*.txt)|*.txt'
    FilterIndex = 0
    Left = 568
    Top = 168
  end
  object SaveTextFileDialog1: TSaveTextFileDialog
    Filter = 'Text file (*.txt)|*.txt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofExtensionDifferent, ofEnableSizing]
    Left = 568
    Top = 136
  end
end
