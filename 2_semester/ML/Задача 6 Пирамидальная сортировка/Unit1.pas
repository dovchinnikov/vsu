unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtDlgs;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    Button1: TButton;
    New: TButton;
    Button5: TButton;
    OpenTextFileDialog1: TOpenTextFileDialog;
    SaveResults: TButton;
    Button2: TButton;
    SaveTextFileDialog1: TSaveTextFileDialog;
    procedure Button2Click(Sender: TObject);
    procedure SaveResultsClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure NewClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  Tarray = array of integer;
var
  Form1: TForm1;
  a: Tarray;
  ks: integer;
  t: Tarray;
implementation

{$R *.dfm}

function parent(var i: integer): integer;
begin
  result:=(i-1) div 2;
  if i=0 then
  result:=-1;
end;

function left(var i: integer): integer;
begin
  result:=2*i+1;
end;

function right(var i: integer): integer;
begin
  result:= 2*i+2;
end;

procedure exchange(x,y: integer);
var
  tmp: integer;
begin
  tmp:=t[x];
  t[x]:=t[y];
  t[y]:=tmp;
end;

{
procedure moveupdown(x: integer; ll: integer);
var
  stop: boolean;
  max,l,r: integer;
  last: integer;
begin
  stop:=false;
  repeat
    if parent(x)>=0 then
    begin
      ks:=ks+1;
      if t[x]>t[parent(x)] then
      begin
        exchange(x,parent(x));
        last:=x;
        x:=parent(x);
      end
      else stop:=true;
    end
    else stop:=true;
  until stop;


  stop:=false;
  repeat

    l:=left(x);
    r:=right(x);

    if l=last then
    begin
      ks:=ks+1;
      if t[r]>t[x] then
      begin
        exchange(x,9);
        x:=r;

      end;
    end;
    if r=last then
    begin
      ks:=ks+1;
      if t[l]>t[x] then
      begin
        exchange(x,9);
        x:=l;

      end;
    end;

    if l=ll-1 then
    begin
      ks:=ks+1;
      if t[l]>t[x] then exchange(x,l);
      exit;
    end;

    if (l<ll) or (r<ll) then
    begin
      ks:=ks+1;
      if (t[l]>t[x]) or (t[r]>t[x]) then
      begin
        if t[l]>t[r] then max:=l
        else max:=r;
        exchange(x,max);
        x:=max;
      end
      else stop:=true;
    end
    else stop:=true;

  until stop;
end;
}
procedure{function} moveup(x: integer){: integer};
var
  stop: boolean;
begin
  stop:=false;
  repeat
    if parent(x)>=0 then
    begin
      ks:=ks+1;
      if t[x]>t[parent(x)] then
      begin
        exchange(x,parent(x));
        x:=parent(x);
      end
      else stop:=true;
    end
    else stop:=true;
  until stop;
  //result:=x;
end;

procedure movedown(x: integer; ll: integer);
var
  stop: boolean;
  max: integer;
  l,r: integer;
begin
  stop:=false;
  repeat
    l:=left(x);
    r:=right(x);
    if l=ll-1 then
    begin
      ks:=ks+1;
      if t[l]>t[x] then exchange(x,l);
      exit;
    end;
    if (l<ll) or (r<ll) then
    begin
      ks:=ks+1;
      if (t[l]>t[x]) or (t[r]>t[x]) then
      begin
        if t[l]>t[r] then max:=l
        else max:=r;
        exchange(x,max);
        x:=max;
      end
      else stop:=true;
    end
    else stop:=true;
  until stop;
end;

procedure piramidsort;
var
  i,l,ll: integer;
  x: integer;
begin

  t:=nil;
  l:=length(a);             //���������� �������� ���� � ������� t
  for i := 0 to l - 1 do
    begin
      ll:=length(t);
      setlength(t,ll+1);    //���������� ������� �� ���� �������
      t[ll]:=a[i];          //���������� ������ �������� � ����� �������
      // moveupdown(ll,ll+1);
      //x:=ll;
      moveup(ll);
      movedown(ll,ll+1);
    end;

  ll:=length(t);
  exchange(0,ll-1);        //��������� ������� ������ �� ���� �����

 // ll:=ll-1;                //���������� ������� ����� � ������ ������� �� ���� �������

  repeat
    ll:=ll-1;
    movedown(0,ll);  //������� ll ���������� �� ������ ��������
                     //�������� ������� ����
    exchange(0,ll-1);
   // ll:=ll-1;
  until ll=1;

  for i := 0 to l - 1 do
    a[i]:=t[i];
  t:=nil;

end;

procedure TForm1.Button1Click(Sender: TObject);
var i:    integer;
    f:    textfile;
    s,sm: string;
    ll:   integer;
begin
  if OpenTextFileDialog1.Execute then
  assignfile(f,OpenTextFileDialog1.FileName)
  else exit;
  //assignfile(f,'C:\Users\AxE\Desktop\input.txt');
  reset(f);
  read(f,s);
  closefile(f);
  a:=nil;
  if s[length(s)]=' ' then delete(s,length(s),1);
  for i := 1 to length(s) do
    if ((s[i]=' ') or (i=length(s))) then
    begin
      if i=length(s) then sm:=sm+s[i];
      ll:=length(a);
      setlength(a,ll+1);
      a[ll]:=strtoint(sm);
      sm:='';
    end
    else sm:=sm+s[i];
  stringgrid1.ColCount:=ll+1;
  for i := 0 to ll do
    stringgrid1.Cells[i,0]:=inttostr(a[i]);
  button2.Enabled:=true;
end;

procedure TForm1.NewClick(Sender: TObject);
var i: integer;
begin
  stringgrid1.ColCount:=20;
  stringgrid2.ColCount:=20;
  for i  := 0 to 19 do
    begin
      stringgrid1.Cells[i,0]:='';
      stringgrid2.Cells[i,0]:='';
    end;
  ks:=0;
  a:=nil;
  button2.Enabled:=false;
  saveresults.Enabled:=false;
end;

procedure TForm1.Button2Click(Sender: TObject);
var i: integer;
begin
  ks:=0;
  piramidsort;
  stringgrid2.ColCount:=length(a);
  for i := 0 to length(a) - 1 do
    stringgrid2.Cells[i,0]:=inttostr(a[i]);
  showmessage('���������� ��������� ����� '+ inttostr(ks));
  saveresults.Enabled:=true;
  button2.Enabled:=false;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  i,m: integer;
  flag: boolean;
  n: integer;
begin
  n:=100;
  randomize;
  i:=0;
  a:=nil;
  setlength(a,n);
  repeat
    a[i]:=random(n)+1;
    flag:=true;
    for m := 0 to i-1 do
      if a[m]=a[i] then flag:=false;
    if flag then
    begin
      i:=i+1;
    end;
  until i=n;
  stringgrid1.ColCount:=n;
  for i := 0 to n-1 do
    stringgrid1.Cells[i,0]:=inttostr(a[i]);
  button2.Enabled:=true;
end;

procedure TForm1.SaveResultsClick(Sender: TObject);
var
  f: textfile;
  i: integer;
begin
  if savetextfiledialog1.execute then
  assignfile(f,savetextfiledialog1.FileName)
  else exit;
  rewrite(f);
  writeln(f,'�������� ������: ');
  writeln(f);
  for i := 0 to stringgrid1.ColCount -1 do
    write(f,stringgrid1.Cells[i,0],' ');
  writeln(f);
  writeln(f);
  writeln(f,'��� ������� �� ',length(a),' ��������� ���������� ��������� ������������� ����������� ����� ',ks);
  //writeln(f);
 // writeln(f);
  //for i := 0 to length(a) -1 do
//    write(f,a[i],' ');
  closefile(f);
  button2.Enabled:=false;
end;


end.
