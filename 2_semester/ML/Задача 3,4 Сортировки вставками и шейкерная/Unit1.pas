unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    StringGrid2: TStringGrid;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    OpenDialog1: TOpenDialog;
    New: TButton;
    Button5: TButton;
    SaveDialog1: TSaveDialog;
    SaveResults: TButton;
    procedure SaveResultsClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure NewClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  a: array of integer;
  n,k1,k2: integer;
implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var i,k:integer;
    f: textfile;
    s,sm: string;
begin
  k:=0;
  n:=0;
  if opendialog1.Execute then
  assignfile(f,opendialog1.FileName)
  else exit;

  //assignfile(f,'C:\Users\AxE\Desktop\input.txt');
  reset(f);
  read(f,s);
  closefile(f);
  if s[length(s)]=' ' then delete(s,length(s),1);
  for i := 1 to length(s) do
    if s[i]=' ' then n:=n+1;
  n:=n+1;
  setlength(a,n);

  for i := 1 to length(s) do
    if ((s[i]=' ') or (i=length(s))) then
    begin
      if i=length(s) then sm:=sm+s[i];
      a[k]:=strtoint(sm);
      k:=k+1;
      sm:='';
    end
    else sm:=sm+s[i];

  stringgrid1.ColCount:=n;
  for i := 0 to n-1 do
    stringgrid1.Cells[i,0]:=inttostr(a[i]);
  button3.Enabled:=true;
  button2.Enabled:=true;
end;

procedure SortInsertion(var a: array of integer);
var i,j : integer;
    key : integer;
begin
  k1:=0;
  for i:=1 to n do
  begin
    key := a[i];
    j := i - 1;
    while (j >= 0) and (A[j] > key) do
    begin
        k1:=k1+1;
        A[j + 1] := A[j];
        j := j - 1;
    end;
    A[j + 1] := key;
  end;
end;

procedure SortShaker (var a: array of integer);
var  j,left,right,tmp,last: integer;
begin
  k2:=0;
  left:=1;
  right:=n;
  last:=n;
  repeat
    for j:=right downto left do
    begin
      k2:=k2+1;
      if a[j-1]>a[j] then
      begin
        tmp:=a[j];
        a[j]:=a[j-1];
        a[j-1]:=tmp;
        last:=j;
      end;
    end;
    left:=last+1;
    for j:=left to right do
    begin
      k2:=k2+1;
      if a[j-1]>a[j] then
      begin
        tmp:=a[j];
        a[j]:=a[j-1];
        a[j-1]:=tmp;
        last:=j;
      end;
    end;
    right:=last-1;
  until left>right;
end;

procedure TForm1.Button2Click(Sender: TObject);
var i: integer;
begin
  sortshaker(a);
  stringgrid2.ColCount:=n;  //����� �������
  for i := 0 to n do
    stringgrid2.Cells[i,0]:=inttostr(a[i]);
  showmessage(inttostr(k2));
  button2.Enabled:=false;
  button3.Enabled:=false;
  saveresults.Enabled :=true;
end;

procedure TForm1.Button3Click(Sender: TObject);
var i: integer;
begin
  sortinsertion(a);
  stringgrid2.ColCount:=n;  //����� �������
  for i := 0 to n do
    stringgrid2.Cells[i,0]:=inttostr(a[i]);
  showmessage(inttostr(k1));
  button2.Enabled:=false;
  button3.Enabled:=false;
  saveresults.Enabled :=true;
end;

procedure TForm1.NewClick(Sender: TObject);
var i: integer;
begin
  stringgrid1.ColCount:=20;
  stringgrid2.ColCount:=20;
  for i  := 0 to 19 do
    begin
      stringgrid1.Cells[i,0]:='';
      stringgrid2.Cells[i,0]:='';
    end;
  button2.Enabled:=false;
  button3.Enabled:=false;
  saveresults.Enabled:=false;
  n:=0;
  a:=nil;
  k1:=0;
  k2:=0;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  i,m: integer;
  flag: boolean;
begin
  n:=100;
  randomize;
  i:=0;
  button3.Enabled:=true;
  button2.Enabled:=true;
  setlength(a,n);
  repeat
    a[i]:=random(n)+1;
    flag:=true;
    for m := 0 to i-1 do
      if a[m]=a[i] then flag:=false;
    if flag then
    begin
      i:=i+1;
    end;
  until i=n;
  stringgrid1.ColCount:=n;
  for i := 0 to n-1 do
    stringgrid1.Cells[i,0]:=inttostr(a[i]);
end;

procedure TForm1.SaveResultsClick(Sender: TObject);
var f: textfile;
  i: integer;
begin
  if savedialog1.execute then
  assignfile(f,savedialog1.FileName)
  else exit;
  rewrite(f);
  writeln(f,'��� ������� �� ',n,' ��������� ���������� ��������� ����������� ��������� ����� ',k1,'.');
  writeln(f,'��� ������� �� ',n,' ��������� ���������� ��������� ��������� ����������� ����� ',k2,'.');
  writeln(f);
  for i := 0 to n-1 do
    write(f,a[i],' ');
  closefile(f);
end;

end.
