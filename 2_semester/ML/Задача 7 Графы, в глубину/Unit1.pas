unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, unit2,unit3, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    label1: Tlabel;
    Edit1: TEdit;
    procedure Button3Click(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  s: string;
  ks: integer;
implementation
var
  drawing: boolean;
{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  form2.Show;
  button2.Enabled:=true;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Showgraph(true);
  button3.Enabled:=true;
end;

procedure TForm1.Button3Click(Sender: TObject);
var tmp: integer;
begin
  s:='';
  focus:=strtoint(edit1.text);
  setvisitfalse;
  ks:=0;
  obhod(focus);
  label1.caption:=s;
  showgraph(false);
  showmessage('���������� ���������� ����� ����� '+inttostr(ks));
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  i: integer;
  r: integer;
begin
  focus:=0;
  r:=diametr div 2;
  for i := 1 to l do
    if sqr(x-r-node[i].x0)+sqr(y-r-node[i].y0)<r*r then focus:=i;
    if focus=0 then drawing:=false
    else drawing:=true;
  showgraph(false);
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if drawing then begin
    node[focus].x0:=x-diametr div 2;
    node[focus].y0:=y-diametr div 2;
    showgraph(false);
  end;
end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=false;
  
end;

end.
