unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls,unit3;

type
  TForm2 = class(TForm)
    StringGrid1: TStringGrid;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}


procedure TForm2.Button1Click(Sender: TObject);
var i,j: integer;
begin
  randomize;
  for i := 1 to l do
    for j:= 1 to i-1 do
      begin
        stringgrid1.Cells[i,j]:=inttostr(random(2));
        stringgrid1.Cells[j,i]:=stringgrid1.Cells[i,j];
      end;
  form2.Button2.Enabled:=true;
end;

procedure TForm2.Button2Click(Sender: TObject);
var
  i,j,ll: integer;
begin
  for i := 1 to l do
    begin
      node[i].Edges:=nil;
      node[i].Name:=inttostr(i);
      for j := 1 to l do
          if stringgrid1.cells[i,j]='1' then
          begin
            ll:=length(node[i].Edges);
            setlength(node[i].Edges,ll+1);
            node[i].Edges[ll].NumNode:=j;
          end;
    end;
  showmessage('Succesfilly complleted!');
  form2.Close;
end;

procedure TForm2.FormCreate(Sender: TObject);
var
  i: integer;
begin
  stringgrid1.RowCount:=l+1;
  stringgrid1.ColCount:=l+1;
  if l>10 then stringgrid1.ScrollBars:=ssBoth;
  for i := 1 to l do
  begin
    stringgrid1.Cells[0,i]:=inttostr(i)+'.';
    stringgrid1.Cells[i,0]:=inttostr(i)+'.';
    stringgrid1.Cells[i,i]:='-';
  end;
end;

end.
