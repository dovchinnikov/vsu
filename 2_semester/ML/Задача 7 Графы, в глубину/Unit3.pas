unit Unit3;

interface
uses
  Graphics, sysutils, dialogs;
const
  l=10;
  diametr=30;
type
  TEdge = record
  //  A           : Integer;  // ��� �����
    NumNode     : Integer;  // ����� ����
  //  x1c, x2c, yc: Integer;  // ������������� ���������� ����
//   Color       : TColor;   // ���� ����
  end;
  TNode = record
    Name    : string;          // ��� ����
    Edges   : array of TEdge;  // ������ ��� (�����)
    Visit   : Boolean;         // ������� "���� �������"
    x0, y0  : Integer;         // ���������� ������ ����
//    NumVisit: Integer;         // � ���������
    Color   : TColor;          // ���� ����
//    Dist    : Integer;  // ����������� ���������� �� ����� ����
  end;
  procedure ShowGraph( first: boolean);
  procedure obhod(var o: integer);
  procedure setvisitfalse;
var
  node: array[1..l] of tnode;
  focus: integer;

implementation

uses
  unit1;

procedure XYGen;
var
  i: integer;
begin
  for i := 1 to l do
    begin
      node[i].x0:=round(cos(2*pi*i/l-pi/2)*150+form1.Width/2)- diametr div 2;
      node[i].y0:=round(sin(2*pi*i/l-pi/2)*150+form1.height/2)- diametr div 2;
    end;
end;

procedure ShowGraph(first: boolean);
var i, j,ll: Integer;
s: string;
r: integer;
begin
  if first then XYGen;
  r:=diametr div 2;
  with form1.Canvas do begin
    Brush.Color := $f0f0f0;
    rectangle(-1, -1, form1.Width , form1.Height );  // ������� �����
    for i := 1 to L do                // ��������� ���
    with Node[i] do begin
      LL := Length(Edges);
      for j := 0 to LL - 1 do
      with Edges[j] do begin
        moveto(x0+r,y0+r);
        lineto(Node[NumNode].x0+r,Node[NumNode].y0+r);
      end;
    end;

    for i := 1 to L do
    with Node[i] do begin
    if i=focus then pen.Width :=2
    else pen.Width:=1;
    ellipse(x0,y0,x0+diametr,y0+diametr);
      s := Name;
      TextOut(r+x0-TextWidth(s) div 2,r+y0-TextHeight(s) div 2,s);
    end;
    pen.Width:=1;
  end;
  form1.label1.Repaint;
end;

procedure obhod(var o: integer);
var
  next: integer;
  a: integer;
  ll: integer;
  flag: boolean;
begin
  ll:=length(node[o].Edges);

  node[o].Visit:=true;
  s:=s+inttostr(o)+' ';
  ks:=ks+1;
  a:=0;
  if ll=0 then
  begin
    showmessage('���� ���������!');
    exit;
  end;
  flag:=false;
  repeat
    next:=node[o].edges[a].NumNode;
    if flag then begin
        s:=s+inttostr(o)+' ';
        ks:=ks+1;
        flag:=false;
      end;
    if (node[next].Visit) then a:=a+1
    else begin
      obhod(next);
      flag:=true;
    end;
  until a>ll-1;
end;

procedure setvisitfalse;
var
  i: integer;
begin
  for i := 1 to l do
    node[i].Visit:=false;
end;

end.
