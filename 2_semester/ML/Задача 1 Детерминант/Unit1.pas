unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, ExtDlgs;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    Edit1: TEdit;
    RndBtn: TButton;
    Label1: TLabel;
    LoadFile: TButton;
    Count: TButton;
    SaveFile: TButton;
    StringGrid2: TStringGrid;
    OpenTextFileDialog1: TOpenTextFileDialog;
    SaveTextFileDialog1: TSaveTextFileDialog;
    procedure SaveFileClick(Sender: TObject);
    procedure LoadFileClick(Sender: TObject);
    procedure CountClick(Sender: TObject);

    procedure RndBtnClick(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;
//  function det(c: array of array of real; r: integer): real;
  massiv=array of array of integer;
var
  Form1: TForm1;
  n: integer;
implementation

{$R *.dfm}

function det(var c: massiv; r: integer; len: integer): integer;
var
  b: massiv;
  d: integer;
  i,j: integer;
begin
  d:=0;
  if not(len=n) then
  begin
    setlength(b,len,len);
    for i := 0 to len-1 do
      for j := 0 to len-1 do
        if j<r then b[i,j]:=c[i+1,j]
        else b[i,j]:=c[i+1,j+1];
  end
  else begin
    setlength(b,len,len);
    for i := 0 to len - 1 do
      for j := 0 to len - 1 do
        b[i,j]:=c[i,j];
  end;

  if len=2 then result:=b[0,0]*b[1,1]-b[0,1]*b[1,0]
  else begin
    for j := 0 to len-1 do
      if (j mod 2=0) then
      d:=d+b[0,j]*det(b,j,len-1)
      else d:=d-b[0,j]*det(b,j,len-1);
    result:=d;
  end;
end;

function check(s:string):boolean;
var
  MySet: set of char;
  i: integer;
begin
  MySet:=['0','1','2','3','4','5','6','7','8','9','-',' '];
  result:=true;
  for i := 1 to Length(s) do
    if not(s[i] in MySet) then result:=false;
  if s='' then result:=false;
end;


procedure TForm1.CountClick(Sender: TObject);
var a: massiv;

procedure getA;
var
  i,j: integer;
begin
  setlength(a,n,n);
  for i := 0 to n - 1 do
    for j := 0 to n - 1 do
      a[i,j]:=strtoint(stringgrid1.Cells[j,i]);
end;

begin
  geta;
  label1.Caption:='������������ �����:  '+floattostr(det(a,-1,n));
end;

procedure TForm1.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  MySet: set of char;
  i: integer;
  flag: boolean;
  s: string;
begin
  MySet:=['0','1','2','3','4','5','6','7','8','9'];
  if key=13 then
  begin
    s:=Edit1.Text;
    flag:=true;
    for i := 1 to Length(s) do
      if not(s[i] in MySet) then flag:=false;
    if flag=true and not(s='') then
    begin
      with StringGrid1 do
      begin
        ColCount:=StrToInt(s);
        RowCount:=StrToInt(s);
        Width:=ColCount*(DefaultColWidth+1)+4;
        Height:=RowCount*(DefaultRowHeight+1)+4;
      end;
    end
    else begin
      showmessage('������� ����� ������������� �����!');
      Edit1.Text:='';
    end;
  end;
end;

procedure TForm1.LoadFileClick(Sender: TObject);
var
  f: textfile;
  s,ss: string;
  i,j,len: integer;
  m: integer;

begin
  n:=0;
  len:=0;
  for i := 0 to stringgrid1.ColCount  - 1 do
    for j := 0 to stringgrid1.RowCount  - 1 do
      stringgrid1.Cells[i,j]:='';

  if opentextfiledialog1.execute then
  assignfile(f,opentextfiledialog1.FileName);
  reset(f);
  repeat
    readln(f,s);
    if not(check(s)) then
    begin
      showmessage('������ ����! ������� ������ �����-������');
      exit;
    end;
    len:=length(s);
    n:=n+1;
    m:=0;
    for i := 1 to len do
      if s[i]=' ' then begin
        m:=m+1;
        stringgrid1.Cells [m-1,n-1]:=ss;
        ss:='';
      end
      else if i=len then begin
             ss:=ss+s[i];
             m:=m+1;
             stringgrid1.Cells[m-1,n-1]:=ss;
             ss:='';
           end
           else ss:=ss+s[i];
  until eof(f);
  closefile(F);
  if n>m then stringgrid1.ColCount:=n
  else stringgrid1.ColCount:=m;
  Stringgrid1.RowCount:=stringgrid1.colcount;
  len:=stringgrid1.ColCount;
  with StringGrid1 do
      begin
        Width:=ColCount*(DefaultColWidth+1)+4;
        Height:=RowCount*(DefaultRowHeight+1)+4;
      end;
  for i := 0 to len-1 do
  for j := 0 to len-1 do
    if stringgrid1.cells[i,j]='' then stringgrid1.cells[i,j]:='0';
  edit1.Text:=inttostr(stringgrid1.ColCount);

end;

procedure TForm1.RndBtnClick(Sender: TObject);
var
  i,j: integer;
begin
  randomize;
  n:=strtoint(edit1.Text);
  for i := 0 to n-1 do
    for j := 0 to n-1 do
      StringGrid1.Cells[i,j]:=FloatToStr(random(10)+1);
end;





procedure TForm1.SaveFileClick(Sender: TObject);
var f: textfile;
begin
  if savetextfiledialog1.Execute  then
    assignfile(f,savetextfiledialog1.FileName);
    rewrite(f);
    write(f,label1.Caption);
    closefile(f);
end;

end.
