object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 331
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 503
    Top = 8
    Width = 161
    Height = 25
    AutoSize = False
    Caption = #1054#1087#1088#1077#1076#1077#1083#1080#1090#1077#1083#1100' '#1088#1072#1074#1077#1085': '
    Layout = tlCenter
  end
  object StringGrid2: TStringGrid
    Left = 448
    Top = 39
    Width = 161
    Height = 129
    DefaultColWidth = 30
    FixedCols = 0
    FixedRows = 0
    ScrollBars = ssNone
    TabOrder = 6
    Visible = False
  end
  object StringGrid1: TStringGrid
    Left = 24
    Top = 39
    Width = 161
    Height = 129
    DefaultColWidth = 30
    FixedCols = 0
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 24
    Top = 8
    Width = 57
    Height = 25
    TabOrder = 1
    Text = '5'
    OnKeyDown = Edit1KeyDown
  end
  object LoadFile: TButton
    Left = 295
    Top = 8
    Width = 98
    Height = 25
    Caption = 'LoadFile'
    TabOrder = 3
    OnClick = LoadFileClick
  end
  object SaveFile: TButton
    Left = 399
    Top = 8
    Width = 98
    Height = 25
    Caption = 'SaveFile'
    TabOrder = 5
    OnClick = SaveFileClick
  end
  object Count: TButton
    Left = 191
    Top = 8
    Width = 98
    Height = 25
    Caption = 'Count'
    TabOrder = 4
    OnClick = CountClick
  end
  object RndBtn: TButton
    Left = 87
    Top = 8
    Width = 98
    Height = 25
    Caption = 'RndBtn'
    TabOrder = 2
    OnClick = RndBtnClick
  end
  object OpenTextFileDialog1: TOpenTextFileDialog
    Left = 49
    Top = 281
  end
  object SaveTextFileDialog1: TSaveTextFileDialog
    Left = 88
    Top = 280
  end
end
