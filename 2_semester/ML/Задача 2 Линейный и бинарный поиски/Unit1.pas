unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

const n=100;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  a: array[1..100] of integer;
  k: integer;
implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  i,m: integer;
  flag: boolean;
begin
  randomize;
  i:=1;
  memo1.Lines.Clear;
  button3.Enabled:=true;
  button2.Enabled:=false;
  repeat
    a[i]:=random(100)+1;
    flag:=true;
    for m := 1 to i-1 do
      if a[m]=a[i] then flag:=false;
    if flag then
    begin
      memo1.Lines.Add(inttostr(a[i]));
      i:=i+1;
    end;
  until i=100;


end;

procedure TForm1.Button2Click(Sender: TObject);

function LineSearch (a:array of integer;x:integer):integer;
var  i:integer;
begin
  i:=N;
  a[0]:=x;
  while a[i]<>x do
  begin
    i:=i-1;
    k:=k+1;
  end;
  Result:=i;
end;

var
  i,m:integer;

begin
  k:=0;
  m:=0;
  for i := 1 to n do
    m:=m+linesearch(a,i);
  showmessage(inttostr(k));

end;

procedure TForm1.Button3Click(Sender: TObject);

function BinarySearch (a: array of integer; x: Integer): Integer;
var left,right,p: Integer;
begin
     left:=1; right:=N;
     repeat
        p:=(left+right) div 2;
        if x>a[p]
           then left:=p+1
           else right:=p-1;
        k:=k+1;
     until (a[p]=x) or (left>right);
     if a[p]=x
        then Result:=p
        else Result:=0;
end;

var
  i,m: integer;
begin
  k:=0;
  m:=0;
  for i := 1 to n do
    m:=m+binarysearch(a,i);
  showmessage(inttostr(k));
end;

procedure TForm1.Button4Click(Sender: TObject);
var i:integer;
begin
  button2.enabled:=true;
  button3.Enabled:=false;
  memo1.lines.Clear;
  for  i:= 1 to n do
    begin
      a[i]:=i;
      memo1.lines.add(inttostr(a[i]));
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  memo1.lines.Clear;
end;

end.
