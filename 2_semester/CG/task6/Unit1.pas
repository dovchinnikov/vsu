unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, unit2, lib, ComCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    showcube: TCheckBox;
    showpiramidka: TCheckBox;
    showoctaedr: TCheckBox;
    CheckBox4: TCheckBox;
    Checkx: TCheckBox;
    Checky: TCheckBox;
    Checkz: TCheckBox;
    TrackBar1: TTrackBar;
    checkmk: TCheckBox;
    octaspeed: TTrackBar;
    tetraspeed: TTrackBar;
    cubespeed: TTrackBar;
    speed: TTrackBar;
    showosi: TCheckBox;
    rotationosi: TCheckBox;
    procedure showosiClick(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormDblClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
var
  Form1: TForm1;
  flag: boolean;

implementation

{$R *.dfm}


procedure TForm1.FormCreate(Sender: TObject);
begin
  a:=1;
  mk:=trackbar1.Position;
  {alf:=5*pi/4;
  bet:=pi/6;
  gam:=pi/2; }
  {alf:=pi/4;
  bet:=3*pi/4;
  gam:=-pi/2;
  }
  alf:=0;
  bet:=0;
  gam:=0;
  setcube;
  setosi;
  set2dim(osi,alf,bet,gam);
  setoctaedr;
  setpiramidka;
end;

procedure TForm1.FormDblClick(Sender: TObject);
begin
  checkx.Checked:=false;
  checky.Checked:=false;
  checkz.Checked:=false;
  alf:=0;
  bet:=0;
  gam:=0;
  //set2dim(osi,alf,bet,gam);
  //showtelo(osi,true,2);
end;

procedure TForm1.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  a:=a-0.05;
  if a<=0.2 then
  a:=0.2;
end;

procedure TForm1.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  a:=a+0.05;
end;

procedure TForm1.showosiClick(Sender: TObject);
begin
  rotationosi.Checked:=true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
os: boolean;
begin
{
  with form1.Canvas.Pen do
  case random(15)+1 of

  1: color:=clBlack;
	2: color:=clMaroon;
	3: color:=clGreen;
	4: color:=clOlive;
	5: color:=clNavy;
	6: color:=clPurple;
	7: color:=clTeal;
	8: color:=clGray;
	9: color:=clSilver;
	10: color:=clRed;
	11: color:=clLime;
	12: color:=clYellow;
	13: color:=clBlue;
	14: color:=clFuchsia;
	15: color:=clAqua;


  end;
  }
  if checkx.checked then alf:=alf+0.001*speed.Position;
  if checky.checked then bet:=bet+0.001*speed.Position;
  if checkz.checked then gam:=gam+0.001*speed.Position;


  flag:=true;
  if showosi.Checked  then
  begin
    if rotationosi.Checked  then set2dim(osi,alf,bet,gam);
    showtelo(osi,flag,2);
    flag:=false;
  end;
  if form1.showcube.Checked then docube;
  if form1.showpiramidka.Checked then dotetra;
  if form1.showoctaedr.Checked then doocta;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  mk:=trackbar1.Position;
end;

end.
