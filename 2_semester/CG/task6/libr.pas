unit libr;





interface
  type
  TVector= array[1..4] of real;
  TMatrix= array of TVector;
  TVertex=record
    x,y,z: real;
  end;
  TSide =record
    p: array of word;   // ������ ������
    A,B,C,D: single;  // ������������ ��������� ���������
    N: TVector;        // ������ ������� � ���������
  end;
  TEdge=record
    p1,p2: word;                   // ������ ������
  end;
  TBody=record
    Vertexs  : array of TVertex;    // ������ ��������� ��������� ������ ����
    VertexsT : array of TVector;    // ������ ������� ��������� ������ ����
    Edges    : array of TEdge;      // ������ ����� ����
    Sides    : array of TSide;      // ������ ������ ���
end;


implementation



end.
