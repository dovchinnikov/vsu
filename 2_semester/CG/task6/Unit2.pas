unit Unit2;

interface
uses lib;

  procedure showtelo(telo: array of tvertex; first: boolean; w: integer);
  procedure set2dim(var telo: array of tvertex; al,be,ga: real);
  procedure docube;
  procedure dotetra;
  procedure doocta;
var
  a,alf,bet,gam: real;
  mk: integer;

implementation
uses
  unit1;


procedure showtelo(telo: array of tvertex;first: boolean; w: integer);
var
  k,m,l,ll: Integer;

begin
  l:=length(telo);
  with form1.Canvas do begin
    pen.Width:=w;
    Brush.Color := $f0f0f0;
    if first then rectangle(-1, -1, form1.Width , form1.Height );  // ������� �����
    for k := 0 to l-1 do                // ��������� ���
    with telo[k] do begin
      LL := Length(Edges);
      for m := 0 to LL - 1 do
      with Edges[m] do begin
        moveto(i,j);
        lineto(telo[nom].i,telo[nom].j);
      end;
    end;
  end;

end;

procedure set2dim(var telo: array of tvertex; al,be,ga: real);
var
  l: integer;
  i: integer;

  xt,yt,zt: real;
begin
  l:=length(telo);
  for i := 0 to l - 1 do
    with telo[i] do begin
      //al:=al+speed/100;
      //be:=be+speed/100;
      //ga:=ga+speed/100;
      xt:=x*cos(ga)*cos(be)+y*(cos(ga)*sin(al)*sin(be)-sin(ga)*cos(al))+z*(cos(ga)*cos(al)*sin(be)+sin(al)*sin(ga));
      yt:=x*sin(ga)*cos(be)+y*(sin(ga)*sin(be)*sin(al)+cos(al)*cos(ga))+z*(sin(ga)*cos(al)*sin(be)-sin(al)*cos(ga));
      zt:=x*sin(-be)+y*(sin(al)*cos(be))+z*(cos(al)*cos(be));
      {
      xx:=x;
      yx:=y*cos(al)+z*sin(-al);
      zx:=y*sin(al)+z*cos(al);

      xy:=xx*cos(be)+zx*sin(be);
      yy:=yx;
      zy:=xx*sin(-be)+zx*cos(be);

      xt:=xy*cos(ga)+yy*sin(-ga);
      yt:=xy*sin(ga)+yy*cos(ga);
      zt:=zy;
      {
      getmx(alf,bet,gam);
      xt:=x*mr[0,0]+y*mr[1,0]+z*mr[2,0];
      yt:=x*mr[0,1]+y*mr[1,1]+z*mr[2,1];
      zt:=x*mr[0,2]+y*mr[1,2]+z*mr[2,2];
      }
      if not(form1.checkmk.checked) then
      begin
        i:=round(a*mk*xt/(zt+mk))+form1.Width div 2;
        j:=-round(a*mk*yt/(zt+mk))+form1.Height div 2;
      end
      else begin
        i:=round(a*xt)+form1.Width div 2;
        j:=-round(a*yt)+form1.Height div 2;
      end;
    end;
end;

procedure docube;
var a,b,g: real;
begin   {
  a:=alf;
  b:=bet;
  g:=gam;
  if form1.Checkx.Checked then a:=a*form1.cubespeed.Position/70;
  if form1.Checky.Checked then b:=b*form1.cubespeed.Position/70;
  if form1.Checkz.Checked then g:=g*form1.cubespeed.Position/70;}
  set2dim(cube,alf,bet,gam);
  showtelo(cube,flag,1);
  flag:=false;
end;

procedure dotetra;
var a,b,g: real;
begin
 { a:=alf;
  b:=bet;
  g:=gam;
  if form1.Checkx.Checked then a:=a*form1.tetraspeed.Position/70;
  if form1.Checky.Checked then b:=b*form1.tetraspeed.Position/70;
  if form1.Checkz.Checked then g:=g*form1.tetraspeed.Position/70;   }
  set2dim(tetra,alf,bet,gam);
  showtelo(tetra,flag,1);
  flag:=false;
end;

procedure doocta;
var a,b,g: real;
begin
 { a:=alf;
  b:=bet;
  g:=gam;
  if form1.Checkx.Checked then a:=a*form1.octaspeed.Position/70;
  if form1.Checky.Checked then b:=b*form1.octaspeed.Position/70;
  if form1.Checkz.Checked then g:=g*form1.octaspeed.Position/70;
  }set2dim(octaedr,alf,bet,gam);
  showtelo(octaedr,flag,1);
  flag:=false;
end;



end.
