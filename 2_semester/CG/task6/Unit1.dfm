object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 514
  ClientWidth = 1053
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  PixelsPerInch = 96
  TextHeight = 13
  object showcube: TCheckBox
    Left = 728
    Top = 32
    Width = 89
    Height = 17
    Caption = #1050#1091#1073
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object showpiramidka: TCheckBox
    Left = 728
    Top = 55
    Width = 89
    Height = 16
    Caption = #1055#1080#1088#1072#1084#1080#1076#1082#1072
    TabOrder = 1
  end
  object showoctaedr: TCheckBox
    Left = 728
    Top = 77
    Width = 89
    Height = 21
    Caption = #1054#1082#1090#1072#1101#1076#1088
    TabOrder = 2
  end
  object CheckBox4: TCheckBox
    Left = 728
    Top = 104
    Width = 89
    Height = 17
    Caption = 'CheckBox4'
    TabOrder = 3
    Visible = False
  end
  object Checkx: TCheckBox
    Left = 736
    Top = 463
    Width = 41
    Height = 17
    Caption = 'x'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object Checky: TCheckBox
    Left = 736
    Top = 478
    Width = 41
    Height = 17
    Caption = 'y'
    TabOrder = 5
  end
  object Checkz: TCheckBox
    Left = 736
    Top = 493
    Width = 41
    Height = 17
    Caption = 'z'
    TabOrder = 6
  end
  object TrackBar1: TTrackBar
    Left = 8
    Top = 8
    Width = 521
    Height = 33
    LineSize = 4
    Max = 1500
    Min = 300
    Frequency = 25
    Position = 1000
    TabOrder = 7
    ThumbLength = 15
    OnChange = TrackBar1Change
  end
  object checkmk: TCheckBox
    Left = 535
    Top = 8
    Width = 179
    Height = 17
    Caption = #1058#1086#1095#1082#1072' '#1089#1093#1086#1076#1072' '#26' '#1073#1077#1089#1082#1086#1085#1077#1095#1085#1086#1089#1090#1100
    TabOrder = 8
  end
  object octaspeed: TTrackBar
    Left = 823
    Top = 83
    Width = 220
    Height = 25
    Max = 150
    Frequency = 3
    Position = 20
    TabOrder = 9
    ThumbLength = 15
    Visible = False
  end
  object tetraspeed: TTrackBar
    Left = 823
    Top = 55
    Width = 220
    Height = 22
    Max = 150
    Frequency = 3
    Position = 20
    TabOrder = 10
    ThumbLength = 15
    Visible = False
  end
  object cubespeed: TTrackBar
    Left = 823
    Top = 32
    Width = 220
    Height = 25
    Max = 150
    Frequency = 3
    Position = 15
    TabOrder = 11
    ThumbLength = 15
    Visible = False
  end
  object speed: TTrackBar
    Left = 770
    Top = 463
    Width = 283
    Height = 32
    Max = 150
    Frequency = 5
    Position = 40
    TabOrder = 12
    ThumbLength = 15
    TickMarks = tmBoth
  end
  object showosi: TCheckBox
    Left = 728
    Top = 9
    Width = 89
    Height = 17
    Caption = #1054#1089#1080
    TabOrder = 13
    OnClick = showosiClick
  end
  object rotationosi: TCheckBox
    Left = 823
    Top = 9
    Width = 97
    Height = 17
    Caption = #1042#1088#1072#1097#1072#1090#1100
    TabOrder = 14
  end
  object Timer1: TTimer
    Interval = 50
    OnTimer = Timer1Timer
    Left = 656
    Top = 40
  end
end
