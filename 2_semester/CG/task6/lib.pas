unit lib;

interface
  type
  tEdge = record
    nom: integer;
    visible: boolean;
  end;

  tVertex = record
    x,y,z: real;
    //xt,yt,zt: real;
    edges: array of tedge;
    i,j: integer;
  end;

var
  cube: array of tvertex;
  osi: array of tvertex;
  tetra: array of tvertex;
  octaedr: array of tvertex;
  mr: array of array of real;

  procedure setcube;
  procedure setosi;
  procedure setpiramidka;
  procedure setoctaedr;
  procedure getmx(al,be,ga: real);

implementation

uses unit2,unit1;

procedure getmx(al,be,ga: real);
var mx,my,mz: array of array of real;
i,j,k: integer;
begin
  setlength(mx,3,3);
  setlength(my,3,3);
  setlength(mz,3,3);

  mx[0,0]:=1;        mx[0,1]:=0;        mx[0,2]:=0;
  mx[1,0]:=0;        mx[1,1]:=cos(al);  mx[1,2]:=sin(al);
  mx[2,0]:=0;        mx[2,1]:=sin(-al); mx[2,2]:=cos(al);

  my[0,0]:=cos(be);  my[0,1]:=0;        my[0,2]:=sin(-be);
  my[1,0]:=0;        my[1,1]:=1;        my[1,2]:=0;
  my[2,0]:=sin(be);  my[2,1]:=0;        my[2,2]:=cos(be);

  mz[0,0]:=cos(ga);  mz[0,1]:=sin(ga);  mz[0,2]:=0;
  mz[1,0]:=sin(-ga); mz[1,1]:=cos(ga);  mz[1,2]:=0;
  mz[2,0]:=0;        mz[2,1]:=0;        mz[2,2]:=1;

  setlength(mr,3,3);
  for i := 0 to 2 do
    for j := 0 to 2 do
      for k := 0 to 2 do
        mr[i,j]:=mr[i,j]+mx[i,k]*my[k,j];
  for i := 0 to 2 do
    for j := 0 to 2 do
      mx[i,j]:=mr[i,j];
  mr:=nil;
  setlength(mr,3,3);
  for i := 0 to 2 do
    for j := 0 to 2 do
      for k := 0 to 2 do
        mr[i,j]:=mr[i,j]+mx[i,k]*mz[k,j];
end;

procedure setcube;
var i: integer;
begin
  setlength(cube,8);
  for i := 0 to 7 do
    setlength(cube[i].edges,3);

  cube[0].x:=100;
  cube[0].y:=100;
  cube[0].z:=100;

  cube[1].x:=-100;
  cube[1].y:=100;
  cube[1].z:=100;

  cube[2].x:=100;
  cube[2].y:=-100;
  cube[2].z:=100;

  cube[3].x:=100;
  cube[3].y:=100;
  cube[3].z:=-100;

  cube[4].x:=-100;
  cube[4].y:=-100;
  cube[4].z:=100;

  cube[5].x:=100;
  cube[5].y:=-100;
  cube[5].z:=-100;

  cube[6].x:=-100;
  cube[6].y:=100;
  cube[6].z:=-100;

  cube[7].x:=-100;
  cube[7].y:=-100;
  cube[7].z:=-100;

  cube[0].edges[0].nom:=1;
  cube[0].edges[1].nom:=2;
  cube[0].edges[2].nom:=3;

  cube[1].edges[0].nom:=0;
  cube[1].edges[1].nom:=4;
  cube[1].edges[2].nom:=6;

  cube[2].edges[0].nom:=0;
  cube[2].edges[1].nom:=4;
  cube[2].edges[2].nom:=5;

  cube[3].edges[0].nom:=0;
  cube[3].edges[1].nom:=5;
  cube[3].edges[2].nom:=6;

  cube[4].edges[0].nom:=1;
  cube[4].edges[1].nom:=2;
  cube[4].edges[2].nom:=7;

  cube[5].edges[0].nom:=2;
  cube[5].edges[1].nom:=3;
  cube[5].edges[2].nom:=7;

  cube[6].edges[0].nom:=1;
  cube[6].edges[1].nom:=3;
  cube[6].edges[2].nom:=7;

  cube[7].edges[0].nom:=4;
  cube[7].edges[1].nom:=5;
  cube[7].edges[2].nom:=6;
end;

procedure setosi;
begin
setlength(osi,4);
  osi[0].x:=0;
  osi[0].y:=0;
  osi[0].z:=0;

  osi[1].x:=200;
  osi[1].y:=0;
  osi[1].z:=0;

  osi[2].x:=0;
  osi[2].y:=200;
  osi[2].z:=0;

  osi[3].x:=0;
  osi[3].y:=0;
  osi[3].z:=200;

  setlength(osi[0].edges,3);
   osi[0].edges[0].nom :=1;
   osi[0].edges[1].nom :=2;
   osi[0].edges[2].nom :=3;
end;

procedure setpiramidka;
var i: integer;
begin
  setlength(tetra,4);
  tetra[0].x:=0;
  tetra[0].y:=100;
  tetra[0].z:=0;

  tetra[1].x:=87;
  tetra[1].y:=-50;
  tetra[1].z:=50;

  tetra[2].x:=-87;
  tetra[2].y:=-50;
  tetra[2].z:=50;

  tetra[3].x:=0;
  tetra[3].y:=-50;
  tetra[3].z:=-100;

  for i := 0 to length(tetra) - 1 do
    setlength(tetra[i].edges,3);
  tetra[0].edges[0].nom:=1;
  tetra[0].edges[1].nom:=2;
  tetra[0].edges[2].nom:=3;

  tetra[1].edges[0].nom:=0;
  tetra[1].edges[1].nom:=1;
  tetra[1].edges[2].nom:=2;

  tetra[2].edges[0].nom:=0;
  tetra[2].edges[1].nom:=1;
  tetra[2].edges[2].nom:=3;

  tetra[3].edges[0].nom:=0;
  tetra[3].edges[1].nom:=1;
  tetra[3].edges[2].nom:=2;
end;

procedure setoctaedr;
var i: integer;
begin
  setlength(octaedr,6);
  octaedr[0].x:=0;
  octaedr[0].y:=100;
  octaedr[0].z:=0;

  octaedr[1].x:=100;
  octaedr[1].y:=0;
  octaedr[1].z:=0;

  octaedr[2].x:=0;
  octaedr[2].y:=0;
  octaedr[2].z:=100;

  octaedr[3].x:=-100;
  octaedr[3].y:=0;
  octaedr[3].z:=0;

  octaedr[4].x:=0;
  octaedr[4].y:=0;
  octaedr[4].z:=-100;

  octaedr[5].x:=0;
  octaedr[5].y:=-100;
  octaedr[5].z:=0;

  for i := 0 to length(octaedr) - 1 do
    setlength(octaedr[i].edges,4);

  octaedr[0].edges[0].nom:=1;
  octaedr[0].edges[1].nom:=2;
  octaedr[0].edges[2].nom:=3;
  octaedr[0].edges[3].nom:=4;

  octaedr[1].edges[0].nom:=0;
  octaedr[1].edges[1].nom:=2;
  octaedr[1].edges[2].nom:=4;
  octaedr[1].edges[3].nom:=5;

  octaedr[2].edges[0].nom:=0;
  octaedr[2].edges[1].nom:=1;
  octaedr[2].edges[2].nom:=3;
  octaedr[2].edges[3].nom:=5;

  octaedr[3].edges[0].nom:=0;
  octaedr[3].edges[1].nom:=2;
  octaedr[3].edges[2].nom:=4;
  octaedr[3].edges[3].nom:=5;

  octaedr[4].edges[0].nom:=0;
  octaedr[4].edges[1].nom:=1;
  octaedr[4].edges[2].nom:=3;
  octaedr[4].edges[3].nom:=5;

  octaedr[5].edges[0].nom:=1;
  octaedr[5].edges[1].nom:=2;
  octaedr[5].edges[2].nom:=3;
  octaedr[5].edges[3].nom:=4;

end;

end.
