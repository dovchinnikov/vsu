program Project1;
{==========================================================}
{          ���������������� ������� � Delphi,  2007        }
{          ������� �., ���������� �., ����������� �.       }
{                                                          }
{                                                          }
{ ������ � 3D                                              }
{==========================================================}

uses
  Forms,
  UnitMain in 'UnitMain.PAS' {FormMain},
  UnitTools in 'UnitTools.pas' {FormTools},
  Lbr in 'Lbr.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormTools, FormTools);
  Application.Run;
end.
