unit UnitTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ComCtrls, Buttons, Lbr;

type
  TFormTools = class(TForm)
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    CheckBox1: TCheckBox;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    ComboBox1: TComboBox;
    Edit1: TEdit;
    Edit2: TEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormTools: TFormTools;

implementation

uses UnitMain;

{$R *.dfm}

procedure TFormTools.SpeedButton1Click(Sender: TObject);
begin
  Xmin:=0.8*Xmin;
  Xmax:=0.8*Xmax;
  Ymin:=0.8*Ymin;
  Ymax:=0.8*Ymax;
  FormMain.Show3D;
end;

procedure TFormTools.SpeedButton2Click(Sender: TObject);
begin
  Xmin:=1.2*Xmin;
  Xmax:=1.2*Xmax;
  Ymin:=1.2*Ymin;
  Ymax:=1.2*Ymax;
  FormMain.Show3D;
end;

procedure TFormTools.CheckBox1Click(Sender: TObject);
begin
  FormMain.Show3D;
end;

procedure TFormTools.TrackBar1Change(Sender: TObject);
begin
  FormMain.Show3D;
end;

end.
