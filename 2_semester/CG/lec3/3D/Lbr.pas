unit Lbr;

interface

var
  Xmin,Xmax,Ymin,Ymax: real;
  x,y: array[0..3] of integer;
  x0,y0,z0,Alf,Bet,A : real;

function F(k: byte; x,y: real): real;

implementation

function F(k: byte; x,y: real): real;
begin
  case k of
    0: F:=0.4+1.5*x*y-0.2*(x-y*y);
    1: F:=x*x+y*y;
    2: F:=0.5*x*x+y*y;
    3: F:=x*x-y*y;
    4: F:=0.1*x-y;
    5: F:=1+2*x*y-x-y;
  end;
end;

end.
