object FormTools: TFormTools
  Left = 810
  Top = 59
  Width = 258
  Height = 148
  Caption = 'FormTools'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 8
    Width = 23
    Height = 22
    Caption = '+'
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 32
    Top = 8
    Width = 23
    Height = 22
    Caption = '-'
    OnClick = SpeedButton2Click
  end
  object Label1: TLabel
    Left = 5
    Top = 59
    Width = 73
    Height = 19
    Alignment = taCenter
    AutoSize = False
    Caption = #1060#1091#1085#1082#1094#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object CheckBox1: TCheckBox
    Left = 69
    Top = 8
    Width = 97
    Height = 17
    Caption = #1054#1089#1080
    Checked = True
    State = cbChecked
    TabOrder = 0
    OnClick = CheckBox1Click
  end
  object TrackBar1: TTrackBar
    Left = 62
    Top = 28
    Width = 171
    Height = 24
    Max = 100
    Min = -100
    TabOrder = 1
    ThumbLength = 15
    OnChange = TrackBar1Change
  end
  object ComboBox1: TComboBox
    Left = 11
    Top = 77
    Width = 97
    Height = 21
    ItemHeight = 13
    ItemIndex = 5
    TabOrder = 2
    Text = 'z=1+2*x*y-x-y '
    Items.Strings = (
      'z=1+2*x*y-x-y'
      'z=x*x+y*y'
      'z=0.5*x*x+y*y'
      'z=x*x-y*y'
      'z=0.1*x-y'
      'z=1+2*x*y-x-y ')
  end
  object Edit1: TEdit
    Left = 176
    Top = 56
    Width = 49
    Height = 21
    TabOrder = 3
    Text = '10'
  end
  object Edit2: TEdit
    Left = 176
    Top = 80
    Width = 49
    Height = 21
    TabOrder = 4
    Text = '10'
  end
end
