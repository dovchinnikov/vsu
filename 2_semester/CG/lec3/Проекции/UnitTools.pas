unit UnitTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, Buttons, Lib, Math;

type
  TFormTools = class(TForm)
    Label1: TLabel;
    Label3: TLabel;
    RgBody: TRadioGroup;
    RgRotate: TRadioGroup;
    RgModel: TRadioGroup;
    ChkBoxAx: TCheckBox;
    ChkBoxPersp: TCheckBox;
    TrackBar1: TTrackBar;
    TrackBar3: TTrackBar;
    TrackBar4: TTrackBar;
    GrShow: TGroupBox;
    TrackBar2: TTrackBar;
    GrZoom: TGroupBox;
    ZoomBtn1: TSpeedButton;
    ZoomBtn2: TSpeedButton;
    BtnReset: TButton;
    BtnExit: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    RgPro: TRadioGroup;
    procedure ZoomBtn2Click(Sender: TObject);
    procedure ZoomBtn1Click(Sender: TObject);
    procedure RgBodyClick(Sender: TObject);
    procedure RgModelClick(Sender: TObject);
    procedure ChkBoxAxClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
    procedure TrackBar4Change(Sender: TObject);
    procedure BtnExitClick(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure RgProClick(Sender: TObject);
    procedure BtnResetClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormTools: TFormTools;

implementation

{$R *.dfm}

uses UnitMain;

procedure TFormTools.RgBodyClick(Sender: TObject);
begin
  case RgBody.ItemIndex of
    0: Body:=Tetraedr(SizeBody);
    1: Body:=Hexaedr(SizeBody);
  end;
  FormMain.Draw;
end;

procedure TFormTools.ZoomBtn1Click(Sender: TObject);
begin
  size:=1.2*Size;
  X1:=-Size; Y1:=-Size;
  X2:=Size;  Y2:=Size;
  FormMain.Draw;
end;

procedure TFormTools.ZoomBtn2Click(Sender: TObject);
begin
  size:=0.8*Size;
  X1:=-Size; Y1:=-Size;
  X2:=Size;  Y2:=Size;
  FormMain.Draw;
end;

procedure TFormTools.ChkBoxAxClick(Sender: TObject);
begin
  FormMain.Draw;
end;

procedure TFormTools.RgModelClick(Sender: TObject);
begin
  FormMain.Draw;
end;

procedure TFormTools.TrackBar1Change(Sender: TObject);
begin
  q:=-TrackBar1.Position/100;
  FormMain.Draw;
end;

procedure TFormTools.TrackBar2Change(Sender: TObject);
begin
  r:=-TrackBar2.Position/100;
  FormMain.Draw;
end;

procedure TFormTools.TrackBar3Change(Sender: TObject);
begin
  Xs:=-TrackBar3.Position/100;
  FormMain.Draw;
end;

procedure TFormTools.TrackBar4Change(Sender: TObject);
begin
  Zs:=-TrackBar4.Position/100;
  FormMain.Draw;
end;

procedure TFormTools.BtnExitClick(Sender: TObject);
begin
  FormMain.Close;
end;

procedure TFormTools.RgProClick(Sender: TObject);
begin
  ChkBoxPersp.Checked:=false;
  ChkBoxPersp.Enabled:=false;
  TrackBar3.Position:=0;
  TrackBar4.Position:=0;
  TrackBar1.Enabled:=false;
  TrackBar2.Enabled:=false;
  TrackBar3.Enabled:=false;
  TrackBar4.Enabled:=false;
  case RgPro.ItemIndex of
      0:begin
          Alf:=Pi/6; Bet:=-Pi/6;
        end;
    1,2:begin
          Bet:=-ArcCos(Sqrt(7/8));
          Bet1:=0;
          Alf:=ArcCos(Sqrt(8/9));
          Alf1:=0;
        end;
    3,4:begin
          Bet:=-Pi/4;
          Bet1:=0;
          Alf:=ArcCos(Sqrt(2/3));
          Alf1:=0;
        end;
      5:begin
          Alf:=Pi/6; Bet:=-Pi/6;
          TrackBar3.Enabled:=true;
          TrackBar4.Enabled:=true;
          ChkBoxPersp.Enabled:=true;
        end;
      6:begin
          q:=-0.5/Sqrt(2);
          r:=q;
          TrackBar1.Enabled:=true;
          TrackBar2.Enabled:=true;
        end;
      7:begin
          q:=-0.5/Sqrt(2);
          r:=q;
          Alf1:=0;
          Bet1:=0;
        end;
      8:begin
          q:=-1/Sqrt(2);
          r:=q;
          Alf1:=0;
          Bet1:=0;
        end;
  end;
  FormMain.Draw;
end;

procedure TFormTools.BtnResetClick(Sender: TObject);
begin
  Alf1:=0;
  Bet1:=0;
  case RgPro.ItemIndex of
      0:begin
          Alf:=Pi/6; Bet:=-Pi/6;
        end;
    1,2:begin
          Bet:=-ArcCos(Sqrt(7/8));
          Alf:=ArcCos(Sqrt(8/9));
        end;
    3,4:begin
          Bet:=-Pi/4;
          Alf:=ArcCos(Sqrt(2/3));
        end;
      5:begin
          Alf:=Pi/6; Bet:=-Pi/6;
          TrackBar3.Position:=0;
          TrackBar4.Position:=0;
        end;
      6:begin
          q:=-0.5/Sqrt(2);
          r:=q;
        end;
      7:begin
          q:=-0.5/Sqrt(2);
          r:=q;
        end;
      8:begin
          q:=-1/Sqrt(2);
          r:=q;
        end;
  end;
  FormMain.Draw;
end;

end.
