unit UnitMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Lib, Math;

type
  TFormMain = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Draw;
  end;

var
  FormMain: TFormMain;
  Size: real=8;
  Drawing: boolean;
  Bitmap: TBitmap;
  I1,I2,J1,J2: word;
  X1,Y1,X2,Y2: real;
  fl_xy: boolean=true;
  Ind: integer=1;
  SizeBody: real=1; // ��������� ������ ����
  Alf,Bet: real;    // ���� �������� ������������ ����
  Alf1,Bet1: real;  // ���� �������� ����
  q,r: real;        // ��������� ������������� �������������
  Xs: real=0;       // 1-� ����� �����
  Zs: real=0;       // 2-� ����� �����

implementation

uses UnitTools;

{$R *.dfm}

function XX(i:integer): real;
begin
  XX:=X1+(i-I1)*(X2-X1)/(I2-I1)
end;

function YY(j:integer): real;
begin
  YY:=Y1+(j-J2)*(Y2-Y1)/(J1-J2)
end;

function JJ(y:real):integer;
begin
  Result:=J1+Round((y-Y2)*(J2-J1)/(Y1-Y2))
end;

function II(x:real):integer;
begin
  Result:=I1+Round((x-X1)*(I2-I1)/(X2-X1))
end;

function IJ(Vt: TVector): TPoint;
begin
  case FormTools.RgPro.ItemIndex of
    0..5:begin  // ����������������� � ����������� ��������
           Vt:=Rotate(Vt, 2, Bet, 0, 0);
           Vt:=Rotate(Vt, 1, Alf, 0, 0);
         end;
    6..8:Vt:=Rotate(Vt, 5, 0, q, r);  // ������������ ��������
  end;
  Result.x:=II(Vt[1]);
  Result.Y:=JJ(Vt[2]);
end;

function ToVector(x,y,z: real): TVector;
begin
  Result[1]:=x;
  Result[2]:=y;
  Result[3]:=z;
  Result[4]:=1;
end;

procedure TFormMain.Draw;
const    // ���������� �����������
  ax=7; // ����� �������
var  
  i,j,L,L0,L1,u,v: integer;
  Vt,NN,V1,V2: TVector;
  Vn,Wn: array[0..2] of TVector;
  P0,P: TPoint;
  W: array of TPoint;
  M: TMatrix;
begin
  with Bitmap.Canvas, Body do begin
    Brush.Color:=clWhite;
    Pen.Color:=clBlack;
    Pen.Style:=psSolid;
    Rectangle(0,0,Width,Height);
    Pen.Mode:=pmCopy;
    // �������� � �������������
    L:=Length(Vertexs);
    for i:=0 to L-1 do begin
      VertexsT[i]:=Vertexs[i];
      case FormTools.RgPro.ItemIndex of
        0..5:begin  // ����������������� � ����������� ��������
               VertexsT[i]:=Rotate(VertexsT[i], 2, Bet1, 0, 0);// ������� ���� ������ OY
               VertexsT[i]:=Rotate(VertexsT[i], 1, Alf1, 0, 0);// ������� ���� ������ OX
               VertexsT[i]:=Rotate(VertexsT[i], 4, 0, Xs, Zs);
               VertexsT[i]:=Rotate(VertexsT[i], 2, Bet, 0, 0); // ������� ���� ������ OY
               VertexsT[i]:=Rotate(VertexsT[i], 1, Alf, 0, 0); // ������� ���� ������ OX
              end;
        6..8:begin  // ������������ ��������
               VertexsT[i]:=Rotate(VertexsT[i], 2, Bet1, 0, 0);// ������� ���� ������ OY
               VertexsT[i]:=Rotate(VertexsT[i], 1, Alf1, 0, 0);// ������� ���� ������ OX
               VertexsT[i]:=Rotate(VertexsT[i], 5, 0, q, r);
             end;
      end;
    end;
    // ���������� ����
    Pen.Color:=clGray;
    Pen.Style:=psDot;
    Brush.Style:=bsClear;
    if FormTools.ChkBoxAx.Checked then begin
      P0:=IJ(ToVector(0,0,0));
      P:=IJ(ToVector(ax,0,0));
      MoveTo(P0.X,P0.Y); LineTo(P.X,P.Y);
      TextOut(P.X,P.Y,'X');
      P:=IJ(ToVector(0,ax,0));
      MoveTo(P0.X,P0.Y); LineTo(P.X,P.Y);
      TextOut(P.X,P.Y,'Y');
      P:=IJ(ToVector(0,0,ax));
      MoveTo(P0.X,P0.Y); LineTo(P.X,P.Y);
      TextOut(P.X,P.Y,'Z');
    end;
    Brush.Style:=bsSolid;
    // ����������� ����� �����
    if FormTools.ChkBoxPersp.Checked then begin
      with Body0 do begin
        L:=Length(Vertexs);
        for i:=0 to L-1 do begin
          VertexsT[i]:=Vertexs[i];
          VertexsT[i]:=Rotate(VertexsT[i], 2, Bet1, 0, 0);
          VertexsT[i]:=Rotate(VertexsT[i], 1, Alf1, 0, 0);
          VertexsT[i]:=Rotate(VertexsT[i], 4, 0, Xs, Zs);
          VertexsT[i]:=Rotate(VertexsT[i], 2, Bet, 0, 0);
          VertexsT[i]:=Rotate(VertexsT[i], 1, Alf, 0, 0);
        end;
        L0:=Length(Edges);
        for i:=0 to L0-1 do begin
          MoveTo(II(VertexsT[Edges[i].p1][1]),JJ(VertexsT[Edges[i].p1][2]));
          LineTo(II(VertexsT[Edges[i].p2][1]),JJ(VertexsT[Edges[i].p2][2]));
        end;
      end; // with Body0
      if Xs<>0 then begin
        V1[2]:=1/Xs; V1[1]:=0; V1[3]:=0; V1[4]:=1;
        V1:=Rotate(V1, 2, Bet, 0, 0);
        V1:=Rotate(V1, 1, Alf, 0, 0);
        u:=II(V1[1]); v:=JJ(V1[2]);
        for i:=0 to 5 do
        if i in [0,1,4,5] then begin
          MoveTo(u,v); LineTo(II(Body0.VertexsT[i][1]),JJ(Body0.VertexsT[i][2]));
        end;
        Pen.Style:=psSolid;
        Pen.Color:=clBlack;
        RectAngle(u-2,v-2,u+2,v+2);
      end;
      if Zs<>0 then begin
        Pen.Color:=clGray;
        Pen.Style:=psDot;
        V2[1]:=0; V2[2]:=0; V2[3]:=1/Zs; V2[4]:=1;
        V2:=Rotate(V2, 2, Bet, 0, 0);
        V2:=Rotate(V2, 1, Alf, 0, 0);
        u:=II(V2[1]); v:=JJ(V2[2]);
        for i:=0 to 3 do begin
          MoveTo(u,v); LineTo(II(Body0.VertexsT[i][1]),JJ(Body0.VertexsT[i][2]));
        end;
        Pen.Style:=psSolid;
        Pen.Color:=clBlack;
        RectAngle(u-2,v-2,u+2,v+2);
      end;
    end;
    // ����������� ����
    Pen.Color:=clBlack;
    Pen.Style:=psSolid;
    case FormTools.RgModel.ItemIndex of
      0: begin     // ��������� ������
           L:=Length(Edges);
           for i:=0 to L-1 do begin
             MoveTo(II(Body.VertexsT[Body.Edges[i].p1][1]),
               JJ(VertexsT[Edges[i].p1][2]));
             LineTo(II(VertexsT[Edges[i].p2][1]),
               JJ(VertexsT[Edges[i].p2][2]));
           end;
         end;
      1: begin    // ����������� ������
           Brush.Color:=clSilver;
           Pen.Color:=clSilver;
           L1:=Length(Sides);
           L0:=Length(Sides[0].p);
           SetLength(w,L0);
           for i:=0 to L1-1 do begin
             for j:=0 to L0-1 do begin
               Vt:=Vertexs[Sides[i].p[j]];
               Vt:=Rotate(Vt, 1, Alf1,0,0);
               Vt:=Rotate(Vt, 2, Bet1,0,0);
               if j<=2 then Vn[j]:=Vt;
               Vt:=Rotate(Vt, 4,   0,Xs,Zs);
               Vt:=Rotate(Vt, 1, Alf,0,0);
               Vt:=Rotate(Vt, 2, Bet,0,0);
               w[j].X:=II(Vt[1]);
               w[j].Y:=JJ(Vt[2]);
               if j<=2 then Wn[j]:=Vt;
             end;
             Sides[i].n:=Lib.Norm(Vn[0],Vn[1],Vn[2]);
             if Sides[i].n[3]>=0 then
               Brush.Color:=Trunc(255*Sides[i].n[3])*$010000
             else
               Brush.Color:=Trunc(-255*Sides[i].n[3]/3)*$010000;
             Pen.Color:=Brush.Color;
             NN:=Lib.Norm(Wn[0],Wn[1],Wn[2]);
             if Lib.Norm(Wn[0],Wn[1],Wn[2])[3]<0 then
               Polygon(w);
           end;
         end;
    end; // case: �����/�����
  end;  // with Bitmap.Canvas, Body
  Canvas.Draw(0,0,Bitmap);
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=Width;
  Bitmap.Height:=Height;
  Drawing:=false;
  I1:=0; J1:=0;
  I2:=Width; J2:=Height;
  X1:=-Size; Y1:=-Size;
  X2:=Size;  Y2:=Size;
  Body:= Hexaedr(SizeBody);
  Body0:=Hexaedr(SizeBody);  // ��� ��� ���������� ����� �����
end;

procedure TFormMain.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Drawing:=True;
end;

procedure TFormMain.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Drawing:=false;
end;

procedure TFormMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var a, b: real;
begin
  if drawing then begin
    a:=x-Width div 2; b:=y-Height div 2;
    case FormTools.RgRotate.ItemIndex of
      1: case FormTools.RgPro.ItemIndex of
           0,5: begin
                Alf:=ArcTan2(b,a);
                Bet:=Sqrt(Sqr(a/10)+Sqr(b/10));
              end;
           1: begin
                Bet:=Sqrt(Sqr(a/10)+Sqr(b/10));
                Alf:=ArcTan(Sin(Bet));
              end;
           3:begin
               if fl_xy then begin
                 Bet:=Bet+Pi/2;
                 if Bet>1.5*Pi then begin
                   Bet:=Bet-2*Pi;
                   fl_xy:=false;
                 end;
               end
               else begin
                 case Ind of
                   1:Alf:=-Alf;
                   2:Alf:=Pi+Alf;
                   3:Alf:=-Alf;
                 end;
                 Inc(Ind);
                 if Ind=4 then begin
                   Alf:=Alf+Pi;
                   Ind:=1;
                 end;
                 fl_xy:=true;
               end;
             end;
         end;
      0: begin
           Alf1:=ArcTan2(b,a);
           Bet1:=Sqrt(Sqr(a/10)+Sqr(b/10));
         end;
    end;
    Draw;
  end;
end;

procedure TFormMain.FormPaint(Sender: TObject);
begin
  Draw;
end;

end.

