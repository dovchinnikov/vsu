unit Lib;

interface

type
  TVector = array[1..4] of Real;
  TMatrix = array[1..4] of TVector;

  TVertex=record
    x,y,z: real;
  end;
  TSide =record
    p: array of word;              // ������ ������
    A,B,C,D: single;               // ������������ ��������� ���������
    N: TVector;                    // ������ ������� � ���������
  end;
  TEdge=record
    p1,p2: word;                   // ������ ������
  end;
  TBody=record
    Vertexs  : array of TVector;  // ������ ��������� ��������� ������ ���� ���
    VertexsT : array of TVector;  // ������ ������� ��������� ������ ���� ���
    Edges    : array of TEdge;    // ������ ����� ���� ���
    Sides    : array of TSide;    // ������ ������ ���� ���
  end;

var
  Body,Body0: TBody;     //Sv: TVector=(-0.5,-0.5,5,1);
  M: TMatrix;  // ������� ��������������
  I: TMatrix=((1,0,0,0),(0,1,0,0),(0,0,1,0),(0,0,0,1));
  Zh: real=-4;

function Tetraedr(Size: real): TBody;
function Hexaedr(Size: real): TBody;
function Rotate(var V: TVector; k: Integer; fi,p,r: Real): TVector;
function Norm(V1,V2,V3: TVector): TVector;
function VM_Mult(var A: TVector; var B: TMatrix): TVector;

implementation

function Norm(V1,V2,V3: TVector): TVector;
var         // ���������� z-�������� ������� � ����� �������������
  A,B: TVector;
  u,v,w,d: real;
begin
  A[1]:=V2[1]-V1[1]; A[2]:=V2[2]-V1[2]; A[3]:=V2[3]-V1[3];
  B[1]:=V3[1]-V1[1]; B[2]:=V3[2]-V1[2]; B[3]:=V3[3]-V1[3];
  u:=A[2]*B[3]-A[3]*B[2];
  v:=-A[1]*B[3]+A[3]*B[1];
  w:=A[1]*B[2]-A[2]*B[1];
  d:=Sqrt(u*u+v*v+w*w);
  if d<>0 then begin
    Result[1]:=u/d;
    Result[2]:=v/d;
    Result[3]:=w/d;
  end
  else begin
    Result[1]:=0;
    Result[2]:=0;
    Result[3]:=0;
  end;
end;

function VM_Mult(var A: TVector; var B: TMatrix): TVector;
var     // ��������� ��������� 4-������� �� �������
  j,k: integer;
begin
  for j:=1 to 4 do begin
    Result[j]:=A[1]*B[1,j];
    for k:=2 to 4 do
      Result[j]:=Result[j]+A[k]*B[k,j];
  end;
  if Result[4]<>0 then
    for j:=1 to 3 do Result[j]:=Result[j]/Result[4];
  Result[4]:=1;
end;

function Rotate(var V: TVector; k: Integer; fi,p,r: Real): TVector;
  procedure Matr;
  begin
    M:=I;
    case k of
      1:begin  // ������� �������� ������ ��� OX
          M[2,2]:= cos(fi); M[2,3]:=sin(fi);
          M[3,2]:=-sin(fi); M[3,3]:=cos(fi);
        end;
      2:begin  // ������� �������� ������ ��� OY
          M[1,1]:=cos(fi); M[1,3]:=-sin(fi);
          M[3,1]:=sin(fi); M[3,3]:=cos(fi);
        end;
      3:begin // ������� �������� ������ ��� OZ
          M[1,1]:= cos(fi); M[1,2]:=sin(fi);
          M[2,1]:=-sin(fi); M[2,2]:=cos(fi);
        end;
      4:begin // ������������� ��������������
          M[2,4]:= p; M[3,4]:=r;
        end;
      5:begin // ������������ �������������
          M[3,1]:=p; M[3,2]:=r;
        end;
    end;
  end; // Matr
begin
  Matr;
  Result:=VM_Mult(V,M); // ��������� ������� �� ���� fi
end; // Rotate

function Tetraedr(Size: real): TBody;
var    // ���������� ���������
  i: Integer;
begin
  with Result do begin
    SetLength(Vertexs,4);     // �������
    SetLength(VertexsT,4);   // SetLength(VertexsO,4);
    Vertexs[0][1]:= Size; Vertexs[0][2]:=-Size; Vertexs[0][3]:=-Size;
    Vertexs[1][1]:= Size; Vertexs[1][2]:= Size; Vertexs[1][3]:= Size;
    Vertexs[2][1]:=-Size; Vertexs[2][2]:=-Size; Vertexs[2][3]:= Size;
    Vertexs[3][1]:=-Size; Vertexs[3][2]:= Size; Vertexs[3][3]:=-Size;
    for i:=0 to 3 do Vertexs[i][4]:=1;
    SetLength(Edges,6);       // �����
    Edges[0].p1:=0; Edges[0].p2:=1;
    Edges[1].p1:=0; Edges[1].p2:=2;
    Edges[2].p1:=1; Edges[2].p2:=2;
    Edges[3].p1:=3; Edges[3].p2:=0;
    Edges[4].p1:=3; Edges[4].p2:=1;
    Edges[5].p1:=3; Edges[5].p2:=2;
    SetLength(Sides,4);      // �����
    for i:=0 to 3 do SetLength(Sides[i].p,3);
    Sides[0].p[0]:=0; Sides[0].p[1]:=1; Sides[0].p[2]:=2;
    Sides[1].p[0]:=1; Sides[1].p[1]:=3; Sides[1].p[2]:=2;
    Sides[2].p[0]:=0; Sides[2].p[1]:=2; Sides[2].p[2]:=3;
    Sides[3].p[0]:=0; Sides[3].p[1]:=3; Sides[3].p[2]:=1;
  end;
end;

function Hexaedr(Size: real): TBody;
var   // ���������� ���������
  i: Integer;
begin
  with Result do begin
    SetLength(Vertexs,8);     // �������
    SetLength(VertexsT,8);    // SetLength(VertexsO,8);
    Vertexs[0][1]:=-Size; Vertexs[0][2]:=-Size; Vertexs[0][3]:=-Size;
    Vertexs[1][1]:= Size; Vertexs[1][2]:=-Size; Vertexs[1][3]:=-Size;
    Vertexs[2][1]:= Size; Vertexs[2][2]:= Size; Vertexs[2][3]:=-Size;
    Vertexs[3][1]:=-Size; Vertexs[3][2]:= Size; Vertexs[3][3]:=-Size;
    for i:=0 to 3 do begin
      Vertexs[i+4]:=Vertexs[i];
      Vertexs[i+4][3]:=Size;
    end;
    for i:=0 to 7 do Vertexs[i][4]:=1;
    SetLength(Edges,12);      // �����
    Edges[ 0].p1:=0; Edges[ 0].p2:=4;
    Edges[ 1].p1:=1; Edges[ 1].p2:=5;
    Edges[ 2].p1:=2; Edges[ 2].p2:=6;
    Edges[ 3].p1:=3; Edges[ 3].p2:=7;
    Edges[ 4].p1:=0; Edges[ 4].p2:=1;
    Edges[ 5].p1:=1; Edges[ 5].p2:=2;
    Edges[ 6].p1:=2; Edges[ 6].p2:=3;
    Edges[ 7].p1:=3; Edges[ 7].p2:=0;
    Edges[ 8].p1:=4; Edges[ 8].p2:=5;
    Edges[ 9].p1:=5; Edges[ 9].p2:=6;
    Edges[10].p1:=6; Edges[10].p2:=7;
    Edges[11].p1:=7; Edges[11].p2:=4;
    SetLength(Sides,6);    // �����
    for i:=0 to 5 do SetLength(Sides[i].p,4);
    Sides[0].p[0]:=3; Sides[0].p[1]:=2; Sides[0].p[2]:=1;  Sides[0].p[3]:=0;
    Sides[1].p[0]:=4; Sides[1].p[1]:=5; Sides[1].p[2]:=6;  Sides[1].p[3]:=7;
    Sides[2].p[0]:=5; Sides[2].p[1]:=1; Sides[2].p[2]:=2;  Sides[2].p[3]:=6;
    Sides[3].p[0]:=6; Sides[3].p[1]:=2; Sides[3].p[2]:=3;  Sides[3].p[3]:=7;
    Sides[4].p[0]:=3; Sides[4].p[1]:=0; Sides[4].p[2]:=4;  Sides[4].p[3]:=7;
    Sides[5].p[0]:=0; Sides[5].p[1]:=1; Sides[5].p[2]:=5;  Sides[5].p[3]:=4;
  end;
end;

end.
