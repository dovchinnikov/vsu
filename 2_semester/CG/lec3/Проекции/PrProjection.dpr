program PrProjection;

uses
  Forms,
  UnitMain in 'UnitMain.pas' {FormMain},
  UnitTools in 'UnitTools.pas' {FormTools},
  Lib in 'Lib.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormTools, FormTools);
  Application.Run;
end.
