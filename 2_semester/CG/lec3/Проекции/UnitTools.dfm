object FormTools: TFormTools
  Left = -5
  Top = 52
  Width = 224
  Height = 515
  BorderStyle = bsSizeToolWin
  Caption = 'Tools'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 216
    Height = 481
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1042#1080#1076#1099' '#1087#1088#1086#1077#1082#1094#1080#1081
      object RgPro: TRadioGroup
        Left = 0
        Top = 0
        Width = 208
        Height = 297
        Align = alTop
        Items.Strings = (
          #1058#1088#1080#1084#1077#1090#1088#1080#1095#1077#1089#1082#1072#1103
          #1044#1080#1084#1077#1090#1088#1080#1095#1077#1089#1082#1072#1103
          #1057#1090#1072#1085#1076#1072#1088#1090#1085#1072#1103' '#1076#1080#1084#1077#1090#1088#1080#1095#1077#1089#1082#1072#1103
          #1048#1079#1086#1084#1077#1090#1088#1080#1095#1077#1089#1082#1072#1103
          #1057#1090#1072#1085#1076#1072#1088#1090#1085#1072#1103' '#1080#1079#1086#1084#1077#1090#1088#1080#1095#1077#1089#1082#1072#1103
          #1062#1077#1085#1090#1088#1072#1083#1100#1085#1072#1103
          #1050#1086#1089#1086#1091#1075#1086#1083#1100#1085#1072#1103
          #1050#1072#1073#1080#1085#1077#1090#1085#1072#1103' '
          #1050#1072#1074#1072#1083#1100#1077#1088#1085#1072#1103)
        TabOrder = 0
        OnClick = RgProClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1044#1077#1081#1089#1090#1074#1080#1103
      ImageIndex = 1
      object Label1: TLabel
        Left = 10
        Top = 320
        Width = 70
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = #1058#1086#1095#1082#1080' '#1089#1093#1086#1076#1072
      end
      object Label3: TLabel
        Left = 10
        Top = 232
        Width = 108
        Height = 13
        Alignment = taCenter
        Caption = #1059#1075#1083#1099' '#1087#1088#1086#1077#1094#1080#1088#1086#1074#1072#1085#1080#1103
      end
      object RgBody: TRadioGroup
        Left = 10
        Top = 10
        Width = 80
        Height = 80
        Caption = #1058#1077#1083#1086
        ItemIndex = 1
        Items.Strings = (
          #1058#1077#1090#1088#1072#1101#1076#1088
          #1043#1077#1082#1089#1072#1101#1076#1088)
        TabOrder = 0
        OnClick = RgBodyClick
      end
      object RgModel: TRadioGroup
        Left = 101
        Top = 10
        Width = 90
        Height = 80
        Caption = #1052#1086#1076#1077#1083#1100
        ItemIndex = 0
        Items.Strings = (
          #1050#1072#1088#1082#1072#1089#1085#1072#1103
          #1057#1087#1083#1086#1096#1085#1072#1103)
        TabOrder = 1
        OnClick = RgModelClick
      end
      object RgRotate: TRadioGroup
        Left = 122
        Top = 98
        Width = 70
        Height = 80
        Caption = #1042#1088#1072#1097#1072#1090#1100
        ItemIndex = 0
        Items.Strings = (
          #1058#1077#1083#1086
          #1054#1089#1080)
        TabOrder = 2
      end
      object TrackBar1: TTrackBar
        Left = 10
        Top = 287
        Width = 187
        Height = 30
        Max = 100
        Frequency = 2
        Position = 12
        TabOrder = 3
        OnChange = TrackBar1Change
      end
      object TrackBar2: TTrackBar
        Left = 10
        Top = 255
        Width = 187
        Height = 30
        Max = 100
        Frequency = 2
        Position = 12
        TabOrder = 4
        OnChange = TrackBar2Change
      end
      object TrackBar3: TTrackBar
        Left = 10
        Top = 375
        Width = 190
        Height = 30
        Max = 100
        Frequency = 2
        TabOrder = 5
        OnChange = TrackBar3Change
      end
      object TrackBar4: TTrackBar
        Left = 10
        Top = 340
        Width = 190
        Height = 30
        Max = 100
        Frequency = 2
        TabOrder = 6
        OnChange = TrackBar4Change
      end
      object GrZoom: TGroupBox
        Left = 72
        Top = 180
        Width = 73
        Height = 49
        Caption = #1052#1072#1089#1096#1090#1072#1073
        TabOrder = 7
        object ZoomBtn1: TSpeedButton
          Left = 42
          Top = 19
          Width = 23
          Height = 22
          Caption = '-'
          OnClick = ZoomBtn1Click
        end
        object ZoomBtn2: TSpeedButton
          Left = 10
          Top = 19
          Width = 23
          Height = 22
          Caption = '+'
          OnClick = ZoomBtn2Click
        end
      end
      object GrShow: TGroupBox
        Left = 10
        Top = 97
        Width = 100
        Height = 80
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100
        TabOrder = 8
        object ChkBoxAx: TCheckBox
          Left = 5
          Top = 23
          Width = 52
          Height = 17
          Caption = #1054#1089#1080
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = ChkBoxAxClick
        end
        object ChkBoxPersp: TCheckBox
          Left = 5
          Top = 47
          Width = 90
          Height = 17
          Caption = #1055#1077#1088#1089#1087#1077#1082#1090#1080#1074#1072
          TabOrder = 1
          OnClick = ChkBoxAxClick
        end
      end
      object BtnExit: TButton
        Left = 133
        Top = 421
        Width = 75
        Height = 25
        Caption = 'Exit'
        TabOrder = 9
        OnClick = BtnExitClick
      end
      object BtnReset: TButton
        Left = 0
        Top = 421
        Width = 75
        Height = 25
        Caption = 'Reset'
        TabOrder = 10
        OnClick = BtnResetClick
      end
    end
  end
end
