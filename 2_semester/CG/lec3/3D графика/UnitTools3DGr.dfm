object FormTools3DGr: TFormTools3DGr
  Left = 883
  Top = 190
  Caption = 'Tools 3D Gr'
  ClientHeight = 267
  ClientWidth = 163
  Color = clBtnFace
  UseDockManager = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Visible = True
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 2
    Top = 108
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888800888888888888800088888888888800088888800008870
      08888800FFFF00078888807FFFFFF708888880FFF11FFF0888880FFFF11FFFF0
      88880FF111111FF088880FF111111FF088880FFFF11FFFF0888880FFF11FFF08
      8888807FFFFFF70888888800FFFF008888888888000088888888}
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 28
    Top = 108
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888800888888888888800088888888888800088888800008870
      08888800FFFF00078888807FFFFFF708888880FFFFFFFF0888880FFFFFFFFFF0
      88880FF111111FF088880FF111111FF088880FFFFFFFFFF0888880FFFFFFFF08
      8888807FFFFFF70888888800FFFF008888888888000088888888}
    OnClick = SpeedButton2Click
  end
  object Label1: TLabel
    Left = 74
    Top = 8
    Width = 11
    Height = 13
    Caption = 'x1'
  end
  object Label2: TLabel
    Left = 74
    Top = 30
    Width = 11
    Height = 13
    Caption = 'x2'
  end
  object Label3: TLabel
    Left = 74
    Top = 51
    Width = 11
    Height = 13
    Caption = 'y1'
  end
  object Label4: TLabel
    Left = 74
    Top = 72
    Width = 11
    Height = 13
    Caption = 'y2'
  end
  object SpeedButton3: TSpeedButton
    Left = 98
    Top = 158
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888888888888888888888888888
      8888888888888888888888888888888888888800000000000888888000000000
      8888888800000008888888888000008888888888880008888888888888808888
      8888888888888888888888888888888888888888888888888888}
    OnClick = SpeedButton3Click
  end
  object SpeedButton4: TSpeedButton
    Tag = 1
    Left = 98
    Top = 184
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888888888888888888888888888
      8888888888888888888888888880888888888888880008888888888880000088
      8888888800000008888888800000000088888800000000000888888888888888
      8888888888888888888888888888888888888888888888888888}
    OnClick = SpeedButton3Click
  end
  object SpeedButton5: TSpeedButton
    Tag = 3
    Left = 126
    Top = 173
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888808888888888888880088888
      8888888880008888888888888000088888888888800000888888888880000008
      8888888880000088888888888000088888888888800088888888888880088888
      8888888880888888888888888888888888888888888888888888}
    OnClick = SpeedButton3Click
  end
  object SpeedButton6: TSpeedButton
    Tag = 2
    Left = 72
    Top = 172
    Width = 23
    Height = 22
    Glyph.Data = {
      F6000000424DF600000000000000760000002800000010000000100000000100
      0400000000008000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888888888888888888888888888888888888888808888888888888800888
      8888888888000888888888888000088888888888000008888888888000000888
      8888888800000888888888888000088888888888880008888888888888800888
      8888888888880888888888888888888888888888888888888888}
    OnClick = SpeedButton3Click
  end
  object Label5: TLabel
    Left = 79
    Top = 98
    Width = 11
    Height = 13
    Caption = 'x0'
  end
  object Label6: TLabel
    Left = 79
    Top = 118
    Width = 11
    Height = 13
    Caption = 'y0'
  end
  object Label7: TLabel
    Left = 80
    Top = 140
    Width = 11
    Height = 13
    Caption = 'z0'
  end
  object CheckBox1: TCheckBox
    Left = 5
    Top = 184
    Width = 43
    Height = 17
    Caption = #1054#1089#1080
    Checked = True
    State = cbChecked
    TabOrder = 0
    OnClick = CheckBox1Click
  end
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 0
    Width = 65
    Height = 105
    Caption = 'Function'
    ItemIndex = 0
    Items.Strings = (
      '0'
      '1'
      '2'
      '3'
      '4'
      '5'
      '6')
    TabOrder = 1
    OnClick = RadioGroup1Click
  end
  object Edit1: TEdit
    Left = 98
    Top = 1
    Width = 57
    Height = 21
    TabOrder = 2
    Text = '0'
    OnKeyDown = Edit1KeyDown
  end
  object Edit2: TEdit
    Tag = 1
    Left = 98
    Top = 25
    Width = 57
    Height = 21
    TabOrder = 3
    Text = '1'
    OnKeyDown = Edit1KeyDown
  end
  object Edit3: TEdit
    Tag = 2
    Left = 98
    Top = 46
    Width = 57
    Height = 21
    TabOrder = 4
    Text = '0'
    OnKeyDown = Edit1KeyDown
  end
  object Edit4: TEdit
    Tag = 3
    Left = 98
    Top = 67
    Width = 57
    Height = 21
    TabOrder = 5
    Text = '1'
    OnKeyDown = Edit1KeyDown
  end
  object Edit5: TEdit
    Tag = 4
    Left = 98
    Top = 93
    Width = 57
    Height = 21
    TabOrder = 6
    Text = '0.5'
    OnKeyDown = Edit1KeyDown
  end
  object Edit6: TEdit
    Tag = 5
    Left = 98
    Top = 114
    Width = 57
    Height = 21
    TabOrder = 7
    Text = '0.5'
    OnKeyDown = Edit1KeyDown
  end
  object Edit7: TEdit
    Tag = 6
    Left = 98
    Top = 135
    Width = 57
    Height = 21
    TabOrder = 8
    Text = '0.5'
    OnKeyDown = Edit1KeyDown
  end
  object CheckBox2: TCheckBox
    Left = 4
    Top = 216
    Width = 93
    Height = 17
    Caption = #1058#1088#1077#1091#1075#1086#1083#1100#1085#1080#1082#1080
    Checked = True
    State = cbChecked
    TabOrder = 9
    OnClick = CheckBox2Click
  end
  object CheckBox3: TCheckBox
    Left = 4
    Top = 200
    Width = 53
    Height = 17
    Caption = #1051#1080#1085#1080#1080
    Checked = True
    State = cbChecked
    TabOrder = 10
    OnClick = CheckBox3Click
  end
  object TrackBar1: TTrackBar
    Left = -1
    Top = 233
    Width = 162
    Height = 23
    Max = 100
    Min = -100
    TabOrder = 11
    OnChange = TrackBar1Change
  end
  object Edit8: TEdit
    Left = 3
    Top = 136
    Width = 53
    Height = 21
    TabOrder = 12
    Text = '10'
    OnKeyDown = Edit8KeyDown
  end
  object Edit9: TEdit
    Left = 3
    Top = 160
    Width = 53
    Height = 21
    TabOrder = 13
    Text = '10'
    OnKeyDown = Edit9KeyDown
  end
end
