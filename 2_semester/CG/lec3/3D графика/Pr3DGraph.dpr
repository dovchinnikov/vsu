program Pr3DGraph;

uses
  Forms,
  UnitMain in 'UnitMain.pas' {FormMain},
  Lib3dGraph in 'Lib3dGraph.pas',
  UnitTools3DGr in 'UnitTools3DGr.pas' {FormTools3DGr};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormTools3DGr, FormTools3DGr);
  Application.Run;
end.
