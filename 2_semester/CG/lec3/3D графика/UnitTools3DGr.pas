unit UnitTools3DGr;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Lib3dGraph, StdCtrls, ExtCtrls, Buttons, Spin, ComCtrls;

type
  TFormTools3DGr = class(TForm)
    CheckBox1: TCheckBox;
    RadioGroup1: TRadioGroup;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    TrackBar1: TTrackBar;
    Edit8: TEdit;
    Edit9: TEdit;
    procedure CheckBox1Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton3Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure CheckBox3Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Edit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormTools3DGr: TFormTools3DGr;

implementation

uses UnitMain;

{$R *.dfm}

procedure TFormTools3DGr.CheckBox1Click(Sender: TObject);
begin
  Graph3D.VisibleAxes:=CheckBox1.Checked;
  //FormMain.MyDraw;
end;

procedure TFormTools3DGr.RadioGroup1Click(Sender: TObject);
begin
  Graph3D.Func:=F(RadioGroup1.ItemIndex);
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.SpeedButton1Click(Sender: TObject);
begin
  with Graph3D do begin
    Xmin:=Xmin*0.8; Xmax:=Xmax*0.8;
    Ymin:=Ymin*0.8; Ymax:=Ymax*0.8;
  end;
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.SpeedButton2Click(Sender: TObject);
begin
  with Graph3D do begin
    Xmin:=Xmin*1.2; Xmax:=Xmax*1.2;
    Ymin:=Ymin*1.2; Ymax:=Ymax*1.2;
  end;  
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Code: integer;
  temp: real;
begin
  with Sender as TEdit do
  if Key=13 then begin
    Val(Text,temp,Code);
    if Code=0 then
    with Graph3D do begin
      case tag of
        0: x1:=temp;
        1: x2:=temp;
        2: y1:=temp;
        3: y2:=temp;
        4: xc:=temp;
        5: yc:=temp;
        6: zc:=temp;
      end;
      FormMain.MyDraw;
    end;
  end;
end;

procedure TFormTools3DGr.SpeedButton3Click(Sender: TObject);
begin
  with Sender as TSpeedButton,Graph3D do begin
    case tag of
      0: begin
           YMin:=YMin-0.1; YMax:=YMax-0.1;
         end;
      1: begin
           YMin:=YMin+0.1; YMax:=YMax+0.1;
         end;
      2: begin
           XMin:=XMin+0.1; XMax:=XMax+0.1;
         end;
      3: begin
           XMin:=XMin-0.1; XMax:=XMax-0.1;
         end;
    end;
  end;
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.CheckBox2Click(Sender: TObject);
begin
  Graph3D.FTypeFaceTRIANGLES:=CheckBox2.Checked;
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.CheckBox3Click(Sender: TObject);
begin
  Graph3D.FtypeFace:=CheckBox3.Checked;
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.TrackBar1Change(Sender: TObject);
begin
  Graph3D.A:=TrackBar1.Position/100;
  FormMain.MyDraw;
end;

procedure TFormTools3DGr.Edit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  temp,Code: integer;
begin
  if Key=13 then begin
    Val(Edit8.Text,temp,Code);
    if Code=0 then
    with Graph3D do begin
      nX:=temp;
      FormMain.MyDraw;
    end;
  end;
end;

procedure TFormTools3DGr.Edit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  temp,Code: integer;
begin
  if Key=13 then begin
    Val(Edit9.Text,temp,Code);
    if Code=0 then
    with Graph3D do begin
      nY:=temp;
      FormMain.MyDraw;
    end;
  end;
end;

end.
