unit UnitMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Lib3dGraph, ExtCtrls;

type
  TFormMain = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MyDraw;
  end;

var
  FormMain: TFormMain;
  drawing: boolean;
  fl_function: integer=0;

function F(k: byte): TF;

implementation

{$R *.dfm}

function F0(x,y: real): real;
begin
  Result:=0.4+1.5*x*y-0.2*(x-y*y);
end;

function F1(x,y: real): real;
begin
  Result:=x*x+y*y;
end;

function F2(x,y: real): real;
begin
  Result:=0.5*x*x+y*y;
end;

function F3(x,y: real): real;
begin
  Result:=x*x-y*y;
end;

function F4(x,y: real): real;
begin
  Result:=0.1*x-y;
end;

function F5(x,y: real): real;
begin
  Result:=1+2*x*y-x-y;
end;

function F6(x,y: real): real;
var
  r,q: real;
begin
  r:=Sqrt(x*x+y*y);
  q:=4*x*x+y*y;
  Result:=q+Exp(-3*r)*Cos(r*25);
end;

function F(k: byte): TF;
begin
  case k of
    0: Result:=@F0;
    1: Result:=@F1;
    2: Result:=@F2;
    3: Result:=@F3;
    4: Result:=@F4;
    5: Result:=@F5;
    6: Result:=@F6;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  drawing:=false;
  Graph3D:=TGraph3D.Create(ClientWidth,ClientHeight,F(fl_function));
end;

procedure TFormMain.MyDraw;
begin
  Graph3D.Draw3D;
  Canvas.Draw(0,0,Graph3D.Bitmap);
end;

procedure TFormMain.FormPaint(Sender: TObject);
begin
  MyDraw;
end;

procedure TFormMain.FormResize(Sender: TObject);
begin
  Graph3D.SetWH(ClientWidth,ClientHeight);
  MyDraw;
end;

procedure TFormMain.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=true;
end;

procedure TFormMain.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=false;
end;

procedure TFormMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if drawing then begin
    Graph3D.SetAngle(x-Width div 2,y-Height div 2);
    MyDraw;
  end;
end;

end.
