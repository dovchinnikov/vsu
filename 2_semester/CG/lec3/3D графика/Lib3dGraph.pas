unit Lib3dGraph;

interface
uses
  Types, Graphics,Controls, Math;

type
  TF=function(x,y: real): real;

  TVector=record
    x,y,z: real;
  end;
  TSides=record
    P: array[0..3] of TVector;
    N: real;
    Zmin: real;
  end;

  TGraph3D=class(TObject)
    Xmin  : real;    // ������������� �� ������
    Xmax  : real;
    Ymin  : real;
    Ymax  : real;
    FX1   : real;    // ��� � ������� ��
    FX2   : real;
    FY1   : real;
    FY2   : real;
    Fxc   : real;    // �����
    Fyc   : real;
    Fzc   : real;
    n     : integer; // ����� ��������� �� X
    m     : integer; // ����� ��������� �� Y
    Alf   : real;
    Bet   : real;
    FA    : real;
    Bitmap: TBitmap;
    V     : array of array of TVector; // �������� Z
    FFunc : TF;
    FVisibleAxes: boolean;
    FtypeFace: boolean;                // true - Line, false - Face
    FTypeFaceTRIANGLES: boolean;       // GL_TRIANGLES GL_QUADS
    Sides: array of TSides;            // �����
    procedure SetVisibleAxes(V: boolean);
    procedure SetF(VF: TF);
    procedure SetX1(V: real);
    procedure SetX2(V: real);
    procedure SetY1(V: real);
    procedure SetY2(V: real);
    procedure SetN (V: integer);
    procedure SetM (V: integer);
    procedure SetXc(V: real);
    procedure SetYc(V: real);
    procedure SetZc(V: real);
    procedure SetA (V: real);
    function  GetNx: integer;
    function  GetNy: integer;
  public
    constructor Create(VW,VH: integer; VF: TF);
    destructor Destroy; override;
    function  IJ(P: TVector): TPoint;
    procedure SetV;
    procedure DrawCub;
    procedure DrawAxes;
    procedure Draw3D;
    property  VisibleAxes: boolean read FVisibleAxes write SetVisibleAxes;
    property  Func: TF read FFunc write SetF;
    property  X1: real read Fx1 write SetX1;
    property  X2: real read Fx2 write SetX2;
    property  Y1: real read Fy1 write SetY1;
    property  Y2: real read Fy2 write SetY2;
    property  Nx: integer read GetNx write SetN;
    property  Ny: integer read GetNy write SetM;
    property  Xc: real read Fxc write SetXc;
    property  Yc: real read Fyc write SetYc;
    property  Zc: real read Fzc write SetZc;
    property  A : real read FA write SetA;
    procedure SetWH(VW,VH: integer);
    procedure SetAngle(x,y: integer);
    function Rotate(P: TVector): TVector;
    procedure Sort;
    function VectorToPoint(P: TVector): TPoint;
  end;

var
  Graph3D: TGraph3D;

implementation

var
  P0,P1: TPoint;

function TGraph3D.GetNx: integer;
begin
  Result:=n;
end;

function TGraph3D.GetNy: integer;
begin
  Result:=m;
end;

procedure TGraph3D.SetAngle(x,y: integer);
begin
  Alf:=ArcTan2(y,x);
  Bet:=Sqrt(Sqr(x/10)+Sqr(y/10));
end;

procedure TGraph3D.SetWH(VW,VH: integer);
begin
  Bitmap.Width:=VW;
  Bitmap.Height:=VH;
end;

procedure TGraph3D.SetN(V: integer);
begin
  if n<>V then begin
    n:=V; SetV;
  end;
end;

procedure TGraph3D.SetM(V: integer);
begin
  if m<>V then begin
    m:=V; SetV;
  end;
end;

procedure TGraph3D.SetX1(V: real);
begin
  if Fx1<>V then begin
    Fx1:=V; SetV;
  end;
end;

procedure TGraph3D.SetA(V: real);
begin
  if FA<>V then begin
    FA:=V; SetV;
  end;
end;

procedure TGraph3D.SetX2(V: real);
begin
  if Fx2<>V then begin
    Fx2:=V; SetV;
  end;
end;

procedure TGraph3D.SetY1(V: real);
begin
  if Fy1<>V then begin
    Fy1:=V; SetV;
  end;
end;

procedure TGraph3D.SetY2(V: real);
begin
  if Fy2<>V then begin
    Fy2:=V; SetV;
  end;
end;

procedure TGraph3D.SetXc(V: real);
begin
  if Fxc<>V then Fxc:=V;
end;

procedure TGraph3D.SetYc(V: real);
begin
  if Fyc<>V then Fyc:=V;
end;

procedure TGraph3D.SetZc(V: real);
begin
  if Fzc<>V then Fzc:=V;
end;

procedure TGraph3D.SetVisibleAxes(V: boolean);
begin
  if FVisibleAxes<>V then FVisibleAxes:=V;
  Draw3D;
end;

procedure TGraph3D.SetF(VF: TF);
begin
  if @FFunc<>@VF then begin
    FFunc:=VF; SetV;
  end;
end;

function RealToVector(x,y,z: real): TVector;
begin
  Result.x:=x;
  Result.y:=y;
  Result.z:=z;
end;

constructor TGraph3D.Create(VW,VH: integer; VF: TF);
begin
  Xmin := -2;
  Xmax :=  2;
  Ymin := -2;
  Ymax :=  2;
  FX1  :=  0;
  FX2  :=  1;
  FY1  :=  0;
  FY2  :=  1;
  Fxc  := 0.5;
  Fyc  := 0.5;
  Fzc  := 0.5;
  n    := 10;
  m    := 10;
  Alf  := 4.31;
  Bet  := 4.92;
  FFunc:= VF;
  FA   := 0;
  FVisibleAxes:=true;
  FtypeFace:=true;
  FTypeFaceTRIANGLES:=true;
  Bitmap:=TBitmap.Create;
  Bitmap.Width:=VW;
  Bitmap.Height:=VH;
  SetV;
end;

destructor TGraph3D.Destroy;
begin
  Bitmap.Free;
  inherited Destroy;
end;

function TGraph3D.Rotate(P: TVector): TVector;
begin
  Result.x:=(P.x-Fxc)*cos(alf)-(P.y-Fyc)*sin(alf);
  Result.y:=((P.x-Fxc)*sin(alf)+(P.y-Fyc)*cos(alf))*cos(Bet)-(P.z-Fzc)*sin(Bet);
  Result.z:=((P.x-Fxc)*sin(alf)+(P.y-Fyc)*cos(alf))*sin(Bet)+(P.z-Fzc)*cos(Bet);
end;

function TGraph3D.VectorToPoint(P: TVector): TPoint;
begin
  with Bitmap do begin
    Result.X:=Trunc(Width*(P.x-Xmin)/(Xmax-Xmin));
    Result.Y:=Trunc(Height*(P.y-Ymax)/(Ymin-Ymax))
  end;
end;

function TGraph3D.IJ(P: TVector): TPoint;
var Xn,Yn,Zn: real;
begin
  Xn:=(P.x-Fxc)*cos(alf)-(P.y-Fyc)*sin(alf);
  Yn:=((P.x-Fxc)*sin(alf)+(P.y-Fyc)*cos(alf))*cos(Bet)-(P.z-Fzc)*sin(Bet);
  Zn:=((P.x-Fxc)*sin(alf)+(P.y-Fyc)*cos(alf))*sin(Bet)+(P.z-Fzc)*cos(Bet);
  Xn:=Xn/(Zn*A+1); Yn:=Yn/(Zn*A+1);
  Result.x:=Trunc(Bitmap.Width*(Xn-Xmin)/(Xmax-Xmin));
  Result.y:=Trunc(Bitmap.Height*(Yn-Ymax)/(Ymin-Ymax))
end;

procedure TGraph3D.SetV;
var
  i,j: integer;
  hx,hy: real;
begin
  SetLength(V,n,m);
  hx:=(Fx2-Fx1)/(m-1); hy:=(Fy2-Fy1)/(n-1);
  for i:=0 to n-1 do
  for j:=0 to m-1 do begin
    V[i,j].x:=Fx1+j*hx;
    V[i,j].y:=Fy1+i*hy;
    V[i,j].z:=FFunc(Fx1+j*hx,Fy1+i*hy);
  end;
end;

procedure TGraph3D.DrawAxes;
begin
  with Bitmap.Canvas do begin
    Pen.Color:=clSilver;
    Pen.Width:=1;
    Pen.Style:=psSolid;
    P0:=IJ(RealToVector(0,0,0));
    P1:=IJ(RealToVector(1.2,0,0));
    MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
    TextOut(P1.X,P1.Y,'X');
    P1:=IJ(RealToVector(0,1.2,0));
    MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
    TextOut(P1.X,P1.Y,'Y');
    P1:=IJ(RealToVector(0,0,1.2));
    MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
    TextOut(P1.X,P1.Y,'Z');
  end;
end;

function Norm(V1,V2,V3: TVector): real;
// ���������� z-�������� ������� � ����� �������������
var
  A,B: TVector;
  u,v,w,d: real;
begin
  A.x:=V2.x-V1.x; A.y:=V2.y-V1.y; A.z:=V2.z-V1.z;
  B.x:=V3.x-V1.x; B.y:=V3.y-V1.y; B.z:=V3.z-V1.z;
  u:=A.y*B.z-A.z*B.y;
  v:=-A.x*B.z+A.z*B.x;
  w:=A.x*B.y-A.y*B.x;
  d:=Sqrt(u*u+v*v+w*w);
  if d<>0 then
    Result:=w/d
  else
    Result:=0;
end;

function Z_Min(Sides: TSides): real;
var i: byte;
begin
  with Sides do begin
    Result:=0;
    for i:=0 to 3 do Result:=Result+P[i].z/4;
  end;
end;

procedure TGraph3D.Sort;
var
  i,j,L: integer;
  T: TSides;
begin
  L:=Length(Sides);
  for i:=1 to L-1 do begin
  for j:=L-1 downto i do
    if Sides[j-1].Zmin>Sides[j].Zmin then begin // ��������� ���������
      t:=Sides[j]; Sides[j]:=Sides[j-1]; Sides[j-1]:=t
    end
  end;
end;

procedure TGraph3D.DrawCub;
var
  P: array[0..1,0..1,0..1] of TPoint;
begin
  with Bitmap.Canvas do begin
    Pen.Color:=clSilver;
    Pen.Width:=1;
    Pen.Style:=psSolid;
    P0:=IJ(RealToVector(0,0,0));

    P[0,0,0]:=IJ(RealToVector(0,0,0));
    P[1,0,0]:=IJ(RealToVector(1,0,0));
    P[0,1,0]:=IJ(RealToVector(0,1,0));
    P[1,1,0]:=IJ(RealToVector(1,1,0));
    P[0,0,1]:=IJ(RealToVector(0,0,1));
    P[1,0,1]:=IJ(RealToVector(1,0,1));
    P[0,1,1]:=IJ(RealToVector(0,1,1));
    P[1,1,1]:=IJ(RealToVector(1,1,1));

    MoveTo(P[1,0,0].x,P[1,0,0].y); LineTo(P[1,0,1].x,P[1,0,1].y);
    MoveTo(P[0,1,0].x,P[0,1,0].y); LineTo(P[0,1,1].x,P[0,1,1].y);
    MoveTo(P[1,1,0].x,P[1,1,0].y); LineTo(P[1,1,1].x,P[1,1,1].y);
    MoveTo(P[1,0,1].x,P[1,0,1].y); LineTo(P[1,1,1].x,P[1,1,1].y);
    MoveTo(P[1,0,0].x,P[1,0,0].y); LineTo(P[1,1,0].x,P[1,1,0].y);
    MoveTo(P[0,1,0].x,P[0,1,0].y); LineTo(P[1,1,0].x,P[1,1,0].y);
    MoveTo(P[0,1,1].x,P[0,1,1].y); LineTo(P[1,1,1].x,P[1,1,1].y);
    MoveTo(P[0,0,1].x,P[0,0,1].y); LineTo(P[1,0,1].x,P[1,0,1].y);
    MoveTo(P[0,0,1].x,P[0,0,1].y); LineTo(P[0,1,1].x,P[0,1,1].y);
  end;
end;

procedure TGraph3D.Draw3D;
var
  i,j,L,q: integer;
  w: array[0..3] of TPoint;
begin
  with Bitmap.Canvas do begin
    Brush.Style:=bsSolid;
    Brush.Color:=clWhite;
    FillRect(Rect(0,0,Bitmap.Width,Bitmap.Height));

    if FtypeFace then begin
      if FVisibleAxes then DrawAxes;
      Pen.Color:=clBlack;
      Pen.Width:=1;
      Pen.Style:=psSolid;
      for i:=0 to n-2 do
      for j:=0 to m-2 do begin
        P0:=IJ(V[i,j]);
        P1:=IJ(V[i,j+1]);
        MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
        P1:=IJ(V[i+1,j]);
        MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
        if FTypeFaceTRIANGLES then begin
          P1:=IJ(V[i+1,j+1]);
          MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
        end;
      end;
      for j:=0 to m-2 do begin
        P0:=IJ(V[n-1,j]);
        P1:=IJ(V[n-1,j+1]);
        MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
      end;
      for i:=0 to n-2 do begin
        P0:=IJ(V[i,m-1]);
        P1:=IJ(V[i+1,m-1]);
        MoveTo(P0.X,P0.Y); LineTo(P1.X,P1.Y);
      end;
    end
    else begin
      L:=(n-1)*(m-1);
      SetLength(Sides,L);
      q:=-1;
      for j:=0 to m-2 do
      for i:=0 to n-2 do begin
        Inc(q);
        with Sides[q] do begin
          P[0]:=Rotate(V[i+0,j+0]);
          P[1]:=Rotate(V[i+1,j+0]);
          P[2]:=Rotate(V[i+1,j+1]);
          P[3]:=Rotate(V[i+0,j+1]);

          N:=Norm(P[0],P[1],P[2]);
          Zmin:=Z_Min(Sides[q]);
        end;
      end;
      Sort;

      for i:=0 to L-1 do
      with Sides[i] do begin
        for j:=0 to 3 do w[j]:=VectorToPoint(P[j]);
        Brush.Color:=Trunc(255*Abs(N))*$010100;
        Pen.Color:={ $FF0000; //}Brush.Color;
        Polygon(w)
      end;
    end;
    DrawCub;
  end;
end;

end.
