object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 577
  ClientWidth = 737
  Color = clBtnFace
  Constraints.MinHeight = 200
  Constraints.MinWidth = 190
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object RandomValues: TButton
    Left = 608
    Top = 433
    Width = 120
    Height = 33
    Caption = 'RandomValues'
    TabOrder = 0
    OnClick = RandomValuesClick
  end
  object StringGrid1: TStringGrid
    Left = 608
    Top = 48
    Width = 120
    Height = 379
    ColCount = 2
    DefaultColWidth = 50
    RowCount = 15
    FixedRows = 0
    ScrollBars = ssVertical
    TabOrder = 1
    RowHeights = (
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24)
  end
  object Edit1: TEdit
    Left = 608
    Top = 16
    Width = 120
    Height = 21
    TabOrder = 2
    Text = '10'
    OnKeyDown = Edit1KeyDown
  end
  object Button1: TButton
    Left = 608
    Top = 472
    Width = 121
    Height = 33
    Caption = 'Draw'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 480
    Top = 16
    Width = 120
    Height = 21
    TabOrder = 4
    Text = '50'
    OnKeyDown = Edit2KeyDown
  end
  object CheckBox1: TCheckBox
    Left = 480
    Top = 43
    Width = 89
    Height = 17
    Caption = 'Use user`s "j"'
    TabOrder = 5
  end
end
