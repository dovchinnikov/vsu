unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids;

const
  d=4;
  wmin=4;
type
  TForm1 = class(TForm)
    RandomValues: TButton;
    StringGrid1: TStringGrid;
    Edit1: TEdit;
    Button1: TButton;
    Edit2: TEdit;
    CheckBox1: TCheckBox;
    procedure Edit2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
 
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RandomValuesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  a,b: array of real;
  n: integer;
  k: real=1;
  max: real;
  flag: boolean;
  wm: integer;
implementation

{$R *.dfm}
procedure slijanie(var n,j: integer);
var
  kol,i: integer;
  step,ss: real;
begin
  step:=n/j;
  setlength(b,0);
  setlength(b,j);
  j:=0;
  i:=0;
  kol:=0;
  ss:=ss+step;

  repeat
    while i<ss do
    begin
      if i=n then break;
      b[j]:=b[j]+a[i];
      i:=i+1;
      kol:=kol+1;
    end;

    b[j]:=b[j]/kol;
    ss:=ss+step;
    j:=j+1;
    kol:=0;

  until i>=n;
  flag:=true;
//  b[j-1]:=b[j-1]*2;
//  form1.button2.caption:='';
end;

procedure findmax;
var
  i: integer;
  len: integer;
begin
  len:=length(a);
  for i := 0 to len-1 do
    if a[i]>max then max:=a[i];
end;

procedure draw(var a: array of real; wm: integer);
var
  c1: integer;
  i: integer;
  h: integer;       //������ ������� �������
  len: integer;
begin
  c1:=d div 2;
  len:=length(a);
  for i := 0 to len - 1 do
    begin
      h:= round(a[i]/max*(form1.Height-100)*k);
      form1.Canvas.Polyline([point(c1+wm,form1.Height -50),point(c1,form1.Height-50),point(c1,form1.Height-50-h),point(c1+wm,form1.Height-50-h),point(c1+wm,form1.Height-50),point(c1+wm+(wm div 2),form1.Height-50-(wm div 2)),point(c1+wm+(wm div 2),form1.Height-50-h-(wm div 2)),point(c1+(wm div 2),form1.Height-50-h-(wm div 2)),point(c1,form1.Height-50-h),point(c1+wm,form1.Height-50-h),point(c1+wm+(wm div 2),form1.Height-50-h-(wm div 2))]);
      form1.Canvas.FloodFill(c1+1,form1.Height -51,clblack,fsborder);
      c1:=c1+d+wm+(wm div 2);
    end;
end;

procedure draw0;
begin
  form1.Canvas.Brush.Color:=$f0f0f0;
  form1.Canvas.Rectangle(0,0,form1.Width,form1.Height);
  form1.Canvas.Brush.Color:=clblue;
end;

procedure Tform1.Button1Click(sender: TObject);
var
  j: integer;       //���������� �������� ��������
begin
  flag:=false;
  draw0;
  j:=(form1.width-140)div(d+wmin+(wmin div 2));

  if checkbox1.Checked  then
  if (strtoint(edit2.Text)<=j) and (strtoint(edit2.Text)<=n) and not(edit2.text='') then
  begin
    j:=strtoint(edit2.Text);
    wm:=round(((form1.Width-150)/j - d)/1.5);
 //   showmessage('����� ���������� '+inttostr(j)+'��������!');
    slijanie(n,j);
    draw(b,wm);
    exit;
  end
  else begin
    if j<n then showmessage('�������� ������ ���� � �������� �� 1 �� '+inttostr(j))
    else showmessage('�������� ������ ���� � �������� �� 1 �� '+inttostr(n));
    exit;
  end;



  if j<n then
  begin
    wm:=wmin;
    slijanie(n,j);
    draw(b,wm);
  end
  else begin
    wm:=round(((form1.Width-150)/n - d)/1.5);
    draw(a,wm);
  end;




end;




procedure TForm1.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key=13 then
    randomvalues.Click;

end;

procedure TForm1.Edit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=13 then
  button1.Click;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
begin
  for i := 0 to 14 do
    stringgrid1.Cells[0,i]:=inttostr(i+1)+'.';
  randomvalues.Click;

end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  form1.Caption:=inttostr(x)+'   '+inttostr(Y);
end;

procedure TForm1.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  k:=k-0.05;
  if k<=0  then k:=0.05;
  if flag then
  begin
    draw0;
    draw(b,wm)
  end
  else begin
    draw0;
    draw(a,wm);
  end;
end;

procedure TForm1.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin

  k:=k+0.05;
  if flag then
  begin
    draw0;
    draw(b,wm)
  end
  else begin
    draw0;
    draw(a,wm);
  end;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  edit1.Left:=form1.Width-140;
  stringgrid1.Left:=form1.Width-140;
  randomvalues.Left:=form1.Width-140;
  button1.Left:=form1.Width-140;
  edit2.left:=form1.Width-273;
  checkbox1.Left:=form1.Width-273;
  button1.click;
end;

procedure TForm1.RandomValuesClick(Sender: TObject);
var
  i,j: integer;
begin
  randomize;
  n:=strtoint(edit1.text);
  if n=0 then exit;
  setlength(a,n);
  stringgrid1.RowCount:=n;
  for i:= 0 to n-1 do
  begin
    a[i]:=random*100;
    stringgrid1.cells[1,i]:=floattostr(a[i]);
    stringgrid1.Cells[0,i]:=inttostr(i+1)+'.';
  end;
  findmax;
end;

end.
