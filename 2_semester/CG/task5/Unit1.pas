unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, math;

const
  x0=420;           // ����� ������
  y0=400;
  x1=320;           // �������
  y1=230;
  R0=100;
  R1=10;     // ������� ������
  R=50;             // ������ ������ ������
  xR1=250; yR1=400;
  xR2=600; yR2=400;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    TrackBar1: TTrackBar;
    procedure TrackBar1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Angle1  : real=0;
  Angle2  : real=Pi;
  Angle3  : real=0;
  Step    : real=0.1;
  L       : real;
implementation

{$R *.dfm}

procedure drawcycle;
begin
  with form1.Canvas do
  begin
    pen.Color:=clblack;
    pen.Width:=10;
    ellipse(150,300,350,500);
    ellipse(500,300,700,500);

    pen.Width:=3;
    ellipse(xr1-r1,yr1-r1,xr1+r1,yr1+r1);
    ellipse(xr2-r1,yr2-r1,xr2+r1,yr2+r1);
    ellipse(x0-r+20,y0-r+20,x0+r-20,y0+r-20);
    pen.Color:=clblue;
    pen.Width:=10;
    polyline([point(320,230),point(420,400),point(250,400),point(340,255),point(510,240),point(420,400)]);
    polyline([point(600,400),point(492,210)]);
    arc(395,190,495,240,530,290,450,190);

    pen.Width:=20;
    pen.Color:=clgray;

    polyline([point(400,75),point(415,170),point(455,190)]);
    polyline([point(320,230),point(400,75),point(400,180),point(473,233)]);

    brush.Color:=clgray;
    ellipse(375,trunc(125{+sin(angle1)}),405,trunc(145+5*sin(angle1*2)));
    brush.Color:=$f0f0f0;

    pen.Width:=10;
    ellipse(393,10,445,70);
    pen.Width:=3;
    pen.Color:=clwhite;
    polyline([point(400,75),point(415,170),point(455,190)]);
    polyline([point(320,230),point(400,75),point(400,180),point(473,233)]);
    pen.Color:=clgreen;
    pen.width:=10;
    moveto(0,505);
    lineto(form1.Width,505);
    application.ProcessMessages;
  end;

end;

procedure DrawFoot(angle: real; flag: boolean; back: boolean); // ����
var
  x,y,x2,y2,xc,yc,a,b: real;
  alf,gam: real;
begin
  with form1.Canvas do begin
    if flag then rectangle(-1,-1,form1.Width,form1.Height );
    x2:=x0+R*cos(Angle);
    y2:=y0+R*sin(Angle);
    xc:=(x1+x2)/2;
    yc:=(y1+y2)/2;
    b:=Sqrt(Sqr(x2-xc)+Sqr(y2-yc));
    a:=Sqrt(L*L-b*b);
    alf:=Arctan2(a,b);
    gam:=Arctan2(y1-y2,x1-x2);
    x:=x2+L*cos(gam+alf);
    y:=y2+L*sin(gam+alf);

    Pen.Width:=20;
    Pen.Color:=clGray;
    if back then
    begin
    moveto(x0,y0);
    pen.Width:=3;
    pen.Color:=clblack;
    lineTo(Round(x2),Round(y2));
    pen.Width:=20;
    pen.Color:=clgray;
    end
    else MoveTo(Round(x2),Round(y2));
    LineTo(Round(x),Round(y));
    LineTo(Round(x1),Round(y1));

    Pen.Width:=3;
    Pen.Color:=clWhite;

    MoveTo(Round(x1),Round(y1));
    LineTo(Round(x),Round(y));
    LineTo(Round(x2),Round(y2));
    if not(back) then begin
    pen.Color:=clblack;
    lineto(x0,y0);

    end;
  application.ProcessMessages;
  end;
end;

procedure DrawWheel; // ������
const n=24;
var
  x1,y1,x2,y2,a: real;
  i: integer;
begin
  with form1.Canvas do begin
    Pen.Width:=1;
    Pen.Color:=clGray;
    for i:=0 to n-1 do begin
      a:=Angle3+2*Pi/n*i;
      x1:=xR1+R0*cos(a);
      y1:=yR1-R0*sin(a);
      x2:=xR1+R1*cos(a);
      y2:=yR1-R1*sin(a);
      MoveTo(Trunc(x2),Trunc(y2));
      LineTo(Trunc(x1),Trunc(y1));
      x1:=x1-xr1+xr2;
      y1:=y1-yr1+yr2;
      x2:=x2-xr1+xr2;
      y2:=y2-yr2+yr2;
      MoveTo(Trunc(x2),Trunc(y2));
      LineTo(Trunc(x1),Trunc(y1));
    end;
  end;
  application.ProcessMessages;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   L:=(Sqrt(Sqr(x0-x1)+Sqr(y0-y1))+R)/2;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  form1.Caption:=inttostr(x)+'  '+inttostr(y);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
 
  Angle1:=Angle1+Step;
  Angle2:=Angle2+Step;
  Angle3:=Angle3-Step*2/3;
  drawfoot(angle1,true,false);
  application.ProcessMessages;
  drawcycle;
  application.ProcessMessages;
  drawwheel;
  application.ProcessMessages;
  drawfoot(angle2,false,true);
  application.ProcessMessages;



end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  if trackbar1.Position=trackbar1.Min  then timer1.Enabled:=false
  else begin
    Step:=TrackBar1.Position/2000;
    timer1.Enabled:=true;
  end;
end;

end.
