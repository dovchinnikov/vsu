unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls,lib;

type
  TForm3 = class(TForm)
    TrackBar1: TTrackBar;
    TrackBar2: TTrackBar;
    TrackBar3: TTrackBar;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar2Change(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure TrackBar3Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;

implementation

{$R *.dfm}

procedure TForm3.FormCreate(Sender: TObject);
begin
  label1.Caption:='x = '+floattostr(x0);
  label2.Caption:='y = '+floattostr(y0);
  label3.Caption:='z = '+floattostr(z0);
end;

procedure TForm3.TrackBar1Change(Sender: TObject);
begin
  x0:=trackbar1.Position-100;
  label1.Caption:='x = '+floattostr(x0);
end;

procedure TForm3.TrackBar2Change(Sender: TObject);
begin
  y0:=trackbar2.Position+200;
  label2.Caption:='y = '+floattostr(y0);
end;

procedure TForm3.TrackBar3Change(Sender: TObject);
begin
  z0:=trackbar3.Position-100;
  label3.Caption:='z = '+floattostr(z0);
end;

end.
