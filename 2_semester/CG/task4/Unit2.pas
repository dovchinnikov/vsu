unit Unit2;

interface
uses lib,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ComCtrls, ExtCtrls;

  procedure showtelo(telo: array of tvertex; first: boolean; w: integer);
  procedure set2dim(var telo: array of tvertex; al,be,ga: real);
  procedure docube;
  procedure dotetra;
  procedure doocta;
  procedure doother;
  procedure doshadow;
  procedure doothershadow;
  procedure showkletki;

var
  a,alf,bet,gam: real;
  mk: integer;
  pt: array of tpoint;
implementation
uses
  unit1;


procedure showtelo(telo: array of tvertex;first: boolean; w: integer);
var
  k,m,l,ll: Integer;

begin
  l:=length(telo);
  with form1.Canvas do begin
    pen.Width:=w;
    Brush.Color := $f0f0f0;
    if first then rectangle(-1, -1, form1.Width , form1.Height );  // ������� �����
    for k := 0 to l-1 do                // ��������� ���
    with telo[k] do begin
      LL := Length(Edges);
      for m := 0 to LL - 1 do
      with Edges[m] do begin
        moveto(i,j);
        lineto(telo[nom].i,telo[nom].j);
      end;
    end;
  end;

end;

procedure set2dim(var telo: array of tvertex; al,be,ga: real);
var
  l: integer;
  i: integer;
  xt,yt,zt: real;
 // xx,zz: real;
begin
  l:=length(telo);
  //pt:=nil;
  setlength(pt,l);
  for i := 0 to l - 1 do begin
    with telo[i] do begin

      xt:=x*cos(ga)*cos(be)+y*(cos(ga)*sin(al)*sin(be)-sin(ga)*cos(al))+z*(cos(ga)*cos(al)*sin(be)+sin(al)*sin(ga));
      yt:=x*sin(ga)*cos(be)+y*(sin(ga)*sin(be)*sin(al)+cos(al)*cos(ga))+z*(sin(ga)*cos(al)*sin(be)-sin(al)*cos(ga));
      zt:=x*sin(-be)+y*(sin(al)*cos(be))+z*(cos(al)*cos(be))+100*a;
      {
      if form1.Checkshadow.Checked then
      begin
        xx:=(yy-y0)*(xt-x0)/(yt-y0)+x0;
        zz:=(yy-y0)*(zt-z0)/(yt-y0)+z0-100*a;
      end;
      }
      if not(form1.checkmk.checked) then
      begin
        i:=round(mk*xt/(zt+mk))+Wid div 2;
        j:=-round(mk*yt/(zt+mk))+Hei div 2;

      end
      else begin
        i:=round(xt)+Wid div 2;
        j:=-round(yt)+Hei div 2;
      end;
    end;
    {
    if form1.Checkshadow.Checked then
    begin
      pt[i].X:=round(mk*xx/(zz+mk))+wid div 2;
      pt[i].y:=-round(mk*yy/(zz+mk))+hei div 2;
    end;
    }
  end;
end;

procedure docube;
begin
  set2dim(cube,alf,bet,gam);
  showtelo(cube,flag,1);
  flag:=false;
end;

procedure dotetra;
begin
  set2dim(tetra,alf,bet,gam);
  showtelo(tetra,flag,1);
  flag:=false;
end;

procedure doocta;
begin
  set2dim(octaedr,alf,bet,gam);
  showtelo(octaedr,flag,1);
  flag:=false;
end;

procedure doother;
begin
  set2dim(other,alf,bet,gam);
  showtelo(other,flag,1);
  flag:=false;
end;

procedure doshadow;
begin
  form1.Canvas.Polygon(pt);
end;

procedure doothershadow;
var i,l,j,ll: integer;
xx,zz: real;
xt,zt,yt: real;
ppt: array of tpoint;
begin
  l:=length(sides);
  form1.Canvas.Brush.Style:=bssolid;
  form1.Canvas.Brush.Color:=clgray;
  form1.Canvas.Pen.Color:=clgray;
  for i := 0 to l - 1 do
  begin
    ll:=length(sides[i]);
    setlength(ppt,ll);
    for j:=0 to ll-1 do begin
      with points[sides[i,j]] do begin
      xt:=x*cos(gam)*cos(bet)+y*(cos(gam)*sin(alf)*sin(bet)-sin(gam)*cos(alf))+z*(cos(gam)*cos(alf)*sin(bet)+sin(alf)*sin(gam));
      yt:=x*sin(gam)*cos(bet)+y*(sin(gam)*sin(bet)*sin(alf)+cos(alf)*cos(gam))+z*(sin(gam)*cos(alf)*sin(bet)-sin(alf)*cos(gam));
      zt:=x*sin(-bet)+y*(sin(alf)*cos(bet))+z*(cos(alf)*cos(bet))+100*a;
      end;
      xx:=(yy-y0)*(xt-x0)/(yt-y0)+x0;
      zz:=(yy-y0)*(zt-z0)/(yt-y0)+z0-100*a;

      ppt[j].X:=round(mk*xx/(zz+mk))+wid div 2;
      ppt[j].y:=-round(mk*yy/(zz+mk))+hei div 2;
    end;
    form1.Canvas.Polygon(ppt);
    ppt:=nil;
  end;

  form1.Canvas.Pen.Color:=clblack;
end;

procedure showkletki;
var x1,z1,x2,z2: integer;
i1,j1,i2,j2: integer;
begin
  x1:=-500;
  z1:=-500;
  x2:=500;
  z2:=1000;
  while x1<=x2 do begin
    i1:=round(mk*x1/(z1+100*a+mk))+wid div 2;
    j1:=-round(mk*yy/(z1+100*a+mk))+hei div 2;
    i2:=round(mk*x1/(z2+100*a+mk))+wid div 2;
    j2:=-round(mk*yy/(z2+100*a+mk))+hei div 2;
    form1.Canvas.MoveTo(i1,j1);
    form1.Canvas.lineTo(i2,j2);
    x1:=x1+100;
  end;
  x1:=-500;
  while z1<=z2 do begin
    i1:=round(mk*x1/(z1+100*a+mk))+wid div 2;
    j1:=-round(mk*yy/(z1+100*a+mk))+hei div 2;
    i2:=round(mk*x2/(z1+100*a+mk))+wid div 2;
    j2:=-round(mk*yy/(z1+100*a+mk))+hei div 2;
    form1.Canvas.MoveTo(i1,j1);
    form1.Canvas.lineTo(i2,j2);
    z1:=z1+100;
  end;
end;

end.
