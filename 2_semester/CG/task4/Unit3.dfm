object Form3: TForm3
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = #1048#1089#1090#1086#1095#1085#1080#1082' '#1089#1074#1077#1090#1072
  ClientHeight = 123
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 73
    Height = 13
    AutoSize = False
  end
  object Label2: TLabel
    Left = 16
    Top = 39
    Width = 65
    Height = 13
    AutoSize = False
  end
  object Label3: TLabel
    Left = 8
    Top = 67
    Width = 73
    Height = 13
    AutoSize = False
  end
  object TrackBar1: TTrackBar
    Left = 104
    Top = 8
    Width = 265
    Height = 25
    Max = 200
    Frequency = 10
    ShowSelRange = False
    TabOrder = 0
    ThumbLength = 15
    OnChange = TrackBar1Change
  end
  object TrackBar2: TTrackBar
    Left = 104
    Top = 39
    Width = 265
    Height = 22
    Max = 200
    Frequency = 10
    Position = 100
    ShowSelRange = False
    TabOrder = 1
    ThumbLength = 15
    OnChange = TrackBar2Change
  end
  object TrackBar3: TTrackBar
    Left = 104
    Top = 67
    Width = 265
    Height = 27
    Max = 200
    Frequency = 10
    ShowSelRange = False
    TabOrder = 2
    ThumbLength = 15
    OnChange = TrackBar3Change
  end
end
