unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, unit2,unit3, Menus, ExtCtrls, ComCtrls,lib;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    showcube: TCheckBox;
    showpiramidka: TCheckBox;
    showoctaedr: TCheckBox;
    CheckOther: TCheckBox;
    Checkx: TCheckBox;
    Checky: TCheckBox;
    Checkz: TCheckBox;
    TrackBar1: TTrackBar;
    checkmk: TCheckBox;
    speed: TTrackBar;
    showosi: TCheckBox;
    rotationosi: TCheckBox;
    Checkshadow: TCheckBox;
    checkkl: TCheckBox;
    PopupMenu1: TPopupMenu;
    Play1: TMenuItem;
    Pause1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    procedure N4Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Pause1Click(Sender: TObject);
    procedure Play1Click(Sender: TObject);
    procedure CheckshadowClick(Sender: TObject);
    procedure showosiClick(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure TrackBar1Change(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
var
  Form1: TForm1;
  flag: boolean;

implementation

{$R *.dfm}


procedure TForm1.CheckshadowClick(Sender: TObject);
begin
  checkkl.Checked:=true;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  a:=5;
  mk:=trackbar1.Position;
  alf:=0;
  bet:=0;
  gam:=0;
  x0:=0;
  y0:=300;
  z0:=0;
  setcube;
  setosi;
  set2dim(osi,alf,bet,gam);
  setoctaedr;
  setpiramidka;
  setmybody;
  form1.DoubleBuffered:=true;
end;


procedure TForm1.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin

  a:=a+0.2;
  if a>10 then
  a:=10;
end;

procedure TForm1.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  a:=a-0.2;
  if a<=-2 then
  a:=-2;
end;

procedure TForm1.N2Click(Sender: TObject);
begin
  form3.Show;
end;

procedure TForm1.N4Click(Sender: TObject);
begin
  form1.Close;
end;

procedure TForm1.Pause1Click(Sender: TObject);
begin
  timer1.Enabled:=false;
 // checkx.Checked:=false;
 // checky.Checked:=false;
 // checkz.Checked:=false;
  alf:=0;
  bet:=0;
  gam:=0;
  form1.Timer1timer(sender);
end;

procedure TForm1.Play1Click(Sender: TObject);
begin
  if timer1.Enabled  then
  begin
    timer1.Enabled:=false;
  end
  else timer1.Enabled:=true;
end;

procedure TForm1.showosiClick(Sender: TObject);
begin
  rotationosi.Checked:=true;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin

  if checkx.checked then alf:=alf+0.001*speed.Position;
  if checky.checked then bet:=bet+0.001*speed.Position;
  if checkz.checked then gam:=gam+0.001*speed.Position;

  flag:=true;
  if showosi.Checked  then
  begin
    if rotationosi.Checked  then set2dim(osi,alf,bet,gam);
    showtelo(osi,flag,2);
    flag:=false;
  end;

  pt:=nil;

  if checkother.Checked  then doother;
  if form1.showcube.Checked then docube;
  if form1.showpiramidka.Checked then dotetra;
  if form1.showoctaedr.Checked then doocta;
  if form1.Checkshadow.Checked then
 // doshadow;
  doothershadow;
  if checkkl.Checked then showkletki;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  mk:=trackbar1.Position;
end;

end.
