unit lib;

interface

const
  wid=1025;
  hei=512;
  yy=-300;
type
  tEdge = record
    nom: integer;
    visible: boolean;
  end;
  tVertex = record
    x,y,z: real;
    //xt,yt,zt: real;
    edges: array of tedge;
    i,j: integer;
  end;
  tpoints = record
    x,y,z: real;
  end;
  tside = record
    num: array of word;
  end;
  txy = record
    x,y: integer;
  end;
var
  cube: array of tvertex;
  osi: array of tvertex;
  tetra: array of tvertex;
  octaedr: array of tvertex;
  other: array of tvertex;
  points: array of tpoints;
  sides: array of array of integer;
  x0,y0,z0: real;
  procedure setcube;
  procedure setosi;
  procedure setpiramidka;
  procedure setoctaedr;
  procedure setmybody;
  procedure setbodyfromsides;

implementation

uses unit2,unit1;


procedure setcube;
var i: integer;
begin
  setlength(cube,8);
  for i := 0 to 7 do
    setlength(cube[i].edges,3);

  cube[0].x:=100;
  cube[0].y:=100;
  cube[0].z:=100;

  cube[1].x:=-100;
  cube[1].y:=100;
  cube[1].z:=100;

  cube[2].x:=100;
  cube[2].y:=-100;
  cube[2].z:=100;

  cube[3].x:=100;
  cube[3].y:=100;
  cube[3].z:=-100;

  cube[4].x:=-100;
  cube[4].y:=-100;
  cube[4].z:=100;

  cube[5].x:=100;
  cube[5].y:=-100;
  cube[5].z:=-100;

  cube[6].x:=-100;
  cube[6].y:=100;
  cube[6].z:=-100;

  cube[7].x:=-100;
  cube[7].y:=-100;
  cube[7].z:=-100;

  cube[0].edges[0].nom:=1;
  cube[0].edges[1].nom:=2;
  cube[0].edges[2].nom:=3;

  cube[1].edges[0].nom:=0;
  cube[1].edges[1].nom:=4;
  cube[1].edges[2].nom:=6;

  cube[2].edges[0].nom:=0;
  cube[2].edges[1].nom:=4;
  cube[2].edges[2].nom:=5;

  cube[3].edges[0].nom:=0;
  cube[3].edges[1].nom:=5;
  cube[3].edges[2].nom:=6;

  cube[4].edges[0].nom:=1;
  cube[4].edges[1].nom:=2;
  cube[4].edges[2].nom:=7;

  cube[5].edges[0].nom:=2;
  cube[5].edges[1].nom:=3;
  cube[5].edges[2].nom:=7;

  cube[6].edges[0].nom:=1;
  cube[6].edges[1].nom:=3;
  cube[6].edges[2].nom:=7;

  cube[7].edges[0].nom:=4;
  cube[7].edges[1].nom:=5;
  cube[7].edges[2].nom:=6;
end;

procedure setosi;
begin
setlength(osi,4);
  osi[0].x:=0;
  osi[0].y:=0;
  osi[0].z:=0;

  osi[1].x:=200;
  osi[1].y:=0;
  osi[1].z:=0;

  osi[2].x:=0;
  osi[2].y:=200;
  osi[2].z:=0;

  osi[3].x:=0;
  osi[3].y:=0;
  osi[3].z:=200;

  setlength(osi[0].edges,3);
   osi[0].edges[0].nom :=1;
   osi[0].edges[1].nom :=2;
   osi[0].edges[2].nom :=3;
end;

procedure setpiramidka;
var i: integer;
begin
  setlength(tetra,4);
  tetra[0].x:=0;
  tetra[0].y:=100;
  tetra[0].z:=0;

  tetra[1].x:=87;
  tetra[1].y:=-50;
  tetra[1].z:=50;

  tetra[2].x:=-87;
  tetra[2].y:=-50;
  tetra[2].z:=50;

  tetra[3].x:=0;
  tetra[3].y:=-50;
  tetra[3].z:=-100;

  for i := 0 to length(tetra) - 1 do
    setlength(tetra[i].edges,3);
  tetra[0].edges[0].nom:=1;
  tetra[0].edges[1].nom:=2;
  tetra[0].edges[2].nom:=3;

  tetra[1].edges[0].nom:=0;
  tetra[1].edges[1].nom:=1;
  tetra[1].edges[2].nom:=2;

  tetra[2].edges[0].nom:=0;
  tetra[2].edges[1].nom:=1;
  tetra[2].edges[2].nom:=3;

  tetra[3].edges[0].nom:=0;
  tetra[3].edges[1].nom:=1;
  tetra[3].edges[2].nom:=2;
end;

procedure setoctaedr;
var i: integer;
begin
  setlength(octaedr,6);
  octaedr[0].x:=0;
  octaedr[0].y:=100;
  octaedr[0].z:=0;

  octaedr[1].x:=100;
  octaedr[1].y:=0;
  octaedr[1].z:=0;

  octaedr[2].x:=0;
  octaedr[2].y:=0;
  octaedr[2].z:=100;

  octaedr[3].x:=-100;
  octaedr[3].y:=0;
  octaedr[3].z:=0;

  octaedr[4].x:=0;
  octaedr[4].y:=0;
  octaedr[4].z:=-100;

  octaedr[5].x:=0;
  octaedr[5].y:=-100;
  octaedr[5].z:=0;

  for i := 0 to length(octaedr) - 1 do
    setlength(octaedr[i].edges,4);

  octaedr[0].edges[0].nom:=1;
  octaedr[0].edges[1].nom:=2;
  octaedr[0].edges[2].nom:=3;
  octaedr[0].edges[3].nom:=4;

  octaedr[1].edges[0].nom:=0;
  octaedr[1].edges[1].nom:=2;
  octaedr[1].edges[2].nom:=4;
  octaedr[1].edges[3].nom:=5;

  octaedr[2].edges[0].nom:=0;
  octaedr[2].edges[1].nom:=1;
  octaedr[2].edges[2].nom:=3;
  octaedr[2].edges[3].nom:=5;

  octaedr[3].edges[0].nom:=0;
  octaedr[3].edges[1].nom:=2;
  octaedr[3].edges[2].nom:=4;
  octaedr[3].edges[3].nom:=5;

  octaedr[4].edges[0].nom:=0;
  octaedr[4].edges[1].nom:=1;
  octaedr[4].edges[2].nom:=3;
  octaedr[4].edges[3].nom:=5;

  octaedr[5].edges[0].nom:=1;
  octaedr[5].edges[1].nom:=2;
  octaedr[5].edges[2].nom:=3;
  octaedr[5].edges[3].nom:=4;

end;

procedure setmybody;
begin
  setlength(points,4);
  points[0].x:=100;
  points[0].y:=45;
  points[0].z:=66;

  points[1].x:=-100;
  points[1].y:=45;
  points[1].z:=66;

  points[2].x:=100;
  points[2].y:=-88;
  points[2].z:=79;

  points[3].x:=-40;
  points[3].y:=45;
  points[3].z:=-50;

  setlength(sides,4);
  setlength(sides[0],3);
  sides[0,0]:=0;
  sides[0,1]:=1;
  sides[0,2]:=2;

  setlength(sides[1],3);
  sides[1,0]:=1;
  sides[1,1]:=2;
  sides[1,2]:=3;

  setlength(sides[2],3);
  sides[2,0]:=0;
  sides[2,1]:=1;
  sides[2,2]:=3;

  setlength(sides[3],3);
  sides[3,0]:=0;
  sides[3,1]:=2;
  sides[3,2]:=3;
  setbodyfromsides;
end;

procedure addedge(var telo: array of tvertex; fr,too: integer);
var ll:integer;
begin
  ll:=length(telo[fr].edges);
  setlength(telo[fr].edges,ll+1);
  telo[fr].edges[ll].nom:=too;

  ll:=length(telo[too].edges);
  setlength(telo[too].edges,ll+1);
  telo[too].edges[ll].nom:=fr;
end;

procedure setbodyfromsides;
var i,j,l,ll,li,lj,ii,jj: integer;
t: array of integer;
begin
  l:=length(points);
  setlength(other,l);
  for i:=0 to l-1 do begin
    other[i].x:=points[i].x;
    other[i].y:=points[i].y;
    other[i].z:=points[i].z;
  end;
  l:=length(sides);
  for i := 0 to L - 1 do
    for j := i+1 to l - 1 do
      if j<>i then begin
        li:=length(sides[i]);
        lj:=length(sides[j]);
        t:=nil;
        for ii:=0 to li-1 do
          for jj := 0 to lj - 1 do
            if sides[i,ii]=sides[j,jj] then
            begin
              setlength(t,length(t)+1);
              t[length(t)-1]:=sides[i,ii];
            end;
        if length(t)=2 then
        addedge(other,t[0],t[1]);
      end;
 // points:=nil;
end;

end.
