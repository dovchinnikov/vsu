object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Form1'
  ClientHeight = 512
  ClientWidth = 1024
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  Visible = True
  OnCreate = FormCreate
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  PixelsPerInch = 96
  TextHeight = 13
  object showcube: TCheckBox
    Left = 728
    Top = 32
    Width = 89
    Height = 17
    Caption = #1050#1091#1073
    TabOrder = 0
  end
  object showpiramidka: TCheckBox
    Left = 728
    Top = 55
    Width = 89
    Height = 16
    Caption = #1055#1080#1088#1072#1084#1080#1076#1082#1072
    TabOrder = 1
  end
  object showoctaedr: TCheckBox
    Left = 728
    Top = 77
    Width = 89
    Height = 21
    Caption = #1054#1082#1090#1072#1101#1076#1088
    TabOrder = 2
  end
  object CheckOther: TCheckBox
    Left = 728
    Top = 104
    Width = 89
    Height = 17
    Caption = #1052#1086#1077' '#1090#1077#1083#1086
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object Checkx: TCheckBox
    Left = 704
    Top = 463
    Width = 41
    Height = 17
    Caption = 'x'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object Checky: TCheckBox
    Left = 704
    Top = 478
    Width = 41
    Height = 17
    Caption = 'y'
    TabOrder = 5
  end
  object Checkz: TCheckBox
    Left = 704
    Top = 493
    Width = 41
    Height = 17
    Caption = 'z'
    TabOrder = 6
  end
  object TrackBar1: TTrackBar
    Left = 8
    Top = 8
    Width = 521
    Height = 33
    LineSize = 4
    Max = 1500
    Min = 300
    Frequency = 25
    Position = 500
    TabOrder = 7
    ThumbLength = 15
    OnChange = TrackBar1Change
  end
  object checkmk: TCheckBox
    Left = 535
    Top = 8
    Width = 179
    Height = 17
    Caption = #1058#1086#1095#1082#1072' '#1089#1093#1086#1076#1072' '#26' '#1073#1077#1089#1082#1086#1085#1077#1095#1085#1086#1089#1090#1100
    TabOrder = 8
  end
  object speed: TTrackBar
    Left = 738
    Top = 463
    Width = 283
    Height = 32
    Max = 150
    Frequency = 5
    Position = 40
    TabOrder = 9
    ThumbLength = 15
    TickMarks = tmBoth
  end
  object showosi: TCheckBox
    Left = 728
    Top = 9
    Width = 89
    Height = 17
    Caption = #1054#1089#1080
    TabOrder = 10
    Visible = False
    OnClick = showosiClick
  end
  object rotationosi: TCheckBox
    Left = 823
    Top = 9
    Width = 97
    Height = 17
    Caption = #1042#1088#1072#1097#1072#1090#1100
    TabOrder = 11
    Visible = False
  end
  object Checkshadow: TCheckBox
    Left = 823
    Top = 104
    Width = 97
    Height = 17
    Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1090#1077#1085#1100
    TabOrder = 12
    OnClick = CheckshadowClick
  end
  object checkkl: TCheckBox
    Left = 919
    Top = 104
    Width = 97
    Height = 17
    Caption = #1055#1083#1086#1089#1082#1086#1089#1090#1100
    TabOrder = 13
  end
  object Timer1: TTimer
    Interval = 50
    OnTimer = Timer1Timer
    Left = 824
    Top = 32
  end
  object PopupMenu1: TPopupMenu
    Left = 416
    Top = 288
    object Play1: TMenuItem
      Caption = 'Play/pause'
      OnClick = Play1Click
    end
    object Pause1: TMenuItem
      Caption = 'Stop'
      OnClick = Pause1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1050#1086#1086#1088#1076#1080#1085#1072#1090#1099' '#1080#1089#1090#1086#1095#1085#1080#1082#1072' '#1089#1074#1077#1090#1072
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #1042#1099#1093#1086#1076
      OnClick = N4Click
    end
  end
end
