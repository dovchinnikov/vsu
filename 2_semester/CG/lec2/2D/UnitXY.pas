unit UnitXY;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, ExtCtrls, Buttons, Lbr;

const
  coeff = 1.1; // ����������� Zoom

type
  TFormMain = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
  private
    { Private declarations }
    I1,I2,J1,J2: integer;
    x0,y0: integer;
    drawing: boolean;
    function II(x: real): integer;
    function JJ(y: real): integer;
    function XX(i: integer): real;
    function YY(j: integer): real;
    procedure OX(fl: byte; x1,x2: real);
    procedure OY(fl: byte; y1,y2: real);
    procedure OYLn(fl: byte; y1,y2: real);
  public
    { Public declarations }
    procedure DrawGraphic;
  end;

var
  FormMain: TFormMain;

implementation

uses UnitTools;

{$R *.DFM}

function TFormMain.II(x: real): integer;
// ������� ��������������� �� ��� OX
begin
  II:=I1+Trunc((x-X1)*(I2-I1)/(X2-X1));
end;

function TFormMain.XX(i: integer): real;
// ������� ��������������� �� ��� OX
begin
  Result:=x1+(I-I1)*(x2-x1)/(I2-I1); //I1+Trunc((x-X1)*(I2-I1)/(X2-X1));
end;

function TFormMain.YY(j: integer): real;
// ������� ��������������� �� ��� OX
begin
  Result:=y1+(J-J2)*(y2-y1)/(J1-J2); //I1+Trunc((x-X1)*(I2-I1)/(X2-X1));
end;

function TFormMain.JJ(y: real): integer;
// ������� ��������������� �� ��� OY
var v: real;
begin
  if not FormTools.ChLnY.Checked then v:=Y2 else v:=Ln(Y2);
  JJ:=J2+Trunc((y-Y1)*(J1-J2)/(v-Y1));
end;

procedure TFormMain.OX(fl: byte; x1,x2: real);
// ��� OX
var i,j,k1,k2: integer;
    Digits   : byte;
    h1       : double;
begin
  h1:=HH(x1,x2); k1:=Trunc(x1/h1)-1; k2:=Trunc(x2/h1);
  Digits:=GetDigits(Abs(x2-x1));

  with Canvas do begin
    Pen.Color:=clBlack;
    Pen.Width:=1;
    Pen.Style:=psSolid;
    MoveTo(II(x1),JJ(0)); LineTo(II(x2),JJ(0));
    Font.Name:='Times New Roman';
    Font.Size:=7;
    for i:=k1 to k2 do begin
      Pen.Color:=clBlack;
      Pen.Style:=psSolid;
      MoveTo(II(i*h1),JJ(0)-7); LineTo(II(i*h1),JJ(0)+7);
      for j:=1 to 9 do begin
        MoveTo(II(i*h1+j*h1/10),JJ(0)-3);
        LineTo(II(i*h1+j*h1/10),JJ(0)+3);
      end;
      if fl=1 then begin
        Pen.Style:=psDot;
        Pen.Color:=clSilver;
        MoveTo(II(i*h1),J1); LineTo(II(i*h1),J2);
      end;
      TextOut(II(i*h1)-5,JJ(0)-c2-10,FloatToStrF(h1*i,ffFixed,8,Digits));
    end;
  end;
end;

procedure TFormMain.OY(fl: byte; y1,y2: real);
// ��� OY
var i,j,k1,k2: integer;
    Digits   : byte;
    h1       : double;
begin
  h1:=HH(y1,y2); k1:=Trunc(y1/h1)-1; k2:=Trunc(y2/h1);
  Digits:=GetDigits(Abs(y2-y1));

  with Canvas do begin
    Pen.Width:=1;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    MoveTo(II(0),JJ(y1)); LineTo(II(0),JJ(y2));
    Font.Name:='Times New Roman';
    Font.Size:=7;
    for i:=k1 to k2 do begin
      Pen.Color:=clBlack;
      Pen.Style:=psSolid;
      MoveTo(II(0)-7,JJ(i*h1)); LineTo(II(0)+7,JJ(i*h1));
      for j:=1 to 9 do begin
        MoveTo(II(0)-3,JJ(i*h1+j*h1/10));
        LineTo(II(0)+3,JJ(i*h1+j*h1/10));
      end;
      if (fl=1) and (i<>0) then begin
         Pen.Color:=clSilver;
         Pen.Style:=psDot;
         MoveTo(I1,JJ(i*h1)); LineTo(I2,JJ(i*h1));
      end;
      TextOut(II(0)+5,JJ(i*h1)-5,FloatToStrF(h1*i,ffFixed,8,Digits));
    end;
  end;
end;

procedure TFormMain.OYLn(fl: byte; y1,y2: real);
// ��� OY Ln
var i,j,k1,k2: integer;
    Digits   : byte;
    h1       : double;
begin
  h1:=HH(0,y2);
  k1:=Trunc(y1/h1)-1; k2:=Trunc(y2/h1);
  Digits:=GetDigits(Abs(y2-y1));

  with Canvas do begin
    Pen.Width:=1;
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    MoveTo(II(0),JJ(y1)); LineTo(II(0),JJ(Ln(y2)));
    Font.Name:='Times New Roman';
    Font.Size:=7;
    for i:=0 to k2 do begin
      Pen.Color:=clBlack;
      Pen.Style:=psSolid;
      if i<>0 then begin
        MoveTo(II(0)-7,JJ(Ln(i*h1))); LineTo(II(0)+7,JJ(Ln(i*h1)));
        TextOut(II(0)+5,JJ(Ln(i*h1))-5,FloatToStrF(h1*i,ffFixed,8,Digits));
      end;
      for j:=1 to 9 do begin
        MoveTo(II(0)-3,JJ(Ln(i*h1+j*h1/10)));
        LineTo(II(0)+3,JJ(Ln(i*h1+j*h1/10)));
      end;
      if (fl=1) and (i*h1>0) and (Ln(i*h1)<>0) then begin
        Pen.Color:=clSilver;
        Pen.Style:=psDot;
        if i<>0 then begin
          MoveTo(I1,JJ(Ln(i*h1))); LineTo(I2,JJ(Ln(i*h1)));
        end;
      end;
    end;
  end;
end;

procedure TFormMain.DrawGraphic;
var i: integer;
    k: byte;
    h,x,y: real;
begin
  // ���������� ���� �� ��� OX
  h:=(x2-x1)/n;
  with Canvas do begin
    RectAngle(-1,-1,Width,Height);
    // ���������� ���� ���������
    k:=FormTools.RadioGroup1.ItemIndex;
    OX(k,x1,x2);
    if not FormTools.ChLnY.Checked then OY(k,y1,y2) else OYLn(k,y1,y2);
    // ���������� ������� ������� ���������
    x:=x1; y:=Func(x,FormTools.ChLnY.Checked);
    MoveTo(II(x),JJ(y));
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    for i:=1 to n do begin
      x:=x+h; y:=Func(x,FormTools.ChLnY.Checked);
      LineTo(II(x),JJ(y));
    end;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  // ������� ���� �� ������
  I1:=0; J1:=0; I2:=ClientWidth; J2:=ClientHeight;
end;

procedure TFormMain.FormPaint(Sender: TObject);
begin
  DrawGraphic;
end;

procedure TFormMain.FormResize(Sender: TObject);
begin
  I2:=ClientWidth; J2:=ClientHeight;
  DrawGraphic;
end;

procedure TFormMain.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=true;
  x0:=x; y0:=y;
  Cursor:=crHandPoint;
end;

procedure TFormMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  dx,dy: real;
begin
  if drawing then
       if (Abs(x-x0)>5) or (Abs(y-y0)>5) then begin
         dx:=XX(x)-XX(x0); dy:=YY(y)-YY(y0);
         x0:=x; y0:=y;
         x1:=x1-dx; y1:=y1-dy; x2:=x2-dx; y2:=y2-dy;
         DrawGraphic;
         FormTools.SetEdit;
       end;
end;

procedure TFormMain.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=false;
  Cursor:=crDefault;
end;

procedure TFormMain.FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
var
  x,y: real;
begin
  x:=XX(MousePos.X); y:=YY(MousePos.Y);
  x1:=x-(x-x1)*coeff;
  x2:=x+(x2-x)*coeff;
  y1:=y-(y-y1)*coeff;
  y2:=y+(y2-y)*coeff;
  DrawGraphic;
  FormTools.SetEdit;
end;

procedure TFormMain.FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
var
  x,y: real;
begin
  x:=XX(MousePos.X); y:=YY(MousePos.Y);
  x1:=x-(x-x1)/coeff;
  x2:=x+(x2-x)/coeff;
  y1:=y-(y-y1)/coeff;
  y2:=y+(y2-y)/coeff;
  DrawGraphic;
  FormTools.SetEdit;
end;

end.
