unit UnitTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Lbr, Buttons;

type
  TFormTools = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Edit3: TEdit;
    Label4: TLabel;
    Edit4: TEdit;
    RadioGroup1: TRadioGroup;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    ChLnY: TCheckBox;
    procedure RadioGroup1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure ChLnYClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetEdit;
  end;

var
  FormTools: TFormTools;

implementation

{$R *.dfm}

uses UnitXY;

procedure TFormTools.RadioGroup1Click(Sender: TObject);
begin
  FormMain.DrawGraphic;
end;

procedure TFormTools.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13: begin
          Val(Edit1.Text,R,Code);
          if Code=0 then begin
            x1:=R; FormMain.DrawGraphic;
          end;
        end;
  end;
end;

procedure TFormTools.Edit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13: begin
          Val(Edit2.Text,R,Code);
          if Code=0 then begin
            x2:=R; FormMain.DrawGraphic;
          end;
        end;
  end;
end;

procedure TFormTools.Edit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13: begin
          Val(Edit3.Text,R,Code);
          if Code=0 then begin
            y1:=R; FormMain.DrawGraphic;
          end;
        end;
  end;
end;

procedure TFormTools.Edit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13: begin
          Val(Edit4.Text,R,Code);
          if Code=0 then begin
            y2:=R; FormMain.DrawGraphic;
          end;
        end;
  end;
end;

procedure TFormTools.SetEdit;
begin
  Edit1.Text:=FloatToStrF(x1,ffFixed,6,2);
  Edit2.Text:=FloatToStrF(x2,ffFixed,6,2);
  Edit3.Text:=FloatToStrF(y1,ffFixed,6,2);
  Edit4.Text:=FloatToStrF(y2,ffFixed,6,2);
end;

procedure TFormTools.FormCreate(Sender: TObject);
begin
  SetEdit;
end;

procedure TFormTools.SpeedButton6Click(Sender: TObject);
begin
  case (Sender as TSpeedButton).Tag of
    6: begin
         Y1:=Y1-0.1; Y2:=Y2-0.1;
       end;
    7: begin
         X1:=X1-0.1; X2:=X2-0.1;
       end;
    8: begin
         Y1:=Y1+0.1; Y2:=Y2+0.1;
       end;
    9: begin
         X1:=X1+0.1; X2:=X2+0.1;
       end;
    5: begin
         X1:=0.8*X1; X2:=0.8*X2;
         Y1:=0.8*Y1; Y2:=0.8*Y2;
       end;
    4: begin
         X1:=1.2*X1; X2:=1.2*X2;
         Y1:=1.2*Y1; Y2:=1.2*Y2;
       end;
  end;
  SetEdit;
  FormMain.DrawGraphic;
end;

procedure TFormTools.ChLnYClick(Sender: TObject);
begin
  FormMain.DrawGraphic;
end;

end.
