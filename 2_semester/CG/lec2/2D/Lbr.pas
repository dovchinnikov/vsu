unit Lbr;

interface

const
  n=170; {����� ������� �����}
  c2=3;
  X1: double=-1.0; X2: double=3.0; Y1: double=-1.0; Y2: double=5.0;
var
  s   : string;
  Code: integer;
  R   : real;

function F(x: real): real;
function HH(a1,a2: real): real;
function GetDigits(dx: real): byte;
function Func(x: real; Ok: boolean): real;

const
  C1=2; c0=0.10;

implementation

function F(x: real): real;
begin
  //F:=1+3*Exp(-Sqr(x-1));
  F:=x*C1/(Abs(x)+C0);
end;

function Func(x: real; Ok: boolean): real;
begin
  if not Ok then Result:=F(x)
  else
    if F(x)>0 then Result:=Ln(F(x))
    else Result:=0;
end;

function HH(a1,a2: real): real;
begin
  Result:=1;
  while Abs(a2-a1)/Result<1 do Result:=Result/10.0;
  while Abs(a2-a1)/Result>=10 do Result:=Result*10.0;
  if Abs(a2-a1)/Result<2.0 then Result:=Result/5.0;
  if Abs(a2-a1)/Result<5.0 then Result:=Result/2.0;
end;

function GetDigits(dx: real): byte;
begin
  if dx>=5 then Result:=0
  else if dx>=0.5 then Result:=1
  else if dx>=0.05 then Result:=2
  else if dx>=0.005 then Result:=3
  else if dx>=0.0005 then Result:=4 else Result:=5;
end;

end.
