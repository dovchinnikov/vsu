unit UnitMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Lib, Grids, StdCtrls, Buttons, ExtCtrls;

type
  TFormMain = class(TForm)
    StringGrid1: TStringGrid;
    BitBtn1: TBitBtn;
    RadioGroup1: TRadioGroup;
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure StringGrid1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure StringGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    drawing: boolean;
    x0,y0,xt,yt: integer;
    x1,y1,x2,y2: real;
    I1,I2,J1,J2: integer;
    function II(x: real): integer;
    function JJ(y: real): integer;
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure TFormMain.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=true;
  x0:=x; y0:=y; xt:=x; yt:=y;
  with Canvas do begin
    Pen.Mode:=pmNotXor;
    MoveTo(x0,y0); LineTo(xt,yt);
  end;
end;

procedure TFormMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if drawing then
  with Canvas do begin
    MoveTo(x0,y0); LineTo(xt,yt);
    xt:=x; yt:=y;
    MoveTo(x0,y0); LineTo(xt,yt);
  end;
end;

procedure TFormMain.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=false;
  with Canvas do begin
    MoveTo(x0,y0); LineTo(xt,yt);
    Pen.Mode:=pmCopy;
    case RadioGroup1.ItemIndex of
      0: begin
           MoveTo(x0,y0); LineTo(xt,yt);
         end;
      1: MyLine0(x0,y0,xt,yt, Canvas);
      2: MyLine1(x0,y0,xt,yt, Canvas);
    end;  
  end;
end;

procedure TFormMain.StringGrid1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  with StringGrid1.Canvas do begin
    if w and (1 shl ACol)<>0 then Brush.Color:=0 else Brush.Color:=clWhite;
    FillRect(Rect);
  end;
end;

procedure TFormMain.StringGrid1DblClick(Sender: TObject);
begin
  with StringGrid1 do begin
    if w and (1 shl Col)=0 then w:=w or (1 shl Col)
    else w:=w and not (1 shl Col);
    Refresh;
  end;
end;

function TFormMain.II(x: real): integer;
// ������� ��������������� �� ��� OX
begin
  II:=I1+Trunc((x-X1)*(I2-I1)/(X2-X1));
end;

function TFormMain.JJ(y: real): integer;
// ������� ��������������� �� ��� OY
begin
  JJ:=J2+Trunc((y-Y1)*(J1-J2)/(Y2-Y1));
end;

procedure TFormMain.BitBtn1Click(Sender: TObject);
const
  n=60;
var
  i: integer;
  k: byte;
  h,x,y: real;
begin
  // ���������� ���� �� ��� OX
  h:=(x2-x1)/n;
  with Canvas do begin
    FillRect(Rect(0,0,Width,Height));
    MoveTo(II(x1),JJ(0)); LineTo(II(x2),JJ(0));
    MoveTo(II(0),JJ(y1)); LineTo(II(0),JJ(y2));

    x:=x1; y:=Func(x);
    MoveTo(II(x),JJ(y));
    Pen.Style:=psSolid;
    Pen.Color:=clBlack;
    for i:=1 to n do begin
      x:=x+h; y:=Func(x);
      LineTo(II(x),JJ(y));
    end;
  end;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
  // ������� ���� �� ������
  I1:=0; J1:=0; I2:=ClientWidth; J2:=ClientHeight;
  x1:=-1; x2:=4; y1:=-1; y2:=2;
end;

end.
