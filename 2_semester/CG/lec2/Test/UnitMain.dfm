object FormMain: TFormMain
  Left = 192
  Top = 114
  Caption = 'FormMain'
  ClientHeight = 473
  ClientWidth = 804
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 512
    Top = 0
    Width = 345
    Height = 25
    ColCount = 16
    DefaultColWidth = 20
    DefaultRowHeight = 20
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    TabOrder = 0
    OnDblClick = StringGrid1DblClick
    OnDrawCell = StringGrid1DrawCell
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Draw'
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object RadioGroup1: TRadioGroup
    Left = 744
    Top = 32
    Width = 105
    Height = 65
    Caption = #1040#1083#1075#1086#1088#1080#1090#1084
    ItemIndex = 0
    Items.Strings = (
      'Line'
      'FOR'
      #1041#1088#1077#1079#1077#1085#1093#1101#1084)
    TabOrder = 2
  end
end
