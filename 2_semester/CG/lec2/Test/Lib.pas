unit Lib;

interface

uses Graphics;

var
  w: word=$FF00;

procedure MyLine0(x1,y1,x2,y2: integer; Canvas: TCanvas);
procedure MyLine1(x1,y1,x2,y2: integer; Canvas: TCanvas);
function Func(x: real): real;

implementation

procedure MyLine0(x1,y1,x2,y2: integer; Canvas: TCanvas);
var
  x,y: integer;
begin
  with Canvas do begin
    for x:=x1 to x2 do begin
      y:=y1+Trunc((x-x1)*(y2-y1)/(x2-x1));
      if w and (1 shl (x mod 16))<>0 then
        Pixels[x,y]:=clBlack;
    end;
  end;
end;

procedure MyLine1(x1,y1,x2,y2: integer; Canvas: TCanvas);
var
  x,y,d,dx,dy: integer;
begin
  x := x1; y := y1;
  dX := x2 - x1;
  dY := y2 - y1;
  d :=2*dY - dX;
  with Canvas do begin
    while X <= X2 do begin
      if w and (1 shl (x mod 16))<>0 then
        Pixels[X,Y]:=clBlack;
      X := X + 1;
      d := d + 2*dY;
      if d >= 0 then begin
        Y := Y + 1;
        d := d - 2*dX
      end
    end
  end;
end;

function Func(x: real): real;
begin
  Result:=Exp(-x*x);
end;

end.
