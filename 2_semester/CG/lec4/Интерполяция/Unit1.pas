unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, ExtCtrls, StdCtrls, Math, Lbr, UnitTools;

type
  TFormMain = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    drawing: boolean;
    I1,I2,J1,J2: integer;
    x0,y0,xt,yt: integer;
    function II(x: real): integer;
    function JJ(y: real): integer;
    function XX(I: Integer): real;
    function YY(J: Integer): real;
  public
    { Public declarations }
    procedure DrawLine;
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure TFormMain.DrawLine;
var
  i,L,j,k: integer;
  h,u,v: real;
begin
  with Canvas do begin
    Brush.Color:=clWhite;
    FillRect(Rect(0,0,I2,J2));
    MoveTo(II(0),JJ(Y1)); LineTo(II(0),JJ(Y2));
    MoveTo(II(X1),JJ(0)); LineTo(II(x2),JJ(0));
    L:=Length(Point);
    if L>0 then begin
      Pen.Color:=clBlack;
      k:=FormTools.RgMethod.ItemIndex;
      case k of
        0: begin
             MoveTo(II(Point[0].x),JJ(Point[0].y));
             for i:=1 to L-1 do
               LineTo(II(Point[i].x),JJ(Point[i].y));
           end;
        1: begin
             h:=SetH;
             u:=Point[0].x; v:=Lagr(u);
             MoveTo(II(u),JJ(v));
             for i:=1 to m do begin
               u:=u+h; v:=Lagr(u);
               LineTo(II(u),JJ(v));
             end;
           end;
        2: begin
             h:=SetH;
             u:=Point[0].x; v:=F2(u);
             MoveTo(II(u),JJ(v));
             for i:=1 to m do begin
               u:=u+h; v:=F2(u);
               LineTo(II(u),JJ(v));
             end;
           end;
        3: begin
             for i:=1 to L-1 do begin
               h:=(Point[i].x-Point[i-1].x)/m;
               u:=Point[i-1].x; v:=F3(i,u);
               MoveTo(II(u),JJ(v));
               for j:=1 to m do begin
                 u:=Point[i-1].x+j*h; v:=F3(i,u); LineTo(II(u),JJ(v));
               end;
             end;
           end;
        4: begin
             SetLength(w,L);
             for i:=0 to L-1 do begin
               w[i].x:=II(Point[i].x);
               w[i].y:=jj(Point[i].y);
             end;
             PolyBezier(w);
             Pen.Color:=clSilver;
             if L mod 3=1 then
             for i:=0 to L div 3-1 do begin
               MoveTo(w[3*i].X,w[3*i].Y);
               LineTo(w[3*i+1].X,w[3*i+1].Y);
               MoveTo(w[3*(i+1)].X,w[3*(i+1)].Y);
               LineTo(w[3*(i+1)-1].X,w[3*(i+1)-1].Y);
             end;
           end;
      end;
      for i:=0 to L-1 do
        RectAngle(II(Point[i].x)-3,JJ(Point[i].y)-3,II(Point[i].x)+3,JJ(Point[i].y)+3);
    end;
  end;
end;

function TFormMain.II(x: real): integer;
// ������� ��������������� �� ��� OX
begin
  II:=I1+Trunc((x-X1)*(I2-I1)/(X2-X1));
end;

function TFormMain.XX(I: Integer): real;
// ������� ��������������� �� ��� OX
begin
  Result:=x1+(I-I1)*(x2-x1)/(I2-I1);
end;

function TFormMain.YY(J: Integer): real;
// ������� ��������������� �� ��� OY
begin
  Result:=y1+(J-J2)*(y2-y1)/(J1-J2);
end;

function TFormMain.JJ(y: real): integer;
// ������� ��������������� �� ��� OY
begin
  JJ:=J2+Trunc((y-Y1)*(J1-J2)/(Y2-Y1));
end;

procedure TFormMain.FormCreate(Sender: TObject);
var i: byte;
begin
  I1:=0; J1:=0; I2:=Width; J2:=Height;
  SetLength(Point,7);
  for i:=0 to 6 do begin
    Point[i].x:=i;
    Point[i].y:=Random+i;
  end;
end;

procedure TFormMain.FormPaint(Sender: TObject);
begin
  DrawLine;
end;

procedure TFormMain.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  L,i: integer;
begin
  case fl_tools of
    0: begin
         NumPoint:=FindPoint(XX(x),YY(y));
         drawing:=true;
       end;
    1: begin
         L:=Length(Point); Inc(L); SetLength(Point,L);
         Point[L-1].x:=XX(x);
         Point[L-1].y:=YY(y);
         case FormTools.RgMethod.ItemIndex of
           2: Gauss(CountStep,Point,C2);
           3: SetSystem3D(a,b,c,d);
         end;
         DrawLine;
       end;
    2: begin
         NumPoint:=FindPoint(XX(x),YY(y));
         L:=Length(Point);
         for i:=NumPoint+1 to L-1 do Point[i-1]:=Point[i];
         Dec(L); SetLength(Point,L);
         case FormTools.RgMethod.ItemIndex of
           2: Gauss(CountStep,Point,C2);
           3: SetSystem3D(a,b,c,d);
         end;
         DrawLine;
       end;
    3: begin
         drawing:=true;
         x0:=x; y0:=y; xt:=x; yt:=y;
         with Canvas do
         DrawFocusRect(Rect(x0,y0,xt,yt));
       end;
  end;
end;

procedure TFormMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if drawing then
  case fl_tools of
    0: begin
         Point[NumPoint].x:=XX(x);
         Point[NumPoint].y:=YY(y);
         case FormTools.RgMethod.ItemIndex of
           2: Gauss(CountStep,Point,C2);
           3: SetSystem3D(a,b,c,d);
         end;
         DrawLine;
       end;
    3: with Canvas do begin
         DrawFocusRect(Rect(x0,y0,xt,yt));
         xt:=x; yt:=y;
         DrawFocusRect(Rect(x0,y0,xt,yt));
       end;
  end;
end;

procedure TFormMain.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=false;
  case fl_tools of
    3: with Canvas do begin
         DrawFocusRect(Rect(x0,y0,xt,yt));
         X1:=XX(x0); X2:=XX(xt);
         Y1:=YY(yt); Y2:=YY(y0);
         DrawLine;
       end;
  end;
end;

end.
