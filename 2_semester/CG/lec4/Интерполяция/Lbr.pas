unit Lbr;

interface

uses Types,Math;

type
  TVector=record
    x,y: real;
  end;
  TAVector=array of TVector;
  // ��� ���������� ���������
  TArray=array of real;
  TMatr = array of TArray;

const
  m=50;   // ����� ����� ����� �������� ������� 
var
  X1: double=-1.0; X2: double=7.0; Y1: double=-1.0; Y2: double=7.0;
  Point: TAVector;
  fl_tools: byte;
  NumPoint: integer;
  // ��� ���������� ���������
  C2: TArray;
  CountStep: word=4;
  // ��� ���������� ��������
  a,b,c,d: TArray;      {������������ ����������� }
  // ��� �����
  w: array of TPoint;

procedure AddPointToLine(var Point: TAVector; kAdd: byte);
function FindPoint(x,y: real): integer;
function Lagr(xt: Real): real;
function F2(x: Real): Real;
procedure Gauss(m: Byte; Point: TAVector; var c: TArray);
function SetH: real; //
function Func(k: word; u: real): real;
function F3(i: Byte; u: Real): Real;
procedure SetSystem3D(var a,b,c,d: TArray);

implementation

function Func(k: word; u: real): real;
begin
  case k of
    1: Result:=Lagr(u);
    2: Result:=F2(u);
  end;
end;

procedure System_3D(a,b,c: TArray; var d: TArray);
// ������� ���������������� ������
var
  i,L: word;
begin
  L:=Length(a)-1;
  SetLength(d,L+1);
  // ������ ���
  for i:=2 to L-1 do begin
    b[i]:=b[i]-c[i-1]*a[i]/b[i-1];
    d[i]:=d[i]-d[i-1]*a[i]/b[i-1];
  end;
  // �������� ���
  d[L-1]:=d[L-1]/b[L-1];
  for i:=L-2 downto 1 do
    d[i]:=(d[i]-d[i+1]*c[i])/b[i];
end;

procedure SetSystem3D(var a,b,c,d: TArray);
var
  i,L: word;
  delt,               // x[i-1]-x[i]
  p,q,r,w: TArray;    // ������������ �������
begin
  L:=Length(Point);
  SetLength(delt,L);
  SetLength(w,L);
  SetLength(q,L);
  SetLength(p,L);
  SetLength(r,L);
  SetLength(a,L);
  SetLength(b,L);
  SetLength(c,L);
  SetLength(d,L);
  for i:=1 to L-1 do delt[i]:=Point[i-1].x-Point[i].x;
  // ���������� ������������� ��� ������� ���������
  for i:=1 to L-2 do begin
    w[i]:=-(Point[i].y-Point[i+1].y)/delt[i+1]+(Point[i-1].y-Point[i].y)/delt[i];
    p[i]:=delt[i]/3;
    q[i]:=2*(delt[i]+Delt[i+1])/3;      r[i]:=delt[i+1]/3;
  end;
  // ������� ���������������� ������� ���������
  System_3D(p,q,r,w);
  for i:=1 to L-1 do b[i]:=w[i]; b[L-1]:=0;
  // ���������� ��������� ������������� ����������
  c[1]:=(Point[0].y-Point[1].y)/delt[1]-2*b[1]*delt[1]/3;
  a[1]:=-b[1]/(3*delt[1]);  d[1]:=Point[1].y;
  for i:=2 to L-1 do begin
    d[i]:=Point[i].y;  a[i]:=(b[i-1]-b[i])/(3*delt[i]);
    c[i]:=(Point[i-1].y-Point[i].y)/delt[i]-delt[i]*b[i-1]/3-2*delt[i]*b[i]/3;
  end;
end;

function F3(i: Byte; u: Real): Real;
begin
  Result:=((a[i]*(u-Point[i].x)+b[i])*(u-Point[i].x)+c[i])*(u-Point[i].x)+d[i];
end;

function SetH: real; //
var
  i,L: word;
  Xmin,Xmax: real;
begin
  L:=Length(Point);
  Xmin:=Point[0].x; Xmax:=Point[0].x;
  for i:=1 to L-1 do begin
    if Xmin>Point[i].x then Xmin:=Point[i].x;
    if Xmax<Point[i].x then Xmax:=Point[i].x;
  end;
  Result:=(Xmax-Xmin)/m;
end;

procedure Gauss(m: Byte; Point: TAVector; var c: TArray);
var
  A: TMatr;
  b: TArray;
  i,j,k,l,n: word;
  P: Real;
begin
  SetLength(A,m+1);
  for i:=0 to m do SetLength(A[i],m+1);
  SetLength(C,m+1);
  SetLength(B,m+1);
  n:=Length(Point);
  // ������������ �������
  for j:=0 to m do
    for k:=j to m do begin
      A[j,k]:=0;
      for i:=0 to n-1 do begin
        p:=1; for l:=1 to j+k do p:=p*Point[i].x;
        A[j,k]:=A[j,k]+P;
      end;
      A[k,j]:=A[j,k];
    end;
  // ��������� �����
  for k:=0 to m do begin
    B[k]:=0;
    for i:=0 to n-1 do begin
      P:=Point[i].y; for l:=1 to k do P:=P*Point[i].x;
      B[k]:=B[k]+P;
    end;
  end;
  // ������� �������: ������ ���
  for i:=0 to m-1 do
    for j:=i+1 to m do begin
      for k:=i+1 to m do
        A[k,j]:=A[k,j]-A[i,j]*A[k,i]/A[i,i];
      B[j]:=B[j]-B[i]*A[i,j]/A[i,i];
    end;
  // ������� �������: �������� ���
  for j:=m downto 0 do begin
    c[j]:=B[j];
    for k:=j+1 to m do c[j]:=c[j]-A[k,j]*c[k];
    c[j]:=c[j]/A[j,j];
  end;
end; // Gauss

function F2(x: Real): Real;
// ���������� �������� ����������
var
  i,L: word;
begin
  L:=Length(C2);
  Result:=0;
  for i:=L-1 downto 0 do Result:=Result*x+C2[i];
end;

function FindPoint(x,y: real): integer;
var
  i,L: integer;
  d,dMin: real;

  function Dist(n: word; x,y: real): real;
  begin
    Result:=Sqrt(Sqr(x-Point[n].x)+Sqr(y-Point[n].y));
  end;

begin
  L:=Length(Point);
  Result:=0; dMin:=Dist(0,x,y);
  for i:=1 to L-1 do begin
    d:=Dist(i,x,y);
    if d<dMin then begin
      dMin:=d; Result:=i;
    end;
  end;
end;

function Lagr(xt: Real): real;
var
  i,j,L: Byte;
  P: Real;
begin
  Result:=0; L:=Length(Point);
  for i:=0 to L-1 do begin
    P:=1;
    for j:=0 to L-1 do
      if i<>j then P:=P*(xt-Point[j].x)/(Point[i].x-Point[j].x);
    Result:=Result+Point[i].y*P;
  end;
end;

function FBzXYZ(t: real; V0,V1,V2,V3: TVector): TVector;
var
  t3,t2,p2,p3: real;
begin
  t2:=t*t; t3:=t2*t; p2:=(1-t)*(1-t); p3:=p2*(1-t);
  Result.x:=t3*V0.x+3*(1-t)*t2*V1.x+3*t*p2*V2.x+p3*V3.x;
  Result.y:=t3*V0.y+3*(1-t)*t2*V1.y+3*t*p2*V2.y+p3*V3.y;
end;

procedure AddPointToLine(var Point: TAVector; kAdd: byte);
var
  L,L0,L1,i,j: integer;
  Po,Pt: array of TVector;
  h,alf,D: real;

  function Angle(m,n: word): real;
  begin
    Result:=ArcTan2(Point[n].y-Point[m].y,Point[n].x-Point[m].x);
  end;

  function SetPoint(P: TVector; alf,d: real; sign: shortint): TVector;
  begin
    Result.x:=P.x+sign*d/3*cos(alf);
    Result.y:=P.y+sign*d/3*sin(alf);
  end;

  function Dist(m,n: word): real;
  begin
    Result:=Sqrt(Sqr(Point[n].x-Point[m].x)+Sqr(Point[n].y-Point[m].y));
  end;

begin
  L:=Length(Point);
  L0:=(L-1)*3+1; SetLength(Po,L0);
  L1:=(L-1)*(kAdd+1)+1; SetLength(Pt,L1);

  for i:=0 to L-1 do begin
    Po[3*i]:=Point[i];
    Pt[(kAdd+1)*i]:=Point[i];
    if i=0 then begin
      alf:=Angle(0,1);
      D:=Dist(0,1);
      Po[1]:=SetPoint(Point[0],alf,D,1);
    end
    else
    if i=L-1 then begin
      alf:=Angle(L-2,L-1);
      D:=Dist(L-2,L-1);
      Po[L0-2]:=SetPoint(Point[L-1],alf,D,-1);
    end
    else begin
      alf:=Angle(i-1,i+1);

      D:=Dist(i-1,i);
      Po[3*i-1]:=SetPoint(Point[i],alf,D,-1);

      D:=Dist(i,i+1);
      Po[3*i+1]:=SetPoint(Point[i],alf,D,1);
    end;
  end;

  h:=1.0/(kAdd+1);
  for i:=0 to L-2 do
    for j:=1 to kAdd do
      Pt[i*(kAdd+1)+j]:=FBzXYZ(1-h*j, Po[3*i],Po[3*i+1],Po[3*i+2],Po[3*i+3]);

  SetLength(Point,L1);
  for i:=0 to L1-1 do Point[i]:=Pt[i];
  SetLength(Po,0); SetLength(Pt,0);
end;

end.
