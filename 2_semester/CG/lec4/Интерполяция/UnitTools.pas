unit UnitTools;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Lbr, ExtCtrls;

type
  TFormTools = class(TForm)
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    BitBtn1: TBitBtn;
    RgMethod: TRadioGroup;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure RgMethodClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormTools: TFormTools;

implementation

uses Unit1;

{$R *.dfm}

procedure TFormTools.SpeedButton1Click(Sender: TObject);
begin
  fl_tools:=(Sender as TSpeedButton).tag;
  Caption:=IntToStr(fl_tools);
end;

procedure TFormTools.BitBtn1Click(Sender: TObject);
begin
  AddPointToLine(Point,6);
  FormMain.DrawLine;
end;

procedure TFormTools.SpeedButton4Click(Sender: TObject);
begin
  X1:=1.2*X1; X2:=1.2*X2;
  Y1:=1.2*Y1; Y2:=1.2*Y2;
  FormMain.DrawLine;
end;

procedure TFormTools.SpeedButton5Click(Sender: TObject);
begin
  X1:=0.8*X1; X2:=0.8*X2;
  Y1:=0.8*Y1; Y2:=0.8*Y2;
  FormMain.DrawLine;
end;

procedure TFormTools.SpeedButton9Click(Sender: TObject);
begin
  X1:=X1+0.5; X2:=X2+0.5;
  FormMain.DrawLine;
end;

procedure TFormTools.SpeedButton7Click(Sender: TObject);
begin
  X1:=X1-0.5; X2:=X2-0.5;
  FormMain.DrawLine;
end;

procedure TFormTools.SpeedButton6Click(Sender: TObject);
begin
  Y1:=Y1-0.5; Y2:=Y2-0.5;
  FormMain.DrawLine;
end;

procedure TFormTools.SpeedButton8Click(Sender: TObject);
begin
  Y1:=Y1+0.5; Y2:=Y2+0.5;
  FormMain.DrawLine;
end;

procedure TFormTools.RgMethodClick(Sender: TObject);
begin
  case RgMethod.ItemIndex of
    2: Gauss(CountStep,Point,C2);
    3: SetSystem3D(a,b,c,d); // ���������� ������������� ����������
  end;
  FormMain.DrawLine;
end;

end.
