unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Image1: TImage;
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormPaint(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  with Image1.Canvas do begin
    Pen.Color:=clRed;
    MoveTo(10,10);
    LineTo(100,100);
  end;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Caption:=IntToStr(x)+' '+IntToStr(y);
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
  with Canvas do begin
    Pen.Color:=$FF0000;
    Pen.Style:=psDot;
    Pen.Width:=1;
    MoveTo(100,0);
    LineTo(100,100);

    Brush.Color:=clRed;

    Brush.Style:=bsHorizontal;
    //Rectangle(10,10,200,200);

  	Canvas.Polygon([Point(10, 10),
	  	Point(30, 10),Point(130, 30), Point(240, 120)]);

    Font.Name:='Times';
    Font.Size:=25;
    TextOut(200,200,'String');
  end;
end;

end.
