unit UnitMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus;

type
  TFormMain = class(TForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Open1: TMenuItem;
    N1: TMenuItem;
    Exit1: TMenuItem;
    OpenDialog1: TOpenDialog;
    procedure Exit1Click(Sender: TObject);
    procedure Open1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReadTXT(FileName: string);
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

procedure TFormMain.Exit1Click(Sender: TObject);
begin
  Close;
end;

function SetBrushStyle(S: string): TBrushStyle;
begin
  if S = 'bsSolid' then Result := bsSolid
  else if S = 'bsClear' then Result := bsClear
  else if S = 'bsHorizontal' then Result := bsHorizontal
  else if S = 'bsVertical' then Result := bsVertical
  else if S = 'bsFDiagonal' then Result := bsFDiagonal
  else if S = 'bsBDiagonal' then Result := bsBDiagonal
  else if S = 'bsCross' then Result := bsCross
  else if S = 'bsDiagCross' then Result := bsDiagCross;
end;

function EraseSpace(s: string): string;
begin
  while (Length(s)>0) and (s[1]=' ') do Delete(s,1,1);
  Result:=s;
end;

procedure TFormMain.ReadTXT(FileName: string);
var
  f          : textfile;
  x1,y1,x2,y2: integer;
  PenColor,
  BrushColor,
  FontSize   : integer;
  ch         : char;
  s          : string;
begin
  AssignFile(f,FileName); Reset(f);
  with Canvas do
  while not EOF(f) do begin
    Read(f,ch);
    case ch of
      'C': begin
             Readln(f,PenColor);
             Pen.Color:=PenColor;
           end;
      'c': begin
             Readln(f,BrushColor);
             Brush.Color:=BrushColor;
           end;
      'R': begin
             Readln(f,x1,y1,x2,y2);
             RectAngle(x1,y1,x2,y2);
           end;
      'E': begin
             Readln(f,x1,y1,x2,y2);
             Ellipse(x1,y1,x2,y2);
           end;
      'F': begin
             Readln(f,x1,y1,x2,y2);
             FillRect(Rect(x1,y1,x2,y2));
           end;
      'T': begin
             Readln(f,x1,y1,s);
             s:=EraseSpace(s);
             TextOut(x1,y1,s);
           end;
      'S': begin
             Readln(f,FontSize);
             Font.Size:=FontSize;
           end;
      's': begin
             Readln(f,s);
             s:=EraseSpace(s);
             Brush.Style:=SetBrushStyle(S);
           end;
    end;
  end;
  CloseFile(f);
end;

procedure TFormMain.Open1Click(Sender: TObject);
begin
  if OpenDialog1.Execute then
    ReadTXT(OpenDialog1.FileName);
end;

end.
