unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;

    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  procedure drawline(x1,y1,x2,y2: integer);
var
  Form1: TForm1;
  drawing: boolean;
  xt,yt: integer;
implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  if radiobutton1.Checked  then radiobutton1click(sender);
  if radiobutton2.Checked  then radiobutton2click(sender);
  if radiobutton3.Checked  then radiobutton3click(sender);

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  form1.Canvas.Pixels[strtoint(edit1.Text),strtoint(edit2.Text)]   :=clblack;
  form1.Canvas.Pixels[strtoint(edit1.Text),strtoint(edit4.Text) ]  :=clblack;
  form1.Canvas.Pixels[strtoint(edit3.Text),strtoint(edit4.Text)  ] :=clblack;
  form1.Canvas.Pixels[strtoint(edit3.Text),strtoint(edit2.Text)   ]:=clblack;

end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=true;
  xt:=X;
  yt:=Y;
end;

procedure drawlinex(x1,y1,x2,y2: integer);
var
  tmp,dx,dy,d,x,y: integer;
begin
  if x1>x2 then
  begin
    tmp:=x1;
    x1:=x2;
    x2:=tmp;
    tmp:=y1;
    y1:=y2;
    y2:=tmp;
  end;

  with form1.Canvas do
  if y1<y2 then
  begin
    x := x1;
    y := y1;
    dx := x2 - x1;
    dy := y2 - y1;
    d := 2*dy - dx;
    while x <= x2 do
    begin
      Pixels [x, y]:=clblack;
      x := x + 1;
      d := d + 2*dy;
      if d >= 0 then
      begin
        y := y + 1;
        d := d - 2*dx
      end;
    end;
  end
  else begin
    x := x1;
    y := y1;
    dx := x2 - x1;
    dy := y1 - y2;
    d := 2*dy - dx;
    while x <= x2 do
    begin
      Pixels [x, y]:=clblack;
      x := x + 1;
      d := d + 2*dy;
      if d >= 0 then
      begin
        y := y - 1;
        d := d - 2*dx
      end;
    end;
  end;
end;

procedure drawliney(x1,y1,x2,y2: integer);
var
  tmp,dx,dy,d,x,y: integer;
begin
  if y1>y2 then
  begin
    tmp:=x1;
    x1:=x2;
    x2:=tmp;
    tmp:=y1;
    y1:=y2;
    y2:=tmp;
  end;

  with form1.Canvas do
  if x1<x2 then
  begin
    x := x1;
    y := y1;
    dx := x2 - x1;
    dy := y2 - y1;
    d := 2*dx - dy;
    while x <= x2 do
    begin
      Pixels [x, y]:=clblack;
      y := y + 1;
      d := d + 2*dx;
      if d >= 0 then
      begin
        x := x + 1;
        d := d - 2*dy
      end;
    end;
  end
  else begin
    x := x1;
    y := y1;
    dx := x2 - x1;
    dy := y1 - y2;
    d := 2*dx - dy;
    while x <= x2 do
    begin
      Pixels [x, y]:=clblack;
      y := y + 1;
      d := d + 2*dx;
      if d >= 0 then
      begin
        x := x - 1;
        d := d - 2*dy
      end;
    end;
  end;
end;

procedure drawline(x1,y1,x2,y2: integer);
begin
  if abs(x1-x2)>=abs(y1-y2) then drawlinex(x1,y1,x2,y2)
  else drawlinex(x1,y1,x2,y2);
end;
procedure TForm1.RadioButton1Click(Sender: TObject);
var
  x,y,dx,dy,d,i,tmp,x1,y1,x2,y2: integer;
begin

   // x1:=strtoint(edit1.text);
    y1:=strtoint(edit2.text);
    x2:=strtoint(edit3.text);
    y2:=strtoint(edit4.text);
    drawline(x1,y1,x2,y2);
    {if x1=x2 then
    for i := y1 to y2 do
      form1.Canvas.Pixels[x1,i]:= clblack;    }
end;


procedure TForm1.RadioButton2Click(Sender: TObject);
var xc,yc,r,x,y,d:integer;
procedure sim(x,y:integer);
begin
  with form1.Canvas do
  begin
    pixels[xc+x,yc+y]:=clblack;
    pixels[xc-x,yc+y]:=clblack;
    pixels[xc+x,yc-y]:=clblack;
    pixels[xc-x,yc-y]:=clblack;
    pixels[xc+y,yc+x]:=clblack;
    pixels[xc-y,yc+x]:=clblack;
    pixels[xc+y,yc-x]:=clblack;
    pixels[xc-y,yc-x]:=clblack;
  end;
end;
begin
  edit4.Text:='300';
  xc:=(strtoint(edit3.Text)+strtoint(edit1.Text))div 2;
  yc:=(strtoint(edit2.Text)+strtoint(edit4.Text))div 2;
  r:= (strtoint(edit3.Text)-strtoint(edit1.Text))div 2;

  x:=0;
  y:=r;
  d:=3-2*y;
  x:=0;
  y:=r;

  while(x <= y) do
  begin
   sim(x,y);
   if d<0    then d:=d+4*x+6
   else begin
     d:=d+4*(x-y)+10;
     dec(y);
   end;
   inc(x);
  end;
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
var xc,yc,rx,ry,x,y,d:integer;
    k: real;

procedure simy(x,y:integer);
begin
  with form1.Canvas do
  begin
    pixels[xc+x,round(yc+y/k)]:=clblack;
    pixels[xc-x,round(yc+y/k)]:=clblack;
    pixels[xc+x,round(yc-y/k)]:=clblack;
    pixels[xc-x,round(yc-y/k)]:=clblack;
    pixels[xc+y,round(yc+x/k)]:=clblack;
    pixels[xc-y,round(yc+x/k)]:=clblack;
    pixels[xc+y,round(yc-x/k)]:=clblack;
    pixels[xc-y,round(yc-x/k)]:=clblack;
  end;
end;

procedure simx(x,y:integer);
begin
  with form1.Canvas do
  begin
    pixels[round(xc+x/k),yc+y]:=clblack;
    pixels[round(xc-x/k),yc+y]:=clblack;
    pixels[round(xc+x/k),yc-y]:=clblack;
    pixels[round(xc-x/k),yc-y]:=clblack;
    pixels[round(xc+y/k),yc+x]:=clblack;
    pixels[round(xc-y/k),yc+x]:=clblack;
    pixels[round(xc+y/k),yc-x]:=clblack;
    pixels[round(xc-y/k),yc-x]:=clblack;
  end;
end;

begin

  xc:=(strtoint(edit3.Text)+strtoint(edit1.Text))div 2;   //�������� ������
  yc:=(strtoint(edit2.Text)+strtoint(edit4.Text))div 2;   //�������� ������
  rx:= (strtoint(edit3.Text)-strtoint(edit1.Text))div 2;  //������ �� ��� �
  ry:= (strtoint(edit4.Text)-strtoint(edit2.Text))div 2;  //������ �� ��� Y

  if rx>ry then
  begin
    k:=rx/ry;                     //����������� ������ �� ��� �
    x:=0;
    y:=rx;                        //����� ������� ������
    d:=3-2*y;
    while(x <= y) do              //�������� �����������
    begin
     simy(x,y);                   //������� ������ �� ��� �, ������������� � ������� �� �
     if d<0    then d:=d+4*x+6
     else begin
       d:=d+4*(x-y)+10;
       dec(y);
     end;
     inc(x);
    end;                         //����� ��������� �����������
  end
  else begin
    k:=ry/rx;
    x:=0;
    y:=ry;
    d:=3-2*y;
    while(x <= y) do
    begin
     simx(y,x);                 // � ������� �� �
     if d<0    then d:=d+4*x+6
     else begin
       d:=d+4*(x-y)+10;
       dec(y);
     end;
     inc(x);
    end;
  end;






end;


end.
