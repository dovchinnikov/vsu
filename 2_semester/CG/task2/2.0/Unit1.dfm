object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 371
  ClientWidth = 722
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 488
    Top = 208
    Width = 3
    Height = 13
  end
  object Editx1: TEdit
    Left = 488
    Top = 16
    Width = 73
    Height = 21
    TabOrder = 0
    Text = 'Editx1'
  end
  object Edity1: TEdit
    Left = 576
    Top = 16
    Width = 73
    Height = 21
    TabOrder = 1
    Text = 'Edity1'
  end
  object Editx2: TEdit
    Left = 488
    Top = 43
    Width = 73
    Height = 21
    TabOrder = 2
    Text = 'Editx2'
  end
  object Edity2: TEdit
    Left = 576
    Top = 43
    Width = 73
    Height = 21
    TabOrder = 3
    Text = 'Edity2'
  end
  object Button1: TButton
    Left = 488
    Top = 160
    Width = 161
    Height = 33
    Caption = 'Draw'
    TabOrder = 4
    OnClick = Button1Click
  end
  object RadioButton1: TRadioButton
    Left = 488
    Top = 72
    Width = 113
    Height = 17
    Caption = 'RadioButton1'
    TabOrder = 5
  end
  object RadioButton2: TRadioButton
    Left = 488
    Top = 96
    Width = 113
    Height = 17
    Caption = 'RadioButton2'
    Checked = True
    TabOrder = 6
    TabStop = True
  end
  object RadioButton3: TRadioButton
    Left = 488
    Top = 120
    Width = 113
    Height = 17
    Caption = 'RadioButton3'
    TabOrder = 7
  end
end
