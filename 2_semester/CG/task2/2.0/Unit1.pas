unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Editx1: TEdit;
    Edity1: TEdit;
    Editx2: TEdit;
    Edity2: TEdit;
    Button1: TButton;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure line;
    procedure circle;
    procedure draw;
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  procedure putpixel(x,y: integer);

var
  Form1: TForm1;
  x1,y1,x2,y2:integer;
  flag,drawing: boolean;

  
implementation

{$R *.dfm}

procedure Tform1.line;
var
  x,y,d,dy,dx,tmp: integer;
begin
  x1:=strtoint(editx1.Text);
  x2:=strtoint(editx2.Text);
  y1:=strtoint(edity1.Text);
  y2:=strtoint(edity2.Text);
  flag:=false;

  if x1=x2 then
  begin
    if y1>y2 then
    begin
      tmp:=y1;
      y1:=y2;
      y2:=tmp;
    end;
    for y := y1 to y2 do
    form1.Canvas.Pixels[x1,y]:=form1.Canvas.Pen.Color;
  end;
  if y1=y2 then
  begin
    if x1>x2 then
    begin
      tmp:=x1;
      x1:=x2;
      x2:=tmp;
    end;
    for x := x1 to x2 do
    form1.Canvas.Pixels[x,y1]:=form1.Canvas.Pen.Color;
  end;

  dx := abs(x2 - x1);
  dy := abs(y2 - y1);

  if dx<dy then    //������ ���
  begin
    tmp:=x1;
    x1:=y1;
    y1:=tmp;
    tmp:=x2;
    x2:=y2;
    y2:=tmp;
    tmp:=dx;
    dx:=dy;
    dy:=tmp;
    flag:=true;
  end;

  if x1>x2 then    //������ ����� �������
  begin
    tmp:=x1;
    x1:=x2;
    x2:=tmp;
    tmp:=y1;
    y1:=y2;
    y2:=tmp;
  end;

  x:= x1;
  y:= 0;            //
  d := 2*dy - dx;

  while x <= x2 do begin  //�������� ����������
    putpixel(x,y);
    x := x + 1;
    d := d + 2*dy;
    if d >= 0 then
    begin
      y := y + 1;
      d := d - 2*dx;
    end;
  end;

end;

procedure tform1.circle;
var
  x,y,d,xc,yc,r: integer;
procedure sim(x,y:integer);
begin
  with form1.Canvas do
  begin
    pixels[xc+x,yc+y]:=pen.color;
    pixels[xc-x,yc+y]:=pen.color;
    pixels[xc+x,yc-y]:=pen.color;
    pixels[xc-x,yc-y]:=pen.color;
    pixels[xc+y,yc+x]:=pen.color;
    pixels[xc-y,yc+x]:=pen.color;
    pixels[xc+y,yc-x]:=pen.color;
    pixels[xc-y,yc-x]:=pen.color;
  end;
end;


begin


  xc:=(x1+x2) div 2 ;        //���������� ������
  yc:=(y1+y2) div 2;
  r:=abs(x1-x2) div 2;       //������

  x:=0;
  y:=r;
  d:=3-2*y;

  sim(x,y);
  while(x <= y) do         //�������� ����������
  begin
   sim(x,y);
   if d<0    then d:=d+4*x+6
   else begin
     d:=d+4*(x-y)+10;
     dec(y);
   end;
   inc(x);
  end;

  
  with form1.Canvas do
  begin
    Pixels[xc,yc-r]:=canvas.pen.Color;
    Pixels[xc,yc+r]:=canvas.pen.Color;
    Pixels[xc+r,yc]:=canvas.pen.Color;
    Pixels[xc-r,yc]:=canvas.pen.Color;
  end;

  label1.Caption:=inttostr(xc)+'  '+inttostr(yc)+'  '+inttostr(r);
end;

procedure putpixel(x,y: integer);
begin
  with form1.Canvas do
  if flag then
  begin
    if y1<y2 then Pixels[y1+y,x]:=pen.Color;
    if y1>y2 then Pixels[y1-y,x]:=pen.Color;
  end
  else begin
    if y1<y2 then Pixels[x,y1+y]:=pen.Color;
    if y1>y2 then Pixels[x,y1-y]:=pen.Color;
  end;
end;

procedure Tform1.draw;
begin
  if radiobutton1.Checked  then
  form1.line;
  if radiobutton2.Checked then
  form1.circle;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  x1:=strtoint(editx1.Text);
  x2:=strtoint(editx2.Text);
  y1:=strtoint(edity1.Text);
  y2:=strtoint(edity2.Text);
  draw;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  editx1.Text:='100';
  editx2.Text:='300';
  edity1.Text:='100';
  edity2.Text:='300';
  form1.Canvas.Pen.Mode:=pmnotxor;
end;

procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  x1:=x;
  y1:=y;
  x2:=x;
  y2:=y;
  editx1.Text:=inttostr(x1);
  edity1.Text:=inttostr(y1);
  editx2.Text:=inttostr(x1);
  edity2.Text:=inttostr(y1);
  drawing:=true;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  form1.caption:='x='+inttostr(x)+' y='+inttostr(y);
  if drawing then
  begin
    draw;
    x2:=x;
    y2:=y;
    editx2.Text:=inttostr(x2);
    edity2.Text:=inttostr(y2);
    draw;
  end;

end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  drawing:=false;
end;

end.
