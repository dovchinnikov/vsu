unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AppEvnts, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  i: integer;
implementation

{$R *.dfm}


procedure TForm1.FormCreate(Sender: TObject);
begin
with form1.canvas do
begin
  pen.Width:=5;
  Brush.Color:= clyellow;
  pen.Color:=$50dcfa;
  Ellipse(200,100,400,300);

  MoveTo(220,120);
  lineto(80,50);
  LineTo(190,170);
  Arc(190,90,410,310,220,120,190,170);
  floodfill(200,120,$50dcfa,fsborder);

  MoveTo(190,190);
  lineto(100,270);
  lineto(214,270);
  Arc(190,90,410,310,190,190,214,270);
  floodfill(190,230,$50dcfa,fsborder);

  moveto(233,287);
  lineto(210,410);
  lineto(280,310);
  Arc(190,90,410,310,230,290,280,310);
  floodfill(233,327,$50dcfa,fsborder);

  moveto(300,310);
  lineto(380,430);
  lineto(360,290);
  Arc(190,90,410,310,300,310,360,290);
  floodfill(340,310,$50dcfa,fsborder);

  moveto(380,270);
  lineto(500,290);
  lineto(410,220);
  Arc(190,90,410,310,380,270,410,220);
  floodfill(410,270,$50dcfa,fsborder);

  moveto(410,200);
  lineto(550,100);
  lineto(380,130);
  Arc(190,90,410,310,410,200,380,130);
  floodfill(410,170,$50dcfa,fsborder);

  moveto(360,110);
  lineto(380,10);
  lineto(310,90);
  Arc(190,90,410,310,360,110,310,90);
  floodfill(360,80,$50dcfa,fsborder);

  moveto(290,90);
  lineto(230,20);
  lineto(230,113);
  Arc(190,90,410,310,290,90,230,113);
  floodfill(260,90,$50dcfa,fsborder);

  {pen.Color:=clgray;
  pen.Width:=1;
  for i := 1 to (form1.width div 10) do
    begin
    MoveTo(i*10,0);
    LineTo(i*10,form1.height);
    end;
  for i := 1 to (form1.height div 10) do
    begin
    MoveTo(0,i*10);
    LineTo(form1.Width,i*10);
    end; }

  pen.Color:=clblack;
  pen.Width:=3;

  moveto(220,160);
  lineto(300,160);
  arc(220,130,300,210,220,160,300,160);
  moveto(310,160);
  lineto(390,160);
  arc(310,130,390,210,310,160,390,160);
  brush.Color:=clblack;
  floodfill(230,170,clblack,fsborder);
  floodfill(320,170,clblack,fsborder);
  moveto(300,170);
  lineto(310,170);
  //ellipse(230,180,330,280);
  //for i := 280 downto 230 do
  //arc(230,180,330,280,230,230,280,280);
  end;

end;
procedure TForm1.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if ((x-300)*(x-300)+(y-200)*(y-200))<10000 then form1.canvas.Arc(230,180,330,280,230,230,280,280);
  //Ellipse(200,100,400,300);
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  form1.Caption:='x:'+inttostr(x)+' y:'+inttostr(y);
end;

end.
