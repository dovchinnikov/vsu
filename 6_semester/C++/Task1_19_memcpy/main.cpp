#include "stdafx.h"

void *memcpy1(void* dest, void* src, size_t count)
{
	char* source = (char *)src;
	char* destination = (char *)dest;
	for (int i = 0; i < (int)count; i++)
		*destination++ = *source++;
	return dest;
}

int _tmain(int argc, _TCHAR* argv[]) 
{
	char string[60] = "a_b_c_d_e_f_g_h_i_j_k_l_m_n_o_p_q_r_s_t_u_v_w_x_y_z\0";
	char buffer[60] = "..........................................................\0";

	printf("Function test without overlap\n");
	printf( "Source:\t\t%s\n", string);
	printf( "Destination:\t%s\n", buffer);
	printf( "Chars count: \t%d\n",34);
	memcpy1(buffer, string, 34);
	printf( "Result:\t\t%s\n\n", buffer);

	printf("Function test with overlap\n");
	printf( "Source:\t\t%s\n", string);
	printf( "Destination:\t%s\n", string + 10);
	printf( "Chars count: \t%d\n",22);
	memcpy1(string + 10, string, 22);
	printf( "Result:\t\t%s\n", string);

	getchar();
	return 0;
}

