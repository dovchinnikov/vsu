#include "stdafx.h"
#include "PointerList.h"
#include <iostream>

using namespace std;

void performInt()
{
	PointerList<int> pl;
	pl.push_back(1);
	pl.push_back(2);
	pl += 3;
	pl.push_back(4);
	pl.push_back(5);
	pl.push_back(6);
	pl.push_back(7);
	cout << "Integer list:" << endl << pl << endl;

	pl.remove(4);
	cout << endl << "Removing item with value = 4" << endl << pl << endl;

	cout << endl << "Pop from back of List" << endl << "Returned value is: "<< pl.pop_back() << endl << pl << endl;

	cout << endl << "Checking List" << endl << pl << endl << endl << "----------------------------------------" << endl;
}

void performChar()
{
	PointerList<char> pl;
	pl.push_back(1);
	pl.push_back(2);
	pl += 3;
	pl.push_back(4);
	pl.push_back(5);
	pl.push_back(6);
	pl.push_back(7);
	cout << "Char list:" << endl << pl << endl;

	pl.remove(4);
	cout << endl << "Removing item with value = 4" << endl << pl << endl;

	cout << endl << "Pop from back of List" << endl << "Returned value is: "<< pl.pop_back() << endl << pl << endl;

	cout << endl << "Checking List" << endl << pl << endl << endl << "----------------------------------------" << endl;
}

void performPChar()
{
	PointerList<char *> pl;
	pl.push_back("one");
	pl += "two";
	pl.push_back("three");
	pl.push_back("four");
	pl.push_back("five");
	pl.push_back("six");
	pl.push_back("seven");
	cout << "PChar list:" << endl << pl << endl;

	pl.remove("four");
	cout << endl << "Removing item with value = 4" << endl << pl << endl;

	cout << endl << "Pop from back of List" << endl << "Returned value is: "<< pl.pop_back() << endl << pl << endl;

	cout << endl << "Checking List" << endl << pl << endl << endl << "----------------------------------------" << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	performInt();
	performChar();
	performPChar();

	getchar();
	return 0;
}
