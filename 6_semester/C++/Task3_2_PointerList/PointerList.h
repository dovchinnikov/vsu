#pragma once

#define NULL 0
#include <iostream>

using namespace std;

template<typename T> class PointerList;

template<typename T> ostream& operator<<(ostream& cout_, const PointerList<T> &list)
{
	for (int i = 0; i < list.size; i++)
		cout_ << "'"<<((PointerList<T> &)list)[i]<<"'  ";
	//cout_ << "It works!";
	return cout_;
}

template<typename T> class PointerList
{
	friend ostream& ::operator<<(ostream&, const PointerList<T> &);

public:
	struct Node
	{
		Node *prev;
		Node *next;
		T value;
	};

	PointerList<T>()
	{
		head = NULL;
		tail = NULL;
		size = 0;
	}

	~PointerList()
	{
		while (head != tail)
		{
			head = head->next;
//			delete head->prev->value;
			delete head->prev;
		}
//		delete head->value;
		delete head;
	}

	void push_back(T value)
	{
		Node *node = new Node();
		node->value = value;
		if (size ==0)
		{
			node->next = node;
			node->prev = node;
			head = node;
			tail = node;
			size = 1;
		}
		else
		{
			node->next = head;
			node->prev = tail;
			tail->next = node;
			head->prev = node;
			tail = node;
			size++;
		}
	}

	T pop_back()
	{
		T ret_value = tail->value;
		head->prev = tail->prev;
		tail->prev->next = head;
		delete tail;
		tail = head->prev;
		size--;
		return ret_value;
	}
	
	void remove(T value)
	{
		Node *curr = head;
		do {
			if (curr->value == value)
			{
				curr->prev->next = curr->next;
				curr->next->prev = curr->prev;
//				delete curr->value;
				delete curr;
				size--;
				break;
			}
			else
				curr = curr->next;
		} while (curr!=head);
	}

	bool isEmpty(void)
	{
		return size == 0;
	}
	
	bool isNotEmpty(void)
	{
		return size != 0;
	}

	const int getSize(void)
	{
		return this->size;
	}

	const T operator[](const int index)
	{
		if (index < 0)
			return NULL;
		if (index < size)
		{
			Node *curr = head;
			for (int i = 0; i < index; i++)
				curr = curr->next;
			return curr->value;
		}
		else
			return NULL;
	}

	void operator+=(T value)
	{
		push_back(value);
	}

private:
	Node *head;
	Node *tail;
	int size;
};

