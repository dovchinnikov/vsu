#include <string>
#include <iostream>
#include "HashSet.h"

#define ERROR_KEY_EXISTS -90283

using namespace std;

int HashSet::add(int key, QString value)
{
    if (contains(key)) {
        return -1;
    } else {
        HashPair pair;
        pair.key = key;
        pair.value = value;
        innerList.push_back(pair);
        return 0;
    }
}

QString HashSet::remove(int key)
{
    for (unsigned int i = 0; i < innerList.size(); i++){
        if (innerList[i].key == key) {
            QString value = innerList[i].value;
            innerList.erase(innerList.begin()+i);
            return value;
        }
    }
    return NULL;
}

QString HashSet::get(int key)
{
    for (unsigned int i = 0; i < innerList.size(); i++){
        if (innerList[i].key == key) {
            return innerList[i].value;
        }
    }
    return NULL;
}

bool HashSet::contains(int key)
{
    for (unsigned int i = 0; i < innerList.size(); i++){
        if (innerList[i].key == key)
            return true;
    }
    return false;
}

size_t HashSet::size()
{
    return innerList.size();
}

void HashSet::clear()
{
    innerList.clear();
}

HashSet::iterator HashSet::begin()
{
    return innerList.begin();
}

HashSet::iterator HashSet::end()
{
    return innerList.end();
}
