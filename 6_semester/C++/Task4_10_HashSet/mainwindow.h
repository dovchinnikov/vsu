#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "hashset.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  Q_SLOT void openFile();
  Q_SLOT void showAbout();

private:
  Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
