#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hashset.h"

#include "hashsetwindow.h"

#include <QFileDialog>
#include <QString>
#include <QMessageBox>
#include <QTextStream>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  this->setCentralWidget(ui->mdiArea);
  QObject::connect(ui->actionOpen,
                   SIGNAL(triggered()),
                   this,
                   SLOT(openFile()));
  QObject::connect(ui->actionAbout,
                   SIGNAL(triggered()),
                   this,
                   SLOT(showAbout()));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::openFile(){
    qDebug() << "Opening file...";
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("Open Document"),
                                                    QDir::homePath(),
                                                    tr("text files (*.txt)"));
    if (filename.isNull() || filename.isEmpty()){
        qDebug() << "No file selected";
        QMessageBox::information(0, "Information", "No file selected");
        return;
    }

    qDebug() << "File name selected: " << filename;
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "Cannot open the file";
        QMessageBox::warning(0, "Information", "Cannot open the file");
        return;
    }
    qDebug() << "File opened successfully! " << filename;
    QTextStream in(&file);
    QString text = in.readAll();
    file.close();
    qDebug() << "File was read to the end";
    HashSetWindow *child2 = new HashSetWindow(filename, text);
    this->ui->mdiArea->addSubWindow(child2);
    child2->show();
}

void MainWindow::showAbout(){
    QMessageBox::information(0,"About", "Ovchinnikov Daniil, 2012");
}
