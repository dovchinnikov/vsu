#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QTextDocumentWriter>
#include <QTextCodec>
#include <QTextStream>

#include "hashsetwindow.h"
#include "ui_hashsetwindow.h"
#include "hashset.h"

HashSetWindow::HashSetWindow(QString filename, QString text, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HashSetWindow)
{
    this->ui->setupUi(this);
    this->ui->statusbar->addWidget(new QLabel("Qt is cute!!"));
    this->ui->wordsSource->append(text);
    this->setWindowTitle("HashSet: " + filename);
    this->setFixedSize(550,286);
    this->filename = filename;
    this->hashSet = new HashSet();
    this->changed = true;
    QObject::connect(ui->doItButton, SIGNAL(clicked()),
                     this, SLOT(doItNow()));
    QObject::connect(ui->saveLessFiveCharsButton, SIGNAL(clicked()),
                     this, SLOT(saveLess5Chars()));
    QObject::connect(ui->saveMirrorsButton, SIGNAL(clicked()),
                     this, SLOT(saveMirrors()));
    QObject::connect(ui->wordsSource, SIGNAL(textChanged()),
                     this, SLOT(textChanged()));
}

HashSetWindow::~HashSetWindow()
{
    qDebug() << "about to destroy window";
    delete hashSet;
    delete ui;
}

void HashSetWindow::doItNow(){
    clearResults();
    if (changed) {
        repopulateHashSet();
        changed = false;
    }
    qDebug() << "Computing started";
    for (HashSet::iterator itr = hashSet->begin(); itr!=hashSet->end(); itr++){
        QString current = *itr;
        if (current.length() == 0) {
            continue;
        }
        if (current.length() < 5)
        {
            qDebug() << "Found words less than 5 chars: " << current;
            ui->wordsLessFiveChars->append(current);
            this->ui->saveLessFiveCharsButton->setEnabled(true);
        }
        if (checkMirror(current)){
            qDebug() << "Found mirror word: " << current;
            ui->wordsMirror->append(current);
            this->ui->saveMirrorsButton->setEnabled(true);
        }
    }
    qDebug() << "Computing finished";
}

void HashSetWindow::saveLess5Chars(){
    qDebug() <<  "Saving less 5 chars";
    saveText(ui->wordsLessFiveChars->toPlainText());
}

void HashSetWindow::saveMirrors(){
    qDebug() <<  "Saving mirrors";
    saveText(ui->wordsMirror->document()->toPlainText());
}

void HashSetWindow::textChanged() {
    this->changed = true;
}

void HashSetWindow::clearResults() {
    qDebug() << "Clearing old results";
    this->ui->wordsLessFiveChars->clear();
    this->ui->wordsMirror->clear();
    this->ui->saveLessFiveCharsButton->setEnabled(false);
    this->ui->saveMirrorsButton->setEnabled(false);
}

void HashSetWindow::printHashSet(){
    for (HashSet::iterator itr = hashSet->begin(); itr!=hashSet->end(); itr++){
        ui->wordsSource->append(*itr);
    }
}

bool HashSetWindow::checkMirror(QString str)
{
    QString mirror;
    for (QString::iterator iter = str.end() - 1; iter >= str.begin(); iter--)
    {
        mirror+=*iter;
    }
    return mirror == str;
}

void HashSetWindow::repopulateHashSet() {
    this->hashSet->clear();
    int i = 0;
    QTextStream *in = new QTextStream(&ui->wordsSource->document()->toPlainText(), QIODevice::ReadOnly);
    while (!in->atEnd()) {
       QString line = in->readLine();
       this->hashSet->add(i, line);
       i++;
    }
}

void HashSetWindow::saveText(QString text) {
    //TODO
    qDebug() << "Saving text to file...";
    QFileInfo fileinfo(this->filename);
    QString saveFileName = QFileDialog::getSaveFileName(this,"Save as: ",fileinfo.absolutePath(),"text files (*.txt)");
    if (saveFileName.isNull() || saveFileName.isEmpty() ) {
        QMessageBox::information(this, "Info", "Please, select file to save");
        qDebug() << "Incorrect file name to save";
    } else {
        QFile outFile(saveFileName);
        if (!outFile.open(QIODevice::WriteOnly)) {
            qDebug() << "Out file cannot be opened";
            return;
        }
        outFile.write(text.toUtf8());
        outFile.close();
        qDebug() << "File saved: " << saveFileName;
    }
}
