#-------------------------------------------------
#
# Project created by QtCreator 2012-08-01T00:16:46
#
#-------------------------------------------------

QT       += core gui

TARGET = HashSetMdi
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hashset.cpp \
    hashsetwindow.cpp

HEADERS  += mainwindow.h \
    hashset.h \
    hashpair.h \
    hashsetwindow.h

FORMS    += mainwindow.ui \
    hashsetwindow.ui
