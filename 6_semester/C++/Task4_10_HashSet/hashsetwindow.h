#ifndef HASHSETWINDOW_H
#define HASHSETWINDOW_H

#include <QMainWindow>
#include "hashset.h"

namespace Ui {
  class HashSetWindow;
}

class HashSetWindow : public QMainWindow
{
  Q_OBJECT
  
public:
    explicit HashSetWindow(QString filename, QString text, QWidget *parent = 0);
    ~HashSetWindow();

    Q_SLOT void doItNow();
    Q_SLOT void saveLess5Chars();
    Q_SLOT void saveMirrors();
    Q_SLOT void textChanged();

private:
    Ui::HashSetWindow *ui;
    QString filename;
    HashSet *hashSet;
    bool changed;

    void clearResults();
    void printHashSet();
    bool checkMirror(QString);
    void repopulateHashSet();
    void saveText(QString);
};

#endif // HASHSETWINDOW_H
