#ifndef HASHPAIR_H
#define HASHPAIR_H

#include <QString>

typedef struct {
    int key;
    QString value;
    operator QString(){
        return value;
    }
} HashPair;

#endif // HASHPAIR_H
