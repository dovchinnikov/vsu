#ifndef HASHSET_H
#define HASHSET_H

#include <string>
#include <vector>
#include "HashPair.h"

#define ERROR_KEY_EXISTS -90283

using namespace std;

class HashSet {

public:
    typedef vector<HashPair>::iterator iterator;

    HashSet(){}
    ~HashSet(){}

    int                 add(int key, QString value);
    QString             remove(int key);
    QString             get(int key);
    bool                contains(int key);
    size_t              size();
    void                clear();
    HashSet::iterator   begin();
    HashSet::iterator   end();

private:
    vector<HashPair> innerList;
};

#endif // HASHSET_H
