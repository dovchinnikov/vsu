#pragma once

#include "__Structs.h"

class __MouseEventArgs
{
public:

	__MouseEventArgs(void)
	{
	}

	__MouseEventArgs(int x, int y, int ax, int ay)
	{
		this->x = x;
		this->y = y;
		this->absx = ax;
		this->absy = ay;
	}

	virtual ~__MouseEventArgs(void)
	{
	}

	_Point getAbsolutePoint(void)
	{
		_Point pt = {this->absx, this ->absy};
		return pt;
	}

	int getX(void)
	{
		return this->x;
	}

	int getY(void)
	{
		return this->y;
	}

	int getAbsoluteX(void)
	{
		return this->absx;
	}

	int getAbsoluteY(void)
	{
		return this->absy;
	}

private:
	int x;
	int y;
	int absx;
	int absy;

};

