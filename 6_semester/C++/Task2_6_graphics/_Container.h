#pragma once

#include "_Component.h"
#include "_Clickable.h"
#include "__MouseEventArgs.h"
#include <vector>

using namespace std;

class _Container : public _Component, public _Clickable
{
public:

	virtual ~_Container(void);

	const vector<_Component *> getChildren(void);
	void addChild(_Component *);
	void removeChild(_Component *);
	int getChildrenCount();

	virtual void Click(__MouseEventArgs *e, _Component *sender);

protected:

	_Container(char *);

private:

	vector<_Component *> children;	
};

