#include "StdAfx.h"
#include "_Container.h"
//#define TITLE_BUF 256

_Component::_Component(char *newText)
{
	this->parent = NULL;
	_Rectangle r =  {0,0,200,100};
	this->position = r;
	//this->text = new char[TITLE_BUF];
	this->text = newText;
	this->type = COMPONENT;
	this->visibility = true;
}

_Component::~_Component(void)
{
	//delete this->text;
	/*	
	if (this->parent != NULL) 
	{
		((_Container *)this->parent)->removeChild(this);
	}
	*/
}

const char *_Component::getText(void)
{
	return (this->text == NULL)?"no text":this->text;
}

void _Component::setText(const char *newText)
{
	this->text = (char *)newText;
}

_Component *_Component::getParent(void)
{
	return this->parent;
}

void _Component::setParent(const _Component *newParent)
{
	if (newParent != NULL)
		if (_Container *container = dynamic_cast<_Container *>((_Component *)newParent))
			this->parent = (_Container *)newParent;
}

const _Rectangle _Component::getPosition(void)
{
	return this->position;
}

void _Component::setPosition(const _Rectangle newPosition)
{
	this->position.x = newPosition.x;
	this->position.y = newPosition.y;
	this->position.width = newPosition.width;
	this->position.height = newPosition.height;
}

void _Component::setPosition(const int x, const int y, const int width, const int height)
{
	this->position.x = x;
	this->position.y = y;
	this->position.width = width;
	this->position.height = height;
}

const int _Component::getPositionX(void)
{
	return this->position.x;
}

const int _Component::getPositionY(void)
{
	return this->position.y;
}

const int _Component::getPositionW(void)
{
	return this->position.width;
}

const int _Component::getPositionH(void)
{
	return this->position.height;
}

const _Location _Component::getLocation(void)
{
	_Location l = {this->position.x, this->position.y};
	return l;
}

void _Component::setLocation(const _Location newLocation)
{
	this->position.x = newLocation.x;
	this->position.y = newLocation.y;
}

void _Component::setLocation(const int x, const int y)
{
	this->position.x = x;
	this->position.y = y;
}
const _Size _Component::getSize(void)
{
	_Size s = {this->position.width, this->position.height};
	return s;
}

void _Component::setSize(const _Size newSize)
{
	this->position.width = newSize.width;
	this->position.height = newSize.height;
}

const bool _Component::isVisible(void)
{
	return this->visibility;
}

void _Component::show(void)
{
	this->visibility = true;
}

void _Component::hide(void)
{
	this->visibility = false;
}

const Type _Component::getType(void)
{
	return this->type;
}
