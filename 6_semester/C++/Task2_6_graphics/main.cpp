#include "stdafx.h"
#include "__Graphics.h"
#include "_Component.h"
#include <iostream>

using namespace std;

void showHierarchy(_Container* root, int margin);
void commonOnClickHandler(__MouseEventArgs *e, _Component *sender);

int _tmain(int argc, _TCHAR* argv[])
{
	_Label *lb1 = new _Label("This is test form here");
	lb1->setPosition(350, 20, 250, 150);
	_Label *lb2 = new _Label("THis is label for delete");
	
	_Button *bt1 = new _Button("Test");
	bt1->setPosition(50, 50, 240, 70);
	bt1->addOnClickHandler(commonOnClickHandler);
	_Button *bt2 = new _Button("Start!");
	bt2->setPosition(40, 220, 280, 100);
	bt2->addOnClickHandler(commonOnClickHandler);
	bt2->addOnClickHandler(commonOnClickHandler);
	_Button *bt3 = new _Button("Exit");
	bt3->setPosition(400, 220, 280, 100);
	bt3->addOnClickHandler(commonOnClickHandler);
	
	_Line *ln1 = new _Line(NULL);
	ln1->setPosition(0, 0, 200, 100);
	_Line *ln2 = new _Line(NULL);
	ln2->setPosition(200, 0, 0, 100);
	
	_Panel *panel = new _Panel("Panel");
	panel->setPosition(20, 200, 680, 140);
	panel->addChild(bt2);
	panel->addChild(bt3);
	panel->addOnClickHandler(commonOnClickHandler);

	_Window *mywindow = new _Window("form1");
	mywindow->setPosition(0, 0, 720, 360);
	mywindow->addChild(lb1);
	mywindow->addChild(bt1);
	mywindow->addChild(lb2);
	mywindow->addChild(panel);
	mywindow->addChild(ln1);
	mywindow->addOnClickHandler(commonOnClickHandler);

	((_Container *)mywindow->getChildren()[3])->addChild(ln2);

	showHierarchy(mywindow, 0);
	cout << endl;

	mywindow->removeChild(lb2);
	showHierarchy(mywindow, 0);
	
	cout << endl << "Click to {55, 55} point:" << endl;
	mywindow->forceClick(55, 55);

	cout << endl << "Click to {21, 201} point:" << endl;
	mywindow->forceClick(21, 201);

	cout << endl << "Click to {45, 225} point:" << endl;
	mywindow->forceClick(45, 225);

	cout << endl << "Move button2 20 pixels to the left and click it again";
	bt2->setLocation(bt2->getPositionX() + 20, bt2->getPositionY());
	cout << endl << "Click to {45, 225} point:" << endl;
	mywindow->forceClick(45, 225);
	
	cout << endl << "Click to {410, 230} point:" << endl;
	mywindow->forceClick(410, 230);

	cout << endl << "Set bt3 visibility to false and click it again";
	bt3->hide();
	cout << endl << "Click to {410, 230} point:" << endl;
	mywindow->forceClick(410, 230);

	delete mywindow;

	getchar();
	return 0;
}

void showHierarchy(_Container* root, int margin)
{
	int size = root->getChildrenCount();
	for (int i = 0; i < size; i++)
	{
		_Component *current = root->getChildren()[i];
		for (int k = 0; k < margin; k++)
			cout<<' ';
		cout << current->getText() << endl;
		if (current->getType() == CONTAINER)
			showHierarchy((_Container *)current, margin + 2);							
	}
}

void commonOnClickHandler(__MouseEventArgs *e, _Component *sender)
{
	cout << "\tClick: " << sender->getText() << ": {" << e->getX() << ", " << e->getY() << "}" << endl;
}