#pragma once

struct _Rectangle{
	int x;
	int y;
	int width;
	int height;
};

struct _Location{
	int x;
	int y;
};

struct _Size{
	int width;
	int height;
};

struct _Point{
	int x;
	int y;

	bool isInsideRectangle(_Rectangle rect)
	{
		return rect.x < x && x < rect.x + rect.width && rect.y < y && y < rect.y + rect.height;
	}
};

enum Type {
    COMPONENT,
    CONTAINER,
	UNDEFINED = -1
};