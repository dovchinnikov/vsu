#pragma once

#include "__MouseEventArgs.h"
#include "_Component.h"
#include <vector>

using namespace std;

typedef void (*MouseEventDelegate)(__MouseEventArgs *e, _Component *sender);

class _Clickable {
public:
	
	virtual ~_Clickable(void)
	{
		this->handlers.clear();
	}
	
	void addOnClickHandler(MouseEventDelegate func)
	{
		this->handlers.push_back(func);
	}
	
	void clearOnClickHandlers(void)
	{
		this->handlers.clear();
	}

	virtual void Click(__MouseEventArgs *mea, _Component *sender)
	{
		int s = this->handlers.size();
		for(int i = 0; i < s; i++)
		{
			this->handlers[i](mea, sender);
		}
	}

protected:

	vector<MouseEventDelegate> handlers;
	_Clickable(void)
	{
	}
};

