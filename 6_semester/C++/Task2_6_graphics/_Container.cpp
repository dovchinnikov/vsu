#include "StdAfx.h"
#include "_Container.h"

_Container::_Container(char * text) : _Component(text), _Clickable()
{
	this->type = CONTAINER;
}

_Container::~_Container(void)
{
	int size = this->children.size();
	for (int i = 0; i <size; i++)
	{
		delete this->children[i];
	}
}

const vector<_Component *> _Container::getChildren(void)
{
	return this->children;
}

void _Container::addChild(_Component *child)
{
	if (child != NULL)
	{
		this->children.push_back(child);
		child->setParent(this);
	}
}

void _Container::removeChild(_Component *child)
{
	int size = this->children.size();
	for (int i = 0; i < size; i++)
	{
		if (this->children[i] == child)
		{
			delete children[i];
			this->children.erase(this->children.begin() + i);
			return;
		}
	}
}

int _Container::getChildrenCount()
{
	return this->children.size();
}

void _Container::Click(__MouseEventArgs *e, _Component *sender)
{
	int size = this->children.size();
	for (int i = 0; i < size; i++)
	{
		_Component *child = this->children[i];
		if (!child->isVisible())
			continue;
		if (!e->getAbsolutePoint().isInsideRectangle(child->getPosition()))
			continue;
		if(_Clickable* child_c = dynamic_cast<_Clickable *>(child))
		{
			__MouseEventArgs *args = new __MouseEventArgs(e->getAbsoluteX() - child->getPositionX(), e->getAbsoluteY() - child->getPositionY(), e->getAbsoluteX(), e->getAbsoluteY());
			child_c->Click(args, child);
			delete args;
		}
	}
	_Clickable::Click(e, this);
}