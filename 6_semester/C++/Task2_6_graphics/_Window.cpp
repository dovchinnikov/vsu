#include "StdAfx.h"
#include "_Window.h"

_Window::_Window(char *text) 
	:_Container(text)
{
	this->parent = NULL;
}

_Window::~_Window(void)
{

}

void _Window::setParent(const _Container *parent)
{
	this->parent = NULL;
}

void _Window::forceClick(int x, int y)
{
	__MouseEventArgs *args = new __MouseEventArgs(x, y, x, y);
	this->Click(args, this);
	delete args;
}