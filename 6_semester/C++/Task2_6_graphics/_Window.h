#pragma once

#include "_Container.h"

class _Window : public _Container
{
public:
	_Window(char *);
	~_Window(void);
	void setParent(const _Container*);
	void forceClick(int x, int y);
};

