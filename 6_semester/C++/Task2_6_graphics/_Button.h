#pragma once

#include "_Component.h"
#include "_Clickable.h"

class _Button : public _Component, public _Clickable
{
public:
	_Button(char *);
	virtual ~_Button(void);
};

