#pragma once

#include "__Structs.h"

using namespace std;

class _Component
{
public:

	virtual ~_Component(void);

	const char *		getText(void);
	void				setText(const char *);

	_Component *		getParent(void);
	virtual void		setParent(const _Component *);

	const _Rectangle	getPosition(void);
	void				setPosition(const _Rectangle newPosition);
	void				setPosition(const int, const int, const int, const int);
	const int			getPositionX(void);
	const int			getPositionY(void);
	const int			getPositionW(void);
	const int			getPositionH(void);
	const _Location		getLocation(void);
	void				setLocation(const _Location newLocation);
	void				setLocation(const int, const int);
	const _Size			getSize(void);
	void				setSize(const _Size newSize);

	const bool			isVisible(void);
	void				show(void);
	void				hide(void);

	const Type			getType(void);
	
protected:	

	_Component(char *);					
	_Component *		parent;						//pointer to parent Component
	char *				text;						//text of Component
	_Rectangle			position;					//position of Component
	bool				visibility;					//visibility of Component
	Type				type;						//type of object instance

};

	
